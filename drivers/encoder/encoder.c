/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include <tools/list.h>

#include "encoder.h"

typedef struct encoder_measure_s {
	encoder_timer_t timer;
	TickType_t start_tc;
	TickType_t stop_tc;

	int32_t start_cnt;
	encoder_measure_done_clbk clbk;
} encoder_measure_t;


static portTASK_FUNCTION_PROTO( vEncoderMeasureTask, pvParameters );
static TaskHandle_t xEncoderMeasureTask;

static SemaphoreHandle_t list_semphr;
static list_t *list = NULL;

int encoder_init(encoder_timer_t timer, encoder_type_t type) {
	int ret = 0;
	ret = encoder_soc_init(timer, type);
	configASSERT(ret == 0);

	list = list_create();
	configASSERT(list != NULL);

	list_semphr = xSemaphoreCreateBinary();
	configASSERT(list_semphr != NULL);
	xSemaphoreGive(list_semphr);

	ret = xTaskCreate(vEncoderMeasureTask, "Encoder measure task", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xEncoderMeasureTask);
	configASSERT(ret == pdPASS);

	return 0;
}

void encoder_deinit(encoder_timer_t timer) {
}

encoder_dir_t encoder_get_direction(encoder_timer_t timer) {
	return encoder_soc_get_direction(timer);
}

int32_t encoder_get_value(encoder_timer_t timer) {
	return encoder_soc_get_value(timer);
}

int32_t encoder_measure_speed_block(encoder_timer_t timer, uint32_t time_ms) {
	int32_t stop, start = encoder_get_value(timer);
	vTaskDelay(time_ms / portTICK_PERIOD_MS);
	stop = encoder_get_value(timer);

	return stop >= start ? stop - start : 65535 - start + stop;
}

int encoder_measure_speed_no_block(encoder_timer_t timer, uint32_t time_ms, encoder_measure_done_clbk clbk) {
	if (clbk == NULL) { 
		return -1;
	}

	encoder_measure_t *measure = pvPortMalloc(sizeof(encoder_measure_t));
	configASSERT(measure);

	measure->timer = timer;
	measure->start_tc = xTaskGetTickCount();
	measure->stop_tc = measure->start_tc + (time_ms * portTICK_PERIOD_MS);
	measure->start_cnt = encoder_get_value(timer);
	measure->clbk = clbk;

	if(xSemaphoreTake(list_semphr, portMAX_DELAY) == pdFALSE) {
		return -2;
	}
	
	int ret = list_insert(list, measure);
	configASSERT(ret == 0);
	
	ret = xSemaphoreGive(list_semphr);
	configASSERT(ret == pdTRUE);

	vTaskResume(xEncoderMeasureTask);

	return 0;
}

static portTASK_FUNCTION( vEncoderMeasureTask, pvParameters ) {
	TickType_t next_wake = 0;
	TickType_t now = 0;

	encoder_measure_t *next = NULL;

	for (;;) {
		if (list_is_empty(list)) {
			vTaskSuspend(NULL);
		}

		next_wake = portMAX_DELAY;
		
		while (xSemaphoreTake(list_semphr, portMAX_DELAY) == pdFALSE);

		list_foreach(list) {
			encoder_measure_t *item = ((encoder_measure_t *)(el->item));
			if (next_wake > item->stop_tc) {
				next_wake = item->stop_tc;
				next = item;
			}
		}

		int ret = xSemaphoreGive(list_semphr);
		configASSERT(ret == pdTRUE);

		now = 0;
		vTaskDelayUntil(&now, next_wake);
		now = xTaskGetTickCount();

		int32_t stop = encoder_get_value(next->timer);

		if(next->clbk) {
			next->clbk(stop >= next->start_cnt ? stop - next->start_cnt : 65535 - next->start_cnt + stop, (now - next->start_tc) * portTICK_PERIOD_MS);
		}

		while (xSemaphoreTake(list_semphr, portMAX_DELAY) == pdFALSE);
		list_delete_by_item(list, next);
		ret = xSemaphoreGive(list_semphr);
		configASSERT(ret == pdTRUE);
		vPortFree(next);
	}
}
