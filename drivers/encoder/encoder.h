/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __ENCODER_H__
#define __ENCODER_H__

#include "encoder_soc.h"

typedef void (*encoder_measure_done_clbk)(int32_t count, uint32_t time_ms);

int encoder_init(encoder_timer_t timer, encoder_type_t type);
void encoder_deinit(encoder_timer_t timer);

encoder_dir_t encoder_get_direction(encoder_timer_t timer);
int32_t encoder_get_value(encoder_timer_t timer);

int32_t encoder_measure_speed_block(encoder_timer_t timer, uint32_t time_ms);
int encoder_measure_speed_no_block(encoder_timer_t timer, uint32_t time_ms, encoder_measure_done_clbk clbk);

#endif //__ENCODER_H__
