/*
 * Copyright (c) 2019 Lukasz Stempien
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __ACC_MAG_LSM303D_PRIV_H__
#define __ACC_MAG_LSM303D_PRIV_H__

#define DEV_ADDR_SDO_SA0_PIN_HIGH   (0x3A)
#define DEV_ADDR_SDO_SA0_PIN_LOW    (0x3C)

#define I2C_AUTO_INCREMENT    (0x80)

// Linear acceleration sensitivity
#define LA_SO_2G_VAL            (0.061)
#define LA_SO_4G_VAL            (0.122)
#define LA_SO_6G_VAL            (0.183)
#define LA_SO_8G_VAL            (0.244)
#define LA_SO_16G_VAL           (0.732)

#define M_GN_2G_VAL             (0.080)
#define M_GN_4G_VAL             (0.160)
#define M_GN_8G_VAL             (0.320)
#define M_GN_12G_VAL            (0.479)

#define LSM303D_CHIP_ID         (0x49)

#define TEMP_OUT_L              (0x05)
#define TEMP_OUT_H              (0x06)

#define STATUS_M                (0x07)

#define OUT_X_L_M               (0x08)
#define OUT_X_H_M               (0x09)
#define OUT_Y_L_M               (0x0A)
#define OUT_Y_H_M               (0x0B)
#define OUT_Z_L_M               (0x0C)
#define OUT_Z_H_M               (0x0D)

#define WHO_AM_I                (0x0F)

#define INT_CTRL_M              (0x12)
#define INT_SRC_M               (0x13)
#define INT_THS_L_M             (0x14)
#define INT_THS_H_M             (0x15)

#define OFFSET_X_L_M            (0x16)
#define OFFSET_X_H_M            (0x17)
#define OFFSET_Y_L_M            (0x18)
#define OFFSET_Y_H_M            (0x19)
#define OFFSET_Z_L_M            (0x1A)
#define OFFSET_Z_H_M            (0x1B)

#define REFERENCE_X             (0x1C)
#define REFERENCE_Y             (0x1D)
#define REFERENCE_Z             (0x1E)

#define CTRL0                   (0x1F)
#define CTRL1                   (0x20)
#define CTRL2                   (0x21)
#define CTRL3                   (0x22)
#define CTRL4                   (0x23)
#define CTRL5                   (0x24)
#define CTRL6                   (0x25)
#define CTRL7                   (0x26)

#define STATUS_A                (0x27)

#define OUT_X_L_A               (0x28)
#define OUT_X_H_A               (0x29)
#define OUT_Y_L_A               (0x2A)
#define OUT_Y_H_A               (0x2B)
#define OUT_Z_L_A               (0x2C)
#define OUT_Z_H_A               (0x2D)

#define FIFO_CTRL               (0x2E)
#define FIFO_SRC                (0x2F)

#define IG_CFG1                 (0x30)
#define IG_SRC1                 (0x31)
#define IG_THS1                 (0x32)
#define IG_DUR1                 (0x33)
#define IG_CFG2                 (0x34)
#define IG_SRC2                 (0x35)
#define IG_THS2                 (0x36)
#define IG_DUR2                 (0x37)

#define CLICK_CFG               (0x38)
#define CLICK_SRC               (0x39)
#define CLICK_THS               (0x3A)

#define TIME_LIMIT              (0x3B)
#define TIME_LATENCY            (0x3C)
#define TIME_WINDOW             (0x3D)

#define ACT_THS                 (0x3E)
#define ACT_DUR                 (0x3F)

#define M_RES_MASK              (0x03)
#define M_RES_SHIFT             (0x05)

#define M_ODR_MASK              (0x07)
#define M_ODR_SHIFT             (0x02)

#define MFS_MASK                (0x03)
#define MFS_SHIFT               (0x05)

#define MD_MASK                 (0x03)
#define MD_SHIFT                (0x00)

#define ZYXMDA_MASK             (0x01)
#define ZYXMDA_SHIFT            (0x03)

#define ZYXADA_MASK             (0x01)
#define ZYXADA_SHIFT            (0x03)

typedef struct acc_control_s {
  i2c_bus_t     i2c_bus;
  uint8_t       dev_addr;
  float         acc_sensitivity;
  float         mag_sensitivity;

  uint8_t       *data_buff;
  size_t        data_size;
} acc_control_t;

#endif //__ACC_MAG_LSM303D_PRIV_H__