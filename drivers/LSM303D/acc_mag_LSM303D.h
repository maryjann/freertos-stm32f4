/*
 * Copyright (c) 2019 Lukasz Stempien
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __ACC_MAG_LSM303D_H__
#define __ACC_MAG_LSM303D_H__

#include <stdint.h>
#include "i2c.h"

typedef enum acc_la_fs_e {
  LA_FS_2G  = 0,
  LA_FS_4G  = 1,
  LA_FS_6G  = 2,
  LA_FS_8G  = 3,
  LA_FS_16G = 4,
} acc_la_fs_t;

typedef enum acc_data_rate_e {
  ACC_DATA_RATE_PWR_DOWN = 0x0,
  ACC_DATA_RATE_3_125Hz = 0x1,
  ACC_DATA_RATE_6_25Hz = 0x2,
  ACC_DATA_RATE_12_5Hz = 0x3,
  ACC_DATA_RATE_25Hz = 0x4,
  ACC_DATA_RATE_50Hz = 0x5,
  ACC_DATA_RATE_100Hz = 0x6,
  ACC_DATA_RATE_200Hz = 0x7,
  ACC_DATA_RATE_400Hz = 0x8,
  ACC_DATA_RATE_800Hz = 0x9,
  ACC_DATA_RATE_1600Hz = 0xA,
  ACC_DATA_RATE_END,
} acc_data_rate_t;

typedef enum mag_data_rate_e {
  MAG_DATA_RATE_3_125Hz = 0x0,
  MAG_DATA_RATE_6_25Hz = 0x1,
  MAG_DATA_RATE_12_5Hz = 0x2,
  MAG_DATA_RATE_25Hz = 0x3,
  MAG_DATA_RATE_50Hz = 0x4,
  MAG_DATA_RATE_100Hz = 0x5,
  MAG_DATA_RATE_END = 0x6,
} mag_data_rate_t;

typedef enum mag_res_e {
  MAG_RES_LOW = 0x0,
  MAG_RES_HIGH = 0x3,
  MAG_RES_END,
} mag_res_t;

typedef enum mag_fs_e {
  MAG_FS_2G  = 0x0,
  MAG_FS_4G  = 0x1,
  MAG_FS_8G  = 0x2,
  MAG_FS_12G = 0x3,
} mag_fs_t;

typedef struct acc_mag_config_s {
  i2c_bus_t         i2c_bus;
  uint8_t           SDO_SA0_pin_state;
  
  acc_la_fs_t       acc_range;
  acc_data_rate_t   acc_rate;
  
  mag_data_rate_t   mag_rate;
  mag_res_t         mag_res;
  mag_fs_t          mag_range;
} acc_mag_config_t;

int8_t lsm303d_acc_mag_init(acc_mag_config_t *config);
int8_t lsm303d_read_acc_val(float *val);
int8_t lsm303d_read_mag_val(float *val);
int8_t lsm303d_reset_mag(void);

#endif /* __ACC_MAG_LSM303D_H__ */
