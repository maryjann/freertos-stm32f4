/*
 * Copyright (c) 2019 Lukasz Stempien
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "acc_mag_LSM303D.h"
#include "acc_mag_LSM303D_priv.h"

#define DATA_BUFFER_SIZE 6

static int8_t i2c_read_reg(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data);
static int8_t i2c_read_regs(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, int size);
static int8_t i2c_write_reg(uint8_t dev_addr, uint8_t reg_addr, uint8_t val);
static int8_t i2c_write_regs(uint8_t dev_addr, uint8_t reg_addr, uint8_t* data, int size);
static int8_t lsm303d_acc_read_chip_id(uint8_t *id);
static uint8_t lsm303d_acc_ready(void);
static uint8_t lsm303d_mag_ready(void);

static acc_control_t control;

int8_t lsm303d_acc_mag_init(acc_mag_config_t *config)
{
  int8_t ret = 0;
  uint8_t chip_id = 0;
  
  uint8_t reg_val;
    
  control.i2c_bus = config->i2c_bus;

  control.data_size = DATA_BUFFER_SIZE;
  control.data_buff = malloc(control.data_size + 1 /*extra byte to store register address*/);
  if (control.data_buff == NULL)
  {
    return -1;
  }

  // Set i2c device address acording to SDO/SA0 pin
  if (config->SDO_SA0_pin_state == 0) {
    control.dev_addr = DEV_ADDR_SDO_SA0_PIN_LOW;
  }
  else if (config->SDO_SA0_pin_state == 1) {
    control.dev_addr = DEV_ADDR_SDO_SA0_PIN_HIGH;
  }
  else {
    return -2;
  }

  // Set accelerometer sensivity
  switch (config->acc_range) {
    case  LA_FS_2G:
      control.acc_sensitivity = LA_SO_2G_VAL;
      break;
    case  LA_FS_4G:
      control.acc_sensitivity = LA_SO_4G_VAL;
      break;
    case  LA_FS_6G:
      control.acc_sensitivity = LA_SO_6G_VAL;
      break;
    case  LA_FS_8G:
      control.acc_sensitivity = LA_SO_8G_VAL;
      break;
    case  LA_FS_16G:
      control.acc_sensitivity = LA_SO_16G_VAL;
      break;
    default:
      return -3;
  }

  switch (config->mag_range) {
    case MAG_FS_2G:
      control.mag_sensitivity = M_GN_2G_VAL;
      break;
    case MAG_FS_4G:
      control.mag_sensitivity = M_GN_4G_VAL;
      break;
    case MAG_FS_8G:
      control.mag_sensitivity = M_GN_8G_VAL;
      break;
    case MAG_FS_12G:
      control.mag_sensitivity = M_GN_12G_VAL;
      break;
    default:
      return -4;
  }

  // Check chip ID
  ret = lsm303d_acc_read_chip_id(&chip_id);
  if (ret) {
    return -5;
  }

  if(chip_id != LSM303D_CHIP_ID) {
    return -6;
  }

  ret = i2c_write_reg(control.dev_addr, CTRL0, 0x80);
  if (ret) {
    return -7;
  }

  vTaskDelay(100/portTICK_PERIOD_MS);

  ret = i2c_read_reg(control.dev_addr, CTRL0, &reg_val);
  if (ret || reg_val != 0)
      return -8;

  // Set acceleration data rate - CTRL1 register
  if (config->acc_rate < ACC_DATA_RATE_END) {
    reg_val = (config->acc_rate << 4) | 0x07; // Enable acceleration X, Y, Z-axis
  }
  else {
    return -7;
  }
  ret = i2c_write_reg(control.dev_addr, CTRL1, reg_val);
  if (ret) {
    return -8;
  }

  // Set CTRL2 register
  reg_val= (config->acc_range << 3);
  ret = i2c_write_reg(control.dev_addr, CTRL2, reg_val);
  if (ret) {
    return -9;
  }

  // Set CTRL5 register
  ret = i2c_read_reg(control.dev_addr, CTRL5, &reg_val);
  if (ret) {
    return -10;
  }
  reg_val &= ~((M_RES_MASK << M_RES_SHIFT) | (M_ODR_MASK << M_ODR_SHIFT));
  reg_val |= (config->mag_rate & M_ODR_MASK) << M_ODR_SHIFT;
  reg_val |= (config->mag_res & M_RES_MASK) << M_RES_SHIFT;
  ret = i2c_write_reg(control.dev_addr, CTRL5, reg_val);
  if (ret) {
    return -11;
  }

  // Set CTRL6 register
  ret = i2c_read_reg(control.dev_addr, CTRL6, &reg_val);
  if (ret) {
    return -12;
  }
  reg_val &= ~(MFS_MASK << MFS_SHIFT);
  reg_val |= (config->mag_range & MFS_MASK) << MFS_SHIFT;
  ret = i2c_write_reg(control.dev_addr, CTRL6, reg_val);
  if (ret) {
    return -13;
  }

  // Set CTRL7 register
  ret = i2c_read_reg(control.dev_addr, CTRL7, &reg_val);
  if (ret) {
    return -14;
  }
  // Enable continuous-conversion mode
  reg_val &= ~(MD_MASK << MD_SHIFT);
  ret = i2c_write_reg(control.dev_addr, CTRL7, reg_val);
  if (ret) {
    return -15;
  }

  return 0;
}

int8_t lsm303d_read_acc_val(float *val)
{
  uint8_t data[6];
  int8_t ret;

  while (lsm303d_acc_ready() == 0) {
    vTaskDelay(1 / portTICK_PERIOD_MS);
  }

  ret = i2c_read_regs(control.dev_addr, OUT_X_L_A, data, sizeof(data) / sizeof(data[0]));
  if (ret) {
    return -1;
  }

  val[0] = (int16_t)((data[1] << 8) | data[0]);
  val[0] *= control.acc_sensitivity;
  val[0] /= 1000;
  val[1] = (int16_t)((data[3] << 8) | data[2]);
  val[1] *= control.acc_sensitivity;
  val[1] /= 1000;
  val[2] = (int16_t)((data[5] << 8) | data[4]);
  val[2] *= control.acc_sensitivity;
  val[2] /= 1000;
  return 0;
}

int8_t lsm303d_read_mag_val(float *val) {
  uint8_t data[6];
  int8_t ret;

  while (lsm303d_mag_ready() == 0) {
    vTaskDelay(1 / portTICK_PERIOD_MS);
  }
    
  ret = i2c_read_regs(control.dev_addr, OUT_X_L_M, data, sizeof(data) / sizeof(data[0]));
  if (ret) {
    return -1;
  }

  val[0] = (int16_t)((data[1] << 8) | data[0]);
  val[0] *= control.mag_sensitivity;
  val[0] /= 1000;
  val[1] = (int16_t)((data[3] << 8) | data[2]);
  val[1] *= control.mag_sensitivity;
  val[1] /= 1000;
  val[2] = (int16_t)((data[5] << 8) | data[4]);
  val[2] *= control.mag_sensitivity;
  val[2] /= 1000;

  return 0; 
}

int8_t lsm303d_reset_mag(void) {
  int8_t ret = 0;
  uint8_t data[6] = {0};

  ret = i2c_write_regs(control.dev_addr, OFFSET_X_L_M, data, sizeof(data) / sizeof(data[0]));
  if (ret) {
    return -1;
  }
  
  while(lsm303d_mag_ready() == 0) {
    vTaskDelay(1 / portTICK_PERIOD_MS);
  }
  
  ret = i2c_read_regs(control.dev_addr, OUT_X_L_M, data, sizeof(data) / sizeof(data[0]));
  if (ret) {
    return -2;
  }

  ret = i2c_write_regs(control.dev_addr, OFFSET_X_L_M, data, sizeof(data) / sizeof(data[0]));
  if (ret) {
    return -3;
  }
  
  return 0;
}

static int8_t i2c_read_reg(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data)
{
  int retry_cnt = 5;
  while(i2c_master_send(control.i2c_bus, dev_addr, &reg_addr, 1) < 0)
  {
    if (retry_cnt--) {
      return -1;
    }
  }
  while(i2c_master_recv(control.i2c_bus, dev_addr, data, 1) < 0)
  {
    if (retry_cnt--) {
      return -2;
    }
  }
  return 0;
}

static int8_t i2c_read_regs(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, int size)
{
  int retry_cnt = 5;
  reg_addr |= 0x80;
  while(i2c_master_send(control.i2c_bus, dev_addr, &reg_addr, 1) < 0)
  {
    if (retry_cnt--) {
      return -1;
    }
  }
  while(i2c_master_recv(control.i2c_bus, dev_addr, data, size) < 0)
  {
    if (retry_cnt--) {
      return -2;
    }
  }
  return 0;
}

static int8_t i2c_write_reg(uint8_t dev_addr, uint8_t reg_addr, uint8_t val)
{
  int retry_cnt = 5;
  
  control.data_buff[0] = reg_addr;
  control.data_buff[1] = val;

  while(i2c_master_send(control.i2c_bus, dev_addr, control.data_buff, 2) < 0)
  {
    if (retry_cnt--) {
      return -2;
    }
  }
  return 0;
}

static int8_t i2c_write_regs(uint8_t dev_addr, uint8_t reg_addr, uint8_t* data, int size) {
  int retry_cnt = 5;

  if (size > 1)
    reg_addr |= I2C_AUTO_INCREMENT;

  if(control.data_size < size)
  {
    uint8_t *tmp = realloc(control.data_buff, size + 1);
    if(tmp == NULL)
    {
      return -1;
    }

      control.data_buff = tmp;
      control.data_size = size;
  }

  control.data_buff[0] = reg_addr;
  memcpy(&control.data_buff[1], data, size);

  while(i2c_master_send(control.i2c_bus, dev_addr, control.data_buff, size + 1) < 0)
  {
    if (retry_cnt--) {
      return -2;
    }
  }
  
  return 0;
}

static int8_t lsm303d_acc_read_chip_id(uint8_t *id)
{
  int8_t ret = 0;
  ret = i2c_read_reg(control.dev_addr, WHO_AM_I, id);
  if (ret) {
    return -1;
  }
  return 0;
}

static uint8_t lsm303d_acc_ready(void) {
  uint8_t status;
  int8_t ret = 0;
  ret = i2c_read_reg(control.dev_addr, STATUS_A, &status);
  if (ret)
    return 0;
  
  return (status & (ZYXADA_MASK << ZYXADA_SHIFT));
}

static uint8_t lsm303d_mag_ready(void) {
  uint8_t status;
  int8_t ret = 0;
  ret = i2c_read_reg(control.dev_addr, STATUS_M, &status);
  if (ret)
    return 0;
  
  return (status & (ZYXMDA_MASK << ZYXMDA_SHIFT));
}