/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FreeRTOS.h>
#include "task.h"
#include "semphr.h"

#include "spi.h"

static void spi_tx_clbk(int result, void *data);
static void spi_rx_clbk(int result, void *data);
static void spi_tx_rx_clbk(int result, void *data);

static SemaphoreHandle_t bus_sem[SPI_BUS_END];
static SemaphoreHandle_t tsk_sem[SPI_BUS_END];

int spi_init(spi_bus_t bus, uint32_t clock_speed)
{
    if (bus >= SPI_BUS_END)
    {
        return -1;
    }

	bus_sem[bus] = xSemaphoreCreateMutex();
    configASSERT(bus_sem[bus] != NULL);
	
	tsk_sem[bus] = xSemaphoreCreateBinary();
    configASSERT(tsk_sem[bus] != NULL);
	
	spi_soc_init(bus, clock_speed);
	return 0;
}

void spi_deinit(spi_bus_t bus)
{
}

int spi_send(spi_bus_t bus, uint8_t *data, size_t length)
{
	xSemaphoreTake(bus_sem[bus], portMAX_DELAY);

    int ret = spi_soc_send(bus, data, length, spi_tx_clbk, (void *)bus);
    if(ret != 0)
    {
        /* TODO - add error handling */
        return ret;
    }

    xSemaphoreTake(tsk_sem[bus], portMAX_DELAY);
    xSemaphoreGive(bus_sem[bus]);

    return 0;
}

int spi_recv(spi_bus_t bus, uint8_t *data, size_t length)
{
	xSemaphoreTake(bus_sem[bus], portMAX_DELAY);
    int ret = spi_soc_recv(bus, data, length, spi_rx_clbk, (void *)bus);
    if(ret != 0)
    {
        /* TODO - add error handling */
        return ret;
    }

    xSemaphoreTake(tsk_sem[bus], portMAX_DELAY);
    xSemaphoreGive(bus_sem[bus]);

    return 0;
}

int spi_send_recv(spi_bus_t bus, uint8_t *send, uint8_t *recv, size_t length)
{
    xSemaphoreTake(bus_sem[bus], portMAX_DELAY);
    int ret = spi_soc_send_recv(bus, send, recv, spi_tx_rx_clbk, length, (void *)bus);
    if(ret != 0)
    {
        /* TODO - add error handling */
        return ret;
    }

    xSemaphoreTake(tsk_sem[bus], portMAX_DELAY);
    xSemaphoreGive(bus_sem[bus]);

    return 0;
}

static inline void spi_clbk(int result, void *data)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
    if(result != SPI_ERROR_NONE)
    {
        /* TODO - add error handling */
        return;
    }

    xSemaphoreGiveFromISR(tsk_sem[(spi_bus_t)data], &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

static void spi_tx_clbk(int result, void *data)
{
    spi_clbk(result, data);
}

static void spi_rx_clbk(int result, void *data)
{
    spi_clbk(result, data);
}

static void spi_tx_rx_clbk(int result, void *data)
{
    spi_clbk(result, data);
}