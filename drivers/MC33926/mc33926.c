/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FreeRTOS.h>
#include "task.h"

#include "gpio.h"
#include "pwm_soc.h"

#include "mc33926.h"

#define	FREQUENCY_HZ 10000
#define RESOLUTION	256

#define DUTY_CYCLE_MIN 0
#define DUTY_CYCLE_MAX RESOLUTION

mc33926_handle_t mc33926_init(const mc33926_init_t *init) {
	configASSERT(init != NULL);
	configASSERT(init->pwm < PWM_TIMER_END);

	int ret = 0;

	if (init->en_pin != GPIO_INVALID_PIN) {
		ret = gpio_init(init->en_pin, G_MODE_OUTPUT_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL);
		configASSERT(ret == 0);
		gpio_write_pin(init->en_pin, 0);
	}

	if (init->dir_pin != GPIO_INVALID_PIN) {
		ret = gpio_init(init->dir_pin, G_MODE_OUTPUT_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL);
		configASSERT(ret == 0);
		gpio_write_pin(init->dir_pin, 0);
	}

	if (init->d2_pin != GPIO_INVALID_PIN) {
		ret = gpio_init(init->d2_pin, G_MODE_OUTPUT_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL);
		configASSERT(ret == 0);
		gpio_write_pin(init->d2_pin, 0);
	}

	ret = pwm_soc_init(init->pwm, FREQUENCY_HZ, RESOLUTION);
	configASSERT(ret == 0);
	gpio_write_pin(init->en_pin, 0);

	return init;
}

void mc33926_deinit(mc33926_handle_t handle) {
	configASSERT(handle != NULL);
}

int mc33926_set_power(mc33926_handle_t handle, mc33926_state_t state) {
	configASSERT(handle != NULL);

	gpio_write_pin(((mc33926_init_t *)handle)->en_pin, state);

	return 0;
}

int mc33926_set_speed(mc33926_handle_t handle, int percent) {
	configASSERT(handle != NULL);

	return pwm_soc_set_duty_cycle(((mc33926_init_t *)handle)->pwm, DUTY_CYCLE_MAX * ((float)percent/(float)100));
}

int mc33926_set_direction(mc33926_handle_t handle, mc33926_dir_t dir) {
	configASSERT(handle != NULL);

	gpio_write_pin(((mc33926_init_t *)handle)->dir_pin, dir);

	return 0;
}

int mc33926_set_output(mc33926_handle_t handle, mc33926_state_t state) {
	configASSERT(handle != NULL);

	gpio_write_pin(((mc33926_init_t *)handle)->d2_pin, state);

	return 0;
}
