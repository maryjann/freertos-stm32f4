/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __MC33926_H__
#define __MC33926_H__

#include "pwm_soc.h"

typedef enum mc33926_dir_e {
	NON_INVERTED,
	INVERTED
} mc33926_dir_t;

typedef enum mc33926_state_e {
	DISABLED,
	ENABLED
} mc33926_state_t;

typedef struct mc33926_init_s {
	size_t en_pin;
	size_t dir_pin;
	size_t d2_pin;
	pwm_timer_t pwm;
} mc33926_init_t;

typedef const void * mc33926_handle_t; 

mc33926_handle_t mc33926_init(const mc33926_init_t *init);
void mc33926_deinit(mc33926_handle_t handle);

int mc33926_set_speed(mc33926_handle_t handle, int percent);
int mc33926_set_power(mc33926_handle_t handle, mc33926_state_t state);
int mc33926_set_direction(mc33926_handle_t handle, mc33926_dir_t dir);
int mc33926_set_output(mc33926_handle_t handle, mc33926_state_t state);

#endif //__MC33926_H__