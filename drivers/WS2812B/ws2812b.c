/*
 * Copyright (c) 2019 Damian Eppel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "ws2812b.h"
#include "ws2812b_soc.h"
#include "ws2812b_bsp.h"
#include <string.h>

#define PWM_FREQ WS2812B_PWM_FREQ
#define PWM_PULSES_IN_50USEC (50 * PWM_FREQ / 1000000UL)

#define WS2812B_RED_OFFSET      1
#define WS2812B_GREEN_OFFSET    0
#define WS2812B_BLUE_OFFSET     2

static uint32_t T0H = 0;
static uint32_t T1H = 0;

struct ws2812b_struct {
    volatile enum ws2812b_state state;
    uint8_t leds_colors[LED_CFG_BYTES_PER_LED * WS2812B_COUNT];
    uint32_t tmp_led_data[2 * LED_CFG_RAW_BYTES_PER_LED];
    int current_led;
};

static struct ws2812b_struct ws2812b_data;
static void led_update_sequence(int tc, void *);
static void send_reset_pulse(struct ws2812b_struct *data);

int pwm_leds_init() {

    int ret = 0;

    if (ws2812b_data.state != UNINITIALIZED)
        return -1;

    if ((ret = ws2812b_soc_dma2timer_init(led_update_sequence, &T0H, &T1H)) == 0) {
        ws2812b_data.state = READY;
    }

    return ret;
}

int pwm_leds_set_led_color(size_t index, uint32_t rgb) {
    if (index < WS2812B_COUNT) {
        ws2812b_data.leds_colors[index * LED_CFG_BYTES_PER_LED + 0] = (rgb >> 24) & 0xff;
        ws2812b_data.leds_colors[index * LED_CFG_BYTES_PER_LED + 1] = (rgb >> 16) & 0xff;
        ws2812b_data.leds_colors[index * LED_CFG_BYTES_PER_LED + 2] = (rgb >> 8) & 0xff;
        return 0;
    }
    return -1;
}

int pwm_leds_set_color_all(uint32_t rgb) {
    size_t index;
    for (index = 0; index < WS2812B_COUNT; index++) {
        ws2812b_data.leds_colors[index * LED_CFG_BYTES_PER_LED + 0] = (rgb >> 24) & 0xff;
        ws2812b_data.leds_colors[index * LED_CFG_BYTES_PER_LED + 1] = (rgb >> 16) & 0xff;
        ws2812b_data.leds_colors[index * LED_CFG_BYTES_PER_LED + 2] = (rgb >> 8) & 0xff;
    }
    return 0;
}

int pwm_leds_update(int block) {
    if (ws2812b_data.state != READY) {
        return -1;
    }

    ws2812b_data.state = BUSY_RESET_PULSE_1;
    send_reset_pulse(&ws2812b_data);

    if (block) {
        while (ws2812b_data.state != READY);      /* Wait to finish */
    }
    return 1;
}

static void send_reset_pulse(struct ws2812b_struct *data) {

    /* Set all bytes to 0 to achieve 50us pulse */
    memset(data->tmp_led_data, 0, sizeof(data->tmp_led_data));

    if (data->state == BUSY_RESET_PULSE_1) {
        /* Set single pulse at the start */
        data->tmp_led_data[0] = 2;
    }

    /* Set DMA to normal mode, set memory to beginning of data and length to 40 elements */
    /* 800kHz PWM x 40 samples = ~50us pulse low */
    ws2812b_soc_dma2timer_start((uint32_t)data->tmp_led_data,
                    PWM_PULSES_IN_50USEC,
                    DMA_TC_IRQ_ENABLED,
                    DMA_MODE_NORMAL);
}

static int fill_led_pwm_data(size_t ledx, uint8_t *leds_colors, uint32_t *raw_pwm_data) {

    size_t i;

    if (ledx < WS2812B_COUNT) {
        for (i = 0; i < 8; i++) {
            raw_pwm_data[i] =
                    (leds_colors[LED_CFG_BYTES_PER_LED * ledx + WS2812B_RED_OFFSET] & (1 << (7 - i))) ?
                        T1H : T0H;

            raw_pwm_data[8 + i] =
                    (leds_colors[LED_CFG_BYTES_PER_LED * ledx + WS2812B_GREEN_OFFSET] & (1 << (7 - i))) ?
                        T1H : T0H;

            raw_pwm_data[16 + i] =
                    (leds_colors[LED_CFG_BYTES_PER_LED * ledx + WS2812B_BLUE_OFFSET] & (1 << (7 - i))) ?
                        T1H : T0H;
        }
        return 0;
    }
    return -1;
}

static void led_update_sequence(int tc, void *unused) {
    tc = !!tc;                                  /* Convert to 1 or 0 value only */

    (void)unused;

    /* Check for reset pulse at the end of PWM stream */
    if (ws2812b_data.state == BUSY_RESET_PULSE_2) {
        /*
         * When reset pulse is active, we have to wait full DMA response,
         * before we can start modifying array which is shared with DMA and PWM
         */
        if (!tc) {
            return;
        }

        ws2812b_soc_dma2timer_stop();

        ws2812b_data.state = READY;  /* We are not updating anymore */
        return;
    }

    /* Check for reset pulse on beginning of PWM stream */
    if (ws2812b_data.state == BUSY_RESET_PULSE_1) {
        /*
         * When reset pulse is active, we have to wait full DMA response,
         * before we can start modifying array which is shared with DMA and PWM
         */
        if (!tc) {
            return;
        }

        /* Disable timer output and disable DMA stream */
        ws2812b_soc_dma2timer_stop();

        ws2812b_data.state = BUSY_LED_UPDATE; /* Not in reset pulse anymore */
        ws2812b_data.current_led = 0; /* Reset current led */
    } else {
        /*
         * When we are not in reset mode,
         * go to next led and process data for it
         */
        ws2812b_data.current_led++;
    }

    /*
     * This part is used to prepare data for "next" led,
     * for which update will start once current transfer stops in circular mode
     */
    if (ws2812b_data.current_led < WS2812B_COUNT) {
        /*
         * If we are preparing data for first time (current_led == 0)
         * or if there was no TC event (it was HT):
         *
         *  - Prepare first part of array, because either there is no transfer
         *      or second part (from HT to TC) is now in process for PWM transfer
         *
         * In other case (TC = 1)
         */
        if (ws2812b_data.current_led == 0 || !tc) {
            fill_led_pwm_data(ws2812b_data.current_led, &ws2812b_data.leds_colors[0], &ws2812b_data.tmp_led_data[0]);
        } else {
            fill_led_pwm_data(ws2812b_data.current_led, &ws2812b_data.leds_colors[0], &ws2812b_data.tmp_led_data[LED_CFG_RAW_BYTES_PER_LED]);
        }

        /*
         * If we are preparing first led (current_led = 0), then:
         *
         *  - We setup first part of array for first led,
         *  - We have to prepare second part for second led to have one led prepared in advance
         *  - Set DMA to circular mode and start the transfer + PWM output
         */
        if (ws2812b_data.current_led == 0) {
            ws2812b_data.current_led++;                      /* Go to next LED */
            fill_led_pwm_data(ws2812b_data.current_led, &ws2812b_data.leds_colors[0], &ws2812b_data.tmp_led_data[LED_CFG_RAW_BYTES_PER_LED]);   /* Prepare second LED too */

            /* Set DMA to circular mode and set length to 48 elements for 2 leds */
            ws2812b_soc_dma2timer_start((uint32_t)ws2812b_data.tmp_led_data,
                            2 * LED_CFG_RAW_BYTES_PER_LED,
                            DMA_HT_IRQ_ENABLED | DMA_TC_IRQ_ENABLED,
                            DMA_MODE_CIRCULAR);
        }

    /*
     * When we reached all leds, we have to wait to transmit data for all leds before we can disable DMA and PWM:
     *
     *  - If TC event is enabled and we have EVEN number of LEDS (2, 4, 6, ...)
     *  - If HT event is enabled and we have ODD number of LEDS (1, 3, 5, ...)
     */
    } else if ((!tc && (WS2812B_COUNT & 0x01)) || (tc && !(WS2812B_COUNT & 0x01))) {
        ws2812b_soc_dma2timer_stop();

        /* It is time to send final reset pulse, 50us at least */
        if (ws2812b_data.state == BUSY_LED_UPDATE) {
            ws2812b_data.state = BUSY_RESET_PULSE_2;
            send_reset_pulse(&ws2812b_data);
        }
    }
}
