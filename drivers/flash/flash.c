/*
 * Copyright (c) 2020 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "semphr.h"

#include "flash.h"
#include "flash_soc.h"

static uint8_t init_done = 0;
static SemaphoreHandle_t flash_semphr;

int flash_init(void)
{
	int ret = 0;
	
	if (init_done)
		return 1;

	flash_semphr = xSemaphoreCreateBinary();
	configASSERT(flash_semphr != NULL);
	
	ret = flash_soc_init();
	if (ret == 0)
		init_done = 1;

	xSemaphoreGive(flash_semphr);

	return ret;
}

void flash_deinit(void)
{
	while (xSemaphoreTake(flash_semphr, portMAX_DELAY) == pdFALSE);
	flash_deinit();
	configASSERT (xSemaphoreGive(flash_semphr) == pdTRUE);

	init_done = 0;
}

size_t flash_get_begin(void)
{
	if (init_done)
		return flash_soc_get_begin();
	return 0;
}

size_t flash_get_end(void)
{
	if (init_done)
		return flash_soc_get_end();
	return 0;
}

int flash_erase(size_t address, size_t len)
{
	int ret = 0;
	while (xSemaphoreTake(flash_semphr, portMAX_DELAY) == pdFALSE);

	ret = flash_soc_erase(address, len);
	
	configASSERT (xSemaphoreGive(flash_semphr) == pdTRUE);

	return ret;
}

int flash_write(size_t address, void *data, size_t len)
{
	int ret = 0;
	while (xSemaphoreTake(flash_semphr, portMAX_DELAY) == pdFALSE);

	ret = flash_soc_write(address, data, len);
	
	configASSERT (xSemaphoreGive(flash_semphr) == pdTRUE);

	return ret;
}

int flash_read(size_t address, void *data, size_t len)
{
	int ret = 0;
	while (xSemaphoreTake(flash_semphr, portMAX_DELAY) == pdFALSE);
	

	ret = flash_soc_read(address, data, len);

	configASSERT (xSemaphoreGive(flash_semphr) == pdTRUE);

	return ret;
}