/*
Copyright (c) 2019 Jakub Nowacki

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include <FreeRTOS.h>
#include "task.h"

#include "gpio.h"
#include "pwm_soc.h"

#include "drv8835.h"

#define	FREQUENCY_HZ 10000
#define RESOLUTION	256

#define DUTY_CYCLE_MIN 0
#define DUTY_CYCLE_MAX RESOLUTION

drv8835_handle_t drv8835_init(const drv8835_init_t *init) {
	configASSERT(init != NULL);
	configASSERT(init->pwm < PWM_TIMER_END);

	int ret = 0;

	if (init->dir_pin != GPIO_INVALID_PIN) {
		ret = gpio_init(init->dir_pin, G_MODE_OUTPUT_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL);
		configASSERT(ret == 0);
		gpio_write_pin(init->dir_pin, 0);
	}

	ret = pwm_soc_init(init->pwm, FREQUENCY_HZ, RESOLUTION);
	configASSERT(ret == 0);
	
	return init;
}

void drv8835_deinit(drv8835_handle_t handle) {
	configASSERT(handle != NULL);
}

int drv8835_set_speed(drv8835_handle_t handle, int percent) {
	configASSERT(handle != NULL);

	return pwm_soc_set_duty_cycle(((drv8835_init_t *)handle)->pwm, DUTY_CYCLE_MAX * ((float)percent/(float)100));
}

int drv8835_set_direction(drv8835_handle_t handle, drv8835_dir_t dir) {
	configASSERT(handle != NULL);

	gpio_write_pin(((drv8835_init_t *)handle)->dir_pin, dir);

	return 0;
}
