/*
Copyright (c) 2019 Jakub Nowacki

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#ifndef __DRV_8835_H__
#define __DRV_8835_H__

#include "pwm_soc.h"

typedef enum drv8835_dir_e {
	NON_INVERTED,
	INVERTED
} drv8835_dir_t;

typedef enum drv8835_state_e {
	DISABLED,
	ENABLED
} drv8835_state_t;

typedef struct drv8835_init_s {
	size_t dir_pin;
	pwm_timer_t pwm;
} drv8835_init_t;

typedef const void * drv8835_handle_t; 

drv8835_handle_t drv8835_init(const drv8835_init_t *init);
void drv8835_deinit(drv8835_handle_t handle);

int drv8835_set_speed(drv8835_handle_t handle, int percent);
int drv8835_set_direction(drv8835_handle_t handle, drv8835_dir_t dir);

#endif //__DRV_8835_H__