/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FreeRTOS.h>
#include "task.h"
#include "semphr.h"

#include "i2c.h"

static SemaphoreHandle_t bus_sem[I2C_BUS_END];
static SemaphoreHandle_t tsk_sem[I2C_BUS_END];

static void i2c_master_clbk(int result, void *data);

int i2c_master_init(i2c_bus_t bus, uint32_t addr_mode, uint32_t clock_speed)
{
    if(bus >= I2C_BUS_END)
        return -1;
    
    bus_sem[bus] = xSemaphoreCreateMutex();
    configASSERT(bus_sem[bus] != NULL);

    tsk_sem[bus] = xSemaphoreCreateBinary();
    configASSERT(tsk_sem[bus] != NULL);

    int ret = i2c_master_soc_init(bus, addr_mode, clock_speed);
    if(ret != 0)
    {
        /* TODO add error logging */
        return ret;
    }

    return 0;
}

void i2c_master_deinit(i2c_bus_t bus)
{   
    i2c_master_soc_deinit(bus);
    vSemaphoreDelete(bus_sem[bus]);
    vSemaphoreDelete(tsk_sem[bus]);
}

int i2c_master_send(i2c_bus_t bus, uint8_t address, uint8_t *data, size_t length)
{
    while(xSemaphoreTake(bus_sem[bus], portMAX_DELAY) == pdFALSE);
    
    int ret = i2c_master_soc_send(bus, address, data, length, i2c_master_clbk, (void *)bus);
    if(ret != 0)
    {
        /* TODO - add error handling */
        xSemaphoreGive(bus_sem[bus]);
        return ret;
    }

    xSemaphoreTake(tsk_sem[bus], portMAX_DELAY);
    xSemaphoreGive(bus_sem[bus]);
    
    return 0;
}

int i2c_master_recv(i2c_bus_t bus, uint8_t address, uint8_t *data, size_t length)
{
    while(xSemaphoreTake(bus_sem[bus], portMAX_DELAY) == pdFALSE);

    int ret = i2c_master_soc_recv(bus, address, data, length, i2c_master_clbk, (void *)bus);
    if(ret != 0)
    {
        /* TODO - add error handling */
        xSemaphoreGive(bus_sem[bus]);
        return ret;
    }

    xSemaphoreTake(tsk_sem[bus], portMAX_DELAY);
    xSemaphoreGive(bus_sem[bus]);

    return 0;
}

static void i2c_master_clbk(int result, void *data)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    if(result != I2C_ERROR_NONE)
    {
        /* TODO - add error handling */
        return;
    }

    xSemaphoreGiveFromISR(tsk_sem[(i2c_bus_t)data], &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
