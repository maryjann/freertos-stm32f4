/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __I2C_H__
#define __I2C_H__

#include "i2c_soc.h"

/* I2C master interface */
int i2c_master_init(i2c_bus_t bus, uint32_t addr_mode, uint32_t clock_speed);
void i2c_master_deinit(i2c_bus_t bus);

int i2c_master_send(i2c_bus_t bus, uint8_t address, uint8_t *data, size_t length);
int i2c_master_recv(i2c_bus_t bus, uint8_t address, uint8_t *data, size_t length);

/* I2C slave interface */
int i2c_slave_init(i2c_bus_t bus);
void i2c_slave_deinit(i2c_bus_t bus);

int i2c_slave_send(i2c_bus_t bus, uint8_t *data, size_t length);
size_t i2c_slave_recv(i2c_bus_t bus, uint8_t *data, size_t lenght);

#endif /* __I2C_H__ */
