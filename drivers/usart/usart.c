/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FreeRTOS.h>
#include <string.h>
#include "task.h"
#include "semphr.h"
#include "usart.h"

#include "tools/ringbuf.h"

#define USART_TX_BUFFER_SIZE 256
#define USART_RX_BUFFER_SIZE 256

typedef struct usart_desc_s {
	usart_t port;
	size_t rx_cnt;
	
	uint8_t rx_data[USART_RX_BUFFER_SIZE];
	uint8_t tx_data[USART_TX_BUFFER_SIZE];

	ringbuf_t b_tx;
	ringbuf_t b_rx;
	
	SemaphoreHandle_t s_rx;
	SemaphoreHandle_t s_tx;

	uint8_t usart_tx_busy;
} usart_desc_t;

static void usart_send_done(void *data, int result);
static void usart_recv_done(void *data, int result);

usart_handle_t usart_init(usart_t port, uint32_t baud_rate, usart_world_length_t world_length, usart_stop_bits_t stop_bits, usart_parity_t parity, usart_hw_control_t hw_control) {
	 if(port >= USART_PORT_END)
        return NULL;
    
    int ret = usart_soc_init(port, baud_rate, world_length, stop_bits, parity, hw_control);
    if(ret != 0) {
        return NULL;
    }

    usart_desc_t *fd = pvPortMalloc(sizeof(usart_desc_t));
	configASSERT(fd != NULL);

	fd->port = port;

	fd->s_tx = xSemaphoreCreateBinary();
	configASSERT(fd->s_tx != NULL);

	fd->s_rx = xSemaphoreCreateBinary();
	configASSERT(fd->s_rx != NULL);

	fd->b_tx = ringbuf_new(USART_TX_BUFFER_SIZE);
	configASSERT(fd->b_tx != NULL);
	
	fd->b_rx = ringbuf_new(USART_RX_BUFFER_SIZE);
	configASSERT(fd->b_rx != NULL);

	ret = usart_soc_recv(fd->port, fd->rx_data, 1, usart_recv_done, fd);
	configASSERT(ret == 0);

	fd->usart_tx_busy = 0;
	
    return fd;
}

void usart_deinit(usart_handle_t fd) {
	usart_soc_deinit(fd->port);
	
	vSemaphoreDelete(fd->s_tx);
	vSemaphoreDelete(fd->s_rx);

	ringbuf_free(&fd->b_tx);
	ringbuf_free(&fd->b_rx);

	vPortFree(fd);

	return;
}

int usart_send(usart_handle_t fd, uint8_t *data, size_t length, int timeout_ms) {
	configASSERT(length <= ringbuf_capacity(fd->b_tx));

	while(1) {
		if(ringbuf_bytes_free(fd->b_tx) < length) {
			int ret = xSemaphoreTake(fd->s_tx, 
									 timeout_ms < 0 ? portMAX_DELAY : timeout_ms / portTICK_PERIOD_MS);
			if(ret == pdFALSE)
				return -1;
			continue;
		}

		if(fd->usart_tx_busy == 0) {
			memcpy(fd->tx_data, data, length);
			fd->usart_tx_busy = 1;
			usart_soc_send(fd->port, fd->tx_data, length, usart_send_done, fd);
			return 0;
		}
		else {
			ringbuf_memcpy_into(fd->b_tx, data, length);
			return 0;
		}
	}

	return -2;
}

int usart_recv(usart_handle_t fd, uint8_t *data, size_t length, int timeout_ms) {
	while (1) {
		size_t b_use = ringbuf_bytes_used(fd->b_rx);
		if(b_use < length) {
			fd->rx_cnt = (length - b_use);
			int ret = xSemaphoreTake(fd->s_rx, 
				                     timeout_ms < 0 ? portMAX_DELAY : timeout_ms / portTICK_PERIOD_MS);
			if(ret == pdFALSE)
				return -1;
			continue;
		}

		ringbuf_memcpy_from(data, fd->b_rx, length);
		return 0;
	}
	
	return -2;
}

static void usart_send_done(void *data, int result) {
	BaseType_t xHigherPriorityTaskWoken;
	usart_handle_t fd = (usart_handle_t)data;
	
	if(ringbuf_is_empty(fd->b_tx) == 0) {
		size_t len = ringbuf_bytes_used(fd->b_tx);
		ringbuf_memcpy_from(fd->tx_data, fd->b_tx, len);
		xSemaphoreGiveFromISR(fd->s_tx, &xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
		int ret = usart_soc_send(fd->port, fd->tx_data, len, usart_send_done, fd);
		configASSERT(ret == 0);
	}
	else
		fd->usart_tx_busy = 0;
}

static void usart_recv_done(void *data, int result) {
	BaseType_t xHigherPriorityTaskWoken;
	usart_handle_t fd = (usart_handle_t)data;

	ringbuf_memcpy_into(fd->b_rx, fd->rx_data, result);
	xSemaphoreGiveFromISR(fd->s_rx, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	if(fd->rx_cnt)
		fd->rx_cnt -= result;
	int ret = usart_soc_recv(fd->port, fd->rx_data, fd->rx_cnt <= 0 ? 1 : fd->rx_cnt, usart_recv_done, fd);
	configASSERT(ret == 0);
}
