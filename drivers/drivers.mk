DRIVERS_DIR = $(ROOT_DIR)/drivers

ifeq ($(CONFIG_ENCODER),'y')
    CONFIG_GPIO ='y'
    CONFIG_SOC_ENCODER = 'y'
    TARGET_OBJ += $(DRIVERS_DIR)/encoder/encoder.o
    CFLAGS += -I$(DRIVERS_DIR)/encoder
endif   

ifeq ($(CONFIG_SERVO), 'y')
    CONFIG_GPIO ='y'
    CONFIG_SOC_PWM ='y'
    TARGET_OBJ += $(DRIVERS_DIR)/servo/servo.o  
    CFLAGS += -I$(DRIVERS_DIR)/servo
endif

ifeq ($(CONFIG_MC33926), 'y')
  CONFIG_GPIO ='y'
  CONFIG_SOC_PWM ='y'
  TARGET_OBJ += $(DRIVERS_DIR)/MC33926/mc33926.o
  CFLAGS += -I$(DRIVERS_DIR)/MC33926
endif 

ifeq ($(CONFIG_DRV8835), 'y')
  CONFIG_GPIO ='y'
  CONFIG_SOC_PWM ='y'
  TARGET_OBJ += $(DRIVERS_DIR)/DRV8835/drv8835.o
  CFLAGS += -I$(DRIVERS_DIR)/DRV8835
endif 

ifeq ($(CONFIG_LSM303D), 'y')
    CONFIG_I2C = 'y'
    TARGET_OBJ += $(DRIVERS_DIR)/LSM303D/acc_mag_LSM303D.o
    CFLAGS += -I$(DRIVERS_DIR)/LSM303D
endif

ifeq ($(CONFIG_L3GD20H), 'y')
    CONFIG_I2C = 'y'
    TARGET_OBJ += $(DRIVERS_DIR)/L3GD20H/l3gd20h.o
    CFLAGS += -I$(DRIVERS_DIR)/L3GD20H
endif

ifeq ($(CONFIG_I2C),'y')
    CONFIG_GPIO ='y'
    CONFIG_SOC_I2C = 'y'
    TARGET_OBJ += $(DRIVERS_DIR)/i2c/i2c_master.o
    CFLAGS += -I$(DRIVERS_DIR)/i2c
endif

ifeq ($(CONFIG_SPI),'y')
    CONFIG_GPIO ='y'
    CONFIG_SOC_SPI = 'y'
    TARGET_OBJ += $(DRIVERS_DIR)/spi/spi.o
    CFLAGS += -I$(DRIVERS_DIR)/spi
endif

ifeq ($(CONFIG_USART),'y')
    CONFIG_GPIO ='y'
    CONFIG_SOC_USART = 'y'
    CONFIG_TOOLS_RINGBUF = 'y'
    TARGET_OBJ += $(DRIVERS_DIR)/usart/usart.o
    CFLAGS += -I$(DRIVERS_DIR)/usart
endif

ifeq ($(CONFIG_GPIO),'y')
    CONFIG_SOC_GPIO = 'y'
    TARGET_OBJ += $(DRIVERS_DIR)/gpio/gpio.o
    CFLAGS += -I$(DRIVERS_DIR)/gpio
endif

ifeq ($(CONFIG_WS2812B),'y')
    CONFIG_SOC_PWM = 'y'
    CONFIG_SOC_DMA = 'y'
    CONFIG_SOC_WS2812B = 'y'
    TARGET_OBJ += $(DRIVERS_DIR)/WS2812B/ws2812b.o
    CFLAGS += -I$(DRIVERS_DIR)/WS2812B
endif

ifeq ($(CONFIG_FLASH),'y')
    CONFIG_SOC_FLASH = 'y'
    TARGET_OBJ += $(DRIVERS_DIR)/flash/flash.o
    CFLAGS += -I$(DRIVERS_DIR)/flash
endif

include $(ROOT_DIR)/drivers/portable/soc/$(SOC_FAMILY)/soc.mk
include $(ROOT_DIR)/drivers/portable/bsp/$(BOARD_TYPE)/bsp.mk
