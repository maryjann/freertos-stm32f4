/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "servo.h"

#define PERIOD_MS 20
#define	FREQUENCY_HZ (1000/PERIOD_MS)
#define RESOLUTION	8192

#define MIN_POSITION ((RESOLUTION/PERIOD_MS))			//1ms pulse for 
#define MAX_POSITION (2*(RESOLUTION/PERIOD_MS))
#define NEUTRAL_POSITION (1.5*(RESOLUTION/PERIOD_MS))

int servo_init(pwm_timer_t timer) {
	int ret = pwm_soc_init(timer, FREQUENCY_HZ, RESOLUTION);
	pwm_soc_set_duty_cycle(timer, NEUTRAL_POSITION);
	return ret;
}

int servo_set_position(pwm_timer_t timer, int percent) {
	if(percent < -100 || percent > 100)
		return -1;
	int position = NEUTRAL_POSITION;
	if(percent <  0) {
		position += (NEUTRAL_POSITION - MIN_POSITION) * ((float)percent/(float)100);
	} else {
		position += (MAX_POSITION - NEUTRAL_POSITION) * ((float)percent/(float)100);
	}
	pwm_soc_set_duty_cycle(timer, position);
	return 0;
}

int servo_get_position(pwm_timer_t timer) {
	return 0;
}
