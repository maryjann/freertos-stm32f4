/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FreeRTOS.h>

#include "gpio.h"

#include "leds.h"

void leds_init(uint32_t leds)
{
    if(leds & LEDS_GREEN)
        gpio_init(LED_1, G_MODE_OUTPUT_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL);
}

void leds_deinit(uint32_t leds)
{
    gpio_deinit(LED_1);
}

void leds_set(uint32_t leds, led_state_t state)
{
    if(leds & LEDS_GREEN)
    {
        gpio_write_pin(LED_1, state);
    }
}

led_state_t leds_get(uint32_t leds)
{
    uint32_t state;
    if(leds & LEDS_GREEN)
    {
        state = gpio_read_pin(LED_1);
    }
    
    return state ? LED_ON : LED_OFF;
}

void leds_toggle( uint32_t leds )
{
    if(leds & LEDS_GREEN)
    {
        gpio_toggle_pin(LED_1);
    }
}

void leds_on( uint32_t leds )
{
    return leds_set( leds, LED_ON );
}

void leds_off( uint32_t leds )
{
    return leds_set( leds, LED_OFF );
}

