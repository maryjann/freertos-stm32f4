/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "stm32f4xx_hal_conf.h"
#include "gpio_soc.h"
#include "bsp_cfg.h"

#ifdef I2C1_SCL_CN10_17
	#define I2C1_SCL_PIN CN10_17
#endif

#ifdef I2C1_SCL_CN10_3
	#define I2C1_SCL_PIN CN10_3
#endif

#ifdef I2C1_SDA_CN7_21
	#define I2C1_SDA_PIN CN7_21
#endif

#ifdef I2C1_SDA_CN10_5
	#define I2C1_SDA_PIN CN10_5
#endif

#ifdef I2C3_SCL_CN10_23
	#define I2C3_SCL_PIN CN10_23
#endif

#ifdef I2C3_SDA_CN10_27
	#define I2C3_SDA_PIN CN10_27
#endif

#ifdef I2C3_SDA_CN10_1
	#define I2C3_SDA_PIN CN10_1
#endif

#ifdef I2C2_SCL_CN10_25
	#define I2C2_SCL_PIN CN10_25
#endif

#ifdef I2C2_SDA_CN10_31
	#define I2C2_SDA_PIN CN10_31
#endif

#ifdef I2C2_SDA_CN10_18
	#define I2C2_SDA_PIN CN10_18
#endif

#ifdef I2C2_SDA_CN7_3
	#define I2C2_SDA_PIN CN7_3
#endif

#define I2C1_IRQ_PRIORITY 5
#define I2C2_IRQ_PRIORITY 5
#define I2C3_IRQ_PRIORITY 5

void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c)
{
	if (hi2c->Instance == I2C1)
	{
	#if defined I2C1_SCL_PIN
		gpio_soc_init_alternate(I2C1_SCL_PIN, G_MODE_ALTERNATE_OD, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF4_I2C1);
	#endif

	#if defined I2C1_SDA_PIN
		gpio_soc_init_alternate(I2C1_SDA_PIN, G_MODE_ALTERNATE_OD, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF4_I2C1);
	#endif

		__HAL_RCC_I2C1_CLK_ENABLE( );

        HAL_NVIC_SetPriority(I2C1_ER_IRQn, I2C1_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C1_ER_IRQn);
        HAL_NVIC_SetPriority(I2C1_EV_IRQn, I2C1_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);
	}
	else if (hi2c->Instance == I2C2)
	{
    #if defined I2C2_SCL_PIN
		gpio_soc_init_alternate(I2C2_SCL_PIN, G_MODE_ALTERNATE_OD, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF4_I2C2);
	#endif

	#if defined I2C2_SDA_PIN
		gpio_soc_init_alternate(I2C2_SDA_PIN, G_MODE_ALTERNATE_OD, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF4_I2C2);
	#endif

        __HAL_RCC_I2C2_CLK_ENABLE( );

        HAL_NVIC_SetPriority(I2C2_ER_IRQn, I2C2_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C2_ER_IRQn);
        HAL_NVIC_SetPriority(I2C2_EV_IRQn, I2C2_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C2_EV_IRQn);
	}
	else if (hi2c->Instance == I2C2)
	{
	#if defined I2C3_SCL_PIN
		gpio_soc_init_alternate(I2C3_SCL_PIN, G_MODE_ALTERNATE_OD, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF4_I2C3);
	#endif

	#if defined I2C3_SDA_PIN
		gpio_soc_init_alternate(I2C3_SDA_PIN, G_MODE_ALTERNATE_OD, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF4_I2C3);
	#endif

		__HAL_RCC_I2C3_CLK_ENABLE( );

        HAL_NVIC_SetPriority(I2C3_ER_IRQn, I2C3_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C3_ER_IRQn);
        HAL_NVIC_SetPriority(I2C3_EV_IRQn, I2C3_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C3_EV_IRQn);
	}
}