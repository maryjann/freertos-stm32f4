/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "stm32f4xx_hal_conf.h"

#include "gpio_soc.h"
#include "bsp_cfg.h"

#ifdef UART_STLINK_UART_TX
	#define UART_USART2_TX_PIN STLINK_UART_TX
#endif

#ifdef UART_STLINK_UART_RX
	#define UART_USART2_RX_PIN STLINK_UART_RX
#endif

#ifdef UART_USART1_TX_CN10_21 //AF7
	#define UART_USART1_TX_PIN CN10_21
#endif

#ifdef UART_USART1_RX_CN7_21 //AF7
	#define UART_USART1_RX_PIN CN7_21
#endif

#ifdef UART_USART1_RX_CN10_33 //AF7
	#define UART_USART1_RX_PIN CN10_33
#endif

#ifdef UART_USART2_TX_CN10_35
	#define UART_USART2_TX_PIN CN10_35
#endif

#ifdef UART_USART2_RX_CN10_37
	#define UART_USART2_RX_PIN CN10_37
#endif

#ifdef UART_USART3_TX_CN10_25 //AF7
	#define UART_USART3_TX_PIN CN10_25
#endif

#ifdef UART_USART3_TX_CN7_1 //AF7
	#define UART_USART3_TX_PIN CN7_1
#endif

#ifdef UART_USART3_RX_CN7_2 //AF7
	#define UART_USART3_RX_PIN CN7_2
#endif

#ifdef UART_UART4_TX_CN7_1 //AF8
	#define UART_UART4_TX_PIN CN7_1
#endif

#ifdef UART_UART4_RX_CN7_2 //AF8
	#define UART_UART4_RX_PIN CN7_2
#endif

#ifdef UART_UART4_TX_CN7_28 //AF8
	#define UART_UART4_TX_PIN CN7_28
#endif

#ifdef UART_UART4_RX_CN7_30 //AF8
	#define UART_UART4_RX_PIN CN7_30
#endif

#ifdef UART_UART5_TX_CN7_3 //AF8
	#define UART_UART5_TX_PIN CN7_3
#endif

#ifdef UART_UART5_RX_CN7_4 //AF8
	#define UART_UART5_RX_PIN CN7_4
#endif

#ifdef UART_USART1_TX_CN10_17 //AF7
	#define UART_USART1_TX_PIN CN10_17
#endif

#ifdef UART_USART6_RX_CN10_19 //AF7
	#define UART_USART6_RX_PIN CN10_19
#endif


#define UART_IRQ_PRIORITY 5

void HAL_UART_MspInit(UART_HandleTypeDef *huart) {
	if (huart == NULL) {
		return;
	}

	if (huart->Instance == USART1) {
		__HAL_RCC_USART1_CLK_ENABLE();
		#ifdef UART_USART1_TX_PIN
			gpio_soc_init_alternate(UART_USART1_TX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_USART1);
		#endif
		#ifdef UART_USART1_RX_PIN
			gpio_soc_init_alternate(UART_USART1_RX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_USART1);
		#endif
		HAL_NVIC_SetPriority(USART1_IRQn, UART_IRQ_PRIORITY, 0);
		HAL_NVIC_EnableIRQ(USART1_IRQn);
	}
	else if (huart->Instance == USART2) {
		__HAL_RCC_USART2_CLK_ENABLE();
		#ifdef UART_USART2_TX_PIN
			gpio_soc_init_alternate(UART_USART2_TX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_USART2);
		#endif
		#ifdef UART_USART2_RX_PIN
			gpio_soc_init_alternate(UART_USART2_RX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_USART2);
		#endif
		HAL_NVIC_SetPriority(USART2_IRQn, UART_IRQ_PRIORITY, 0);
		HAL_NVIC_EnableIRQ(USART2_IRQn);
	}
	else if (huart->Instance == USART3) {
		__HAL_RCC_USART3_CLK_ENABLE();
		#ifdef UART_USART3_TX_PIN
			gpio_soc_init_alternate(UART_USART3_TX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_USART3);
		#endif
		#ifdef UART_USART3_RX_PIN
			gpio_soc_init_alternate(UART_USART3_RX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_USART3);
		#endif
		HAL_NVIC_SetPriority(USART3_IRQn, UART_IRQ_PRIORITY, 0);
		HAL_NVIC_EnableIRQ(USART3_IRQn);
	}
	else if (huart->Instance == UART4) {
		__HAL_RCC_USART3_CLK_ENABLE();
		#ifdef UART_UART4_TX_PIN
			gpio_soc_init_alternate(UART_UART4_TX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_UART4);
		#endif
		#ifdef UART_UART4_RX_PIN
			gpio_soc_init_alternate(UART_UART4_RX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_UART4);
		#endif
		HAL_NVIC_SetPriority(UART4_IRQn, UART_IRQ_PRIORITY, 0);
		HAL_NVIC_EnableIRQ(UART4_IRQn);
	}
	else if (huart->Instance == UART5) {
		__HAL_RCC_USART3_CLK_ENABLE();
		#ifdef UART_UART5_TX_PIN
			gpio_soc_init_alternate(UART_UART5_TX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_UART5);
		#endif
		#ifdef UART_UART5_RX_PIN
			gpio_soc_init_alternate(UART_UART5_RX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF7_UART5);
		#endif
		HAL_NVIC_SetPriority(UART5_IRQn, UART_IRQ_PRIORITY, 0);
		HAL_NVIC_EnableIRQ(UART5_IRQn);
	}
	else if (huart->Instance == USART6) {
		#ifdef UART_USART6_TX_PIN
			gpio_soc_init_alternate(UART_USART6_TX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF8_USART6);
		#endif
		#ifdef UART_USART6_RX_PIN
			gpio_soc_init_alternate(UART_USART6_RX_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF8_USART6);
		#endif
		__HAL_RCC_USART6_CLK_ENABLE();
		HAL_NVIC_SetPriority(USART6_IRQn, UART_IRQ_PRIORITY, 0);
		HAL_NVIC_EnableIRQ(USART6_IRQn);
	}
}
