/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "stm32f4xx_hal_conf.h"

#include "encoder_soc.h"
#include "gpio_soc.h"
#include "bsp_cfg.h"

#ifdef ENCODER_TIMER_1_CH1_CN10_23
	#define TIMER_1_CH1_PIN CN10_23
#endif

#ifdef ENCODER_TIMER_1_CH2_CN10_21
	#define TIMER_1_CH2_PIN CN10_21
#endif

#ifdef ENCODER_TIMER_2_CH1_CN7_17
	#define TIMER_2_CH1_PIN CN7_17
#endif

#ifdef ENCODER_TIMER_2_CH1_CN7_28
	#define TIMER_2_CH1_PIN CN7_28
#endif

#ifdef ENCODER_TIMER_2_CH1_CN10_3
	#define TIMER_2_CH1_PIN CN10_3
#endif

#ifdef ENCODER_TIMER_2_CH1_CN10_11
	#define TIMER_2_CH1_PIN CN10_11
#endif

#ifdef ENCODER_TIMER_2_CH2_CN7_30
	#define TIMER_2_CH2_PIN CN7_30
#endif

#ifdef ENCODER_TIMER_2_CH2_CN10_5
	#define TIMER_2_CH2_PIN CN10_5
#endif

#ifdef ENCODER_TIMER_2_CH2_CN10_31
	#define TIMER_2_CH2_PIN CN10_31
#endif

#ifdef ENCODER_TIMER_3_CH1_CN10_27
	#define TIMER_3_CH1_PIN CN10_27
#endif

#ifdef ENCODER_TIMER_3_CH1_CN10_13
	#define TIMER_3_CH1_PIN CN10_13
#endif

#ifdef ENCODER_TIMER_3_CH1_CN10_4
	#define TIMER_3_CH1_PIN CN10_4
#endif

#ifdef ENCODER_TIMER_3_CH2_CN10_15
	#define TIMER_3_CH2_PIN CN10_15
#endif

#ifdef ENCODER_TIMER_3_CH2_CN10_19
	#define TIMER_3_CH2_PIN CN10_19
#endif

#ifdef ENCODER_TIMER_3_CH2_CN10_29
	#define TIMER_3_CH2_PIN CN10_29
#endif

#ifdef ENCODER_TIMER_4_CH1_CN10_17
	#define TIMER_4_CH1_PIN CN10_17
#endif

#ifdef ENCODER_TIMER_4_CH2_CN7_21
	#define TIMER_4_CH2_PIN CN7_21
#endif

#ifdef ENCODER_TIMER_5_CH1_CN7_28
	#define TIMER_5_CH1_PIN CN7_28
#endif

#ifdef ENCODER_TIMER_5_CH2_CN7_30
	#define TIMER_5_CH2_PIN CN7_30
#endif

#ifdef ENCODER_TIMER_8_CH1_CN10_4
	#define TIMER_8_CH1_PIN CN10_4
#endif

#ifdef ENCODER_TIMER_8_CH2_CN10_19
	#define TIMER_8_CH2_PIN CN10_19
#endif

uint32_t encoder_bsp_get_mode(encoder_timer_t timer) {
	if (timer >= ENCODER_TIMER_END) {
		return 0;
	}

	uint32_t mode = 0;

	if (timer == ENCODER_TIMER_1) {
		#if defined TIMER_1_CH1_PIN
			mode |= TIM_ENCODERMODE_TI1;
		#endif

		#if defined TIMER_1_CH2_PIN
			mode |= TIM_ENCODERMODE_TI2;
		#endif
	}

	else if (timer == ENCODER_TIMER_2) {
		#if defined TIMER_2_CH1_PIN
			mode |= TIM_ENCODERMODE_TI1;
		#endif

		#if defined TIMER_2_CH2_PIN
			mode |= TIM_ENCODERMODE_TI2;
		#endif
	} 
	
	else if (timer == ENCODER_TIMER_3) {
		#if defined TIMER_3_CH1_PIN
			mode |= TIM_ENCODERMODE_TI1;
		#endif

		#if defined TIMER_3_CH2_PIN
			mode |= TIM_ENCODERMODE_TI2;
		#endif
	}
	
	else if (timer == ENCODER_TIMER_4) {
		#if defined TIMER_4_CH1_PIN
			mode |= TIM_ENCODERMODE_TI1;
		#endif

		#if defined TIMER_4_CH2_PIN
			mode |= TIM_ENCODERMODE_TI2;
		#endif
	}
	
	else if (timer == ENCODER_TIMER_5) {
		#if defined TIMER_5_CH1_PIN
			mode |= TIM_ENCODERMODE_TI1;
		#endif

		#if defined TIMER_5_CH2_PIN
			mode |= TIM_ENCODERMODE_TI2;
		#endif
	}

	else if (timer == ENCODER_TIMER_8) {
		#if defined TIMER_8_CH1_PIN
			mode |= TIM_ENCODERMODE_TI1;
		#endif

		#if defined TIMER_8_CH2_PIN
			mode |= TIM_ENCODERMODE_TI2;
		#endif
	}
	
	return mode;
}

void HAL_TIM_Encoder_MspInit(TIM_HandleTypeDef *htim) {

	if (htim->Instance == TIM1) {
		__HAL_RCC_TIM1_CLK_ENABLE();
		#if defined TIMER_1_CH1_PIN
			gpio_soc_init_alternate(TIMER_1_CH1_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM1);
		#endif
		#if defined TIMER_1_CH2_PIN
			gpio_soc_init_alternate(TIMER_1_CH2_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM1);
		#endif
	}

	else if (htim->Instance == TIM2) {
		__HAL_RCC_TIM2_CLK_ENABLE();
		#if defined TIMER_2_CH1_PIN
			gpio_soc_init_alternate(TIMER_2_CH1_PIN, G_MODE_ALTERNATE_OD, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM2);
		#endif

		#if defined TIMER_2_CH2_PIN
			gpio_soc_init_alternate(TIMER_2_CH2_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM2);
		#endif
	}

	else if (htim->Instance == TIM3) {
		__HAL_RCC_TIM3_CLK_ENABLE();
		#if defined TIMER_3_CH1_PIN
			gpio_soc_init_alternate(TIMER_3_CH1_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM3);
		#endif

		#if defined TIMER_3_CH2_PIN
			gpio_soc_init_alternate(TIMER_3_CH2_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM3);
		#endif
	}

	else if (htim->Instance == TIM4) {
		__HAL_RCC_TIM4_CLK_ENABLE();
		#if defined TIMER_4_CH1_PIN
			gpio_soc_init_alternate(TIMER_4_CH1_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM4);
		#endif

		#if defined TIMER_4_CH2_PIN
			gpio_soc_init_alternate(TIMER_4_CH2_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM4);
		#endif
	}

	else if (htim->Instance == TIM5) {
		__HAL_RCC_TIM5_CLK_ENABLE();
		#if defined TIMER_5_CH1_PIN
			gpio_soc_init_alternate(TIMER_5_CH1_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM5);
		#endif
	}

	else if (htim->Instance == TIM8) {
		__HAL_RCC_TIM8_CLK_ENABLE();
		#if defined TIMER_8_CH1_PIN
			gpio_soc_init_alternate(TIMER_8_CH1_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
		#endif
		#if defined TIMER_8_CH2_PIN
			gpio_soc_init_alternate(TIMER_8_CH2_PIN, G_MODE_ALTERNATE_OD, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
		#endif
	}
}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim) {
	HAL_TIM_Encoder_MspInit(htim);
}

#if defined TIMER_1_CH1_PIN || \
	defined TIMER_1_CH2_PIN

void TIM1_UP_TIM10_IRQHandler(void) {
	encoder_soc_handle_irq(ENCODER_TIMER_1);
}

#endif


#if defined TIMER_2_CH1_PIN || \
	defined TIMER_2_CH2_PIN

void TIM2_IRQHandler(void) {
	encoder_soc_handle_irq(ENCODER_TIMER_2);
}

#endif

#if defined TIMER_3_CH1_PIN || \
	defined TIMER_3_CH2_PIN

void TIM3_IRQHandler(void) {
	encoder_soc_handle_irq(ENCODER_TIMER_3);
}

#endif

#if defined TIMER_4_CH1_PIN || \
	defined TIMER_4_CH2_PIN

void TIM4_IRQHandler(void) {
	encoder_soc_handle_irq(ENCODER_TIMER_4);
}

#endif

#if defined TIMER_5_CH1_PIN || \
	defined TIMER_5_CH2_PIN

void TIM5_IRQHandler(void) {
	encoder_soc_handle_irq(ENCODER_TIMER_5);
}

#endif

#if defined TIMER_8_CH1_PIN || \
	defined TIMER_8_CH2_PIN

void TIM8_UP_TIM13_IRQHandler(void) {
	encoder_soc_handle_irq(ENCODER_TIMER_8);
}

#endif