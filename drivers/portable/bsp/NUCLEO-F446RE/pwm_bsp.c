/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "stm32f4xx_hal_conf.h"

#include "pwm_soc.h"
#include "gpio_soc.h"
#include "bsp_cfg.h"

#ifdef PWM_TIMER_1_CH1_CN10_23
	#define TIMER_1_CH1_PIN CN10_23
#endif

#ifdef PWM_TIMER_1_CH2_CN10_21
	#define TIMER_1_CH2_PIN CN10_21
#endif

#ifdef PWM_TIMER_1_CH3_CN10_33
	#define TIMER_1_CH3_PIN CN10_33
#endif

#ifdef PWM_TIMER_1_CH4_CN10_14
	#define TIMER_1_CH4_PIN CN10_14
#endif

#ifdef PWM_TIMER_1_CH1N_CN10_15
	#define TIMER_1_CH1N_PIN CN10_15
#endif

#ifdef PWM_TIMER_1_CH1N_CN10_30
	#define TIMER_1_CH1N_PIN CN10_30
#endif

#ifdef PWM_TIMER_1_CH2N_CN7_34
	#define TIMER_1_CH2N_PIN CN7_34
#endif

#ifdef PWM_TIMER_1_CH2N_CN10_28
	#define TIMER_1_CH2N_PIN CN10_28
#endif

#ifdef PWM_TIMER_1_CH3N_CN10_24
	#define TIMER_1_CH3N_PIN CN10_24
#endif

#ifdef PWM_TIMER_1_CH3N_CN10_26
	#define TIMER_1_CH3N_PIN CN10_26
#endif

#ifdef PWM_TIMER_2_CH1_CN7_17
	#define TIMER_2_CH1_PIN CN7_17
#endif

#ifdef PWM_TIMER_2_CH1_CN7_28
	#define TIMER_2_CH1_PIN CN7_28
#endif

#ifdef PWM_TIMER_2_CH1_CN10_3
	#define TIMER_2_CH1_PIN CN10_3
#endif

#ifdef PWM_TIMER_2_CH1_CN10_11
	#define TIMER_2_CH1_PIN CN10_11
#endif

#ifdef PWM_TIMER_2_CH2_CN7_30
	#define TIMER_2_CH2_PIN CN7_30
#endif

#ifdef PWM_TIMER_2_CH2_CN10_5
	#define TIMER_2_CH2_PIN CN10_5
#endif

#ifdef PWM_TIMER_2_CH2_CN10_31
	#define TIMER_2_CH2_PIN CN10_31
#endif

#ifdef PWM_TIMER_2_CH3_CN10_25
	#define TIMER_2_CH3_PIN CN10_25
#endif

#ifdef PWM_TIMER_2_CH3_CN10_35
	#define TIMER_2_CH3_PIN CN10_35
#endif

#ifdef PWM_TIMER_2_CH4_CN10_37
	#define TIMER_2_CH4_PIN CN10_37
#endif

#ifdef PWM_TIMER_2_CH4_CN10_22
	#define TIMER_2_CH4_PIN CN10_22
#endif

#ifdef PWM_TIMER_3_CH1_CN10_27
	#define TIMER_3_CH1_PIN CN10_27
#endif

#ifdef PWM_TIMER_3_CH1_CN10_13
	#define TIMER_3_CH1_PIN CN10_13
#endif

#ifdef PWM_TIMER_3_CH1_CN10_4
	#define TIMER_3_CH1_PIN CN10_4
#endif

#ifdef PWM_TIMER_3_CH2_CN10_15
	#define TIMER_3_CH2_PIN CN10_15
#endif

#ifdef PWM_TIMER_3_CH2_CN10_19
	#define TIMER_3_CH2_PIN CN10_19
#endif

#ifdef PWM_TIMER_3_CH2_CN10_29
	#define TIMER_3_CH2_PIN CN10_29
#endif

#ifdef PWM_TIMER_3_CH3_CN7_34
	#define TIMER_3_CH3_PIN CN7_34
#endif

#ifdef PWM_TIMER_3_CH3_CN10_2
	#define TIMER_3_CH3_PIN CN10_2
#endif

#ifdef PWM_TIMER_3_CH4_CN10_1
	#define TIMER_3_CH4_PIN CN10_1
#endif

#ifdef PWM_TIMER_3_CH4_CN10_24
	#define TIMER_3_CH4_PIN CN10_24
#endif

#ifdef PWM_TIMER_4_CH1_CN10_17
	#define TIMER_4_CH1_PIN CN10_17
#endif

#ifdef PWM_TIMER_4_CH2_CN7_21
	#define TIMER_4_CH2_PIN CN7_21
#endif

#ifdef PWM_TIMER_4_CH3_CN10_3
	#define TIMER_4_CH3_PIN CN10_3
#endif

#ifdef PWM_TIMER_4_CH4_CN10_5
	#define TIMER_4_CH4_PIN CN10_5
#endif

#ifdef PWM_TIMER_5_CH1_CN7_28
	#define TIMER_5_CH1_PIN CN7_28
#endif

#ifdef PWM_TIMER_5_CH2_CN7_30
	#define TIMER_5_CH2_PIN CN7_30
#endif

#ifdef PWM_TIMER_5_CH3_CN10_35
	#define TIMER_5_CH3_PIN CN10_35
#endif

#ifdef PWM_TIMER_5_CH4_CN10_37
	#define TIMER_5_CH4_PIN CN10_37
#endif

#ifdef PWM_TIMER_8_CH1_CN10_4
	#define TIMER_8_CH1_PIN CN10_4
#endif

#ifdef PWM_TIMER_8_CH2_CN10_19
	#define TIMER_8_CH2_PIN CN10_19
#endif

#ifdef PWM_TIMER_8_CH3_CN10_2
	#define TIMER_8_CH3_PIN CN10_2
#endif

#ifdef PWM_TIMER_8_CH4_CN10_1
	#define TIMER_8_CH4_PIN CN10_1
#endif

#ifdef PWM_TIMER_8_CH1N_CN10_15
	#define TIMER_8_CH1N_PIN CN10_15
#endif

#ifdef PWM_TIMER_8_CH1N_CN10_11
	#define TIMER_8_CH1N_PIN CN10_11
#endif

#ifdef PWM_TIMER_8_CH2N_CN7_34
	#define TIMER_8_CH2N_PIN CN7_34
#endif

#ifdef PWM_TIMER_8_CH2N_CN10_28
	#define TIMER_8_CH2N_PIN CN10_28
#endif

#ifdef PWM_TIMER_8_CH3N_CN10_24
	#define TIMER_8_CH3N_PIN CN10_24
#endif

#ifdef PWM_TIMER_8_CH3N_CN10_26
	#define TIMER_8_CH3N_PIN CN10_26
#endif

#ifdef PWM_TIMER_9_CH1_CN10_35
	#define TIMER_9_CH1_PIN CN10_35
#endif

#ifdef PWM_TIMER_9_CH2_CN10_37
	#define TIMER_9_CH2_PIN CN10_37
#endif

#ifdef PWM_TIMER_10_CH1_CN10_3
	#define TIMER_10_CH1_PIN CN10_3
#endif

#ifdef PWM_TIMER_11_CH1_CN10_5
	#define TIMER_11_CH1_PIN CN10_5
#endif

#ifdef PWM_TIMER_12_CH1_CN10_28
	#define TIMER_12_CH1_PIN CN10_28
#endif

#ifdef PWM_TIMER_12_CH2_CN10_26
	#define TIMER_12_CH2_PIN CN10_26
#endif

#ifdef PWM_TIMER_13_CH1_CN10_13
	#define TIMER_13_CH1_PIN CN10_13
#endif

#ifdef PWM_TIMER_14_CH1_CN10_15
	#define TIMER_14_CH1_PIN CN10_15
#endif

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim) {
	if (htim == NULL) {
		return;
	}
	
	pwm_handle_t *pwm_handle = (pwm_handle_t *)htim;

	if (htim->Instance == TIM1) {
		__HAL_RCC_TIM1_CLK_ENABLE();

		#if defined TIMER_1_CH1_PIN
			gpio_soc_init_alternate(TIMER_1_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM1);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_1_CH2_PIN
			gpio_soc_init_alternate(TIMER_1_CH2_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM1);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_1_CH3_PIN
			gpio_soc_init_alternate(TIMER_1_CH3_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM1);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_1_CH4_PIN
			gpio_soc_init_alternate(TIMER_1_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM1);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_1_CH1N_PIN
			gpio_soc_init_alternate(TIMER_1_CH4_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM1);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_1_CH2N_PIN
			gpio_soc_init_alternate(TIMER_1_CH2N_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM1);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_1_CH3N_PIN
			gpio_soc_init_alternate(TIMER_1_CH3N_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM1);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
	}

	if (htim->Instance == TIM2) {
		__HAL_RCC_TIM2_CLK_ENABLE();

		#if defined TIMER_2_CH1_PIN
			gpio_soc_init_alternate(TIMER_2_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM2);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_2_CH2_PIN
			gpio_soc_init_alternate(TIMER_2_CH2_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM2);
			pwm_handle->channel = TIM_CHANNEL_2;
		#endif

		#if defined TIMER_2_CH3_PIN
			gpio_soc_init_alternate(TIMER_2_CH3_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM2);
			pwm_handle->channel = TIM_CHANNEL_3;
		#endif

		#if defined TIMER_2_CH4_PIN
			gpio_soc_init_alternate(TIMER_2_CH4_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF1_TIM2);
			pwm_handle->channel = TIM_CHANNEL_4;
		#endif
	}

	if (htim->Instance == TIM3) {
		__HAL_RCC_TIM3_CLK_ENABLE();

		#if defined TIMER_3_CH1_PIN
			gpio_soc_init_alternate(TIMER_3_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM3);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_3_CH2_PIN
			gpio_soc_init_alternate(TIMER_3_CH2_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM3);
			pwm_handle->channel = TIM_CHANNEL_2;
		#endif

		#if defined TIMER_3_CH3_PIN
			gpio_soc_init_alternate(TIMER_3_CH3_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM3);
			pwm_handle->channel = TIM_CHANNEL_3;
		#endif

		#if defined TIMER_3_CH4_PIN
			gpio_soc_init_alternate(TIMER_3_CH4_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM3);
			pwm_handle->channel = TIM_CHANNEL_4;
		#endif
	}

	if (htim->Instance == TIM4) {
		__HAL_RCC_TIM4_CLK_ENABLE();

		#if defined TIMER_4_CH1_PIN
			gpio_soc_init_alternate(TIMER_4_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM4);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_4_CH2_PIN
			gpio_soc_init_alternate(TIMER_4_CH2_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM4);
			pwm_handle->channel = TIM_CHANNEL_2;
		#endif

		#if defined TIMER_4_CH3_PIN
			gpio_soc_init_alternate(TIMER_4_CH3_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM4);
			pwm_handle->channel = TIM_CHANNEL_3;
		#endif

		#if defined TIMER_4_CH4_PIN
			gpio_soc_init_alternate(TIMER_4_CH4_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM4);
			pwm_handle->channel = TIM_CHANNEL_4;
		#endif
	}

	if (htim->Instance == TIM5) {
		__HAL_RCC_TIM5_CLK_ENABLE();

		#if defined TIMER_5_CH1_PIN
			gpio_soc_init_alternate(TIMER_5_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM5);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_5_CH2_PIN
			gpio_soc_init_alternate(TIMER_5_CH2_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM5);
			pwm_handle->channel = TIM_CHANNEL_2;
		#endif

		#if defined TIMER_5_CH3_PIN
			gpio_soc_init_alternate(TIMER_5_CH3_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM5);
			pwm_handle->channel = TIM_CHANNEL_3;
		#endif

		#if defined TIMER_5_CH4_PIN
			gpio_soc_init_alternate(TIMER_5_CH4_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF2_TIM5);
			pwm_handle->channel = TIM_CHANNEL_4;
		#endif
	}

	if (htim->Instance == TIM8) {
		__HAL_RCC_TIM8_CLK_ENABLE();

		#if defined TIMER_8_CH1_PIN
			gpio_soc_init_alternate(TIMER_8_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_8_CH2_PIN
			gpio_soc_init_alternate(TIMER_8_CH2_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
			pwm_handle->channel = TIM_CHANNEL_2;
		#endif

		#if defined TIMER_8_CH3_PIN
			gpio_soc_init_alternate(TIMER_8_CH3_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
			pwm_handle->channel = TIM_CHANNEL_3;
		#endif

		#if defined TIMER_8_CH4_PIN
			gpio_soc_init_alternate(TIMER_8_CH4_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
			pwm_handle->channel = TIM_CHANNEL_4;
		#endif

		#if defined TIMER_8_CH1N_PIN
			gpio_soc_init_alternate(TIMER_8_CH1N_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_8_CH2N_PIN
			gpio_soc_init_alternate(TIMER_8_CH2N_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
			pwm_handle->channel = TIM_CHANNEL_2;
		#endif

		#if defined TIMER_8_CH3N_PIN
			gpio_soc_init_alternate(TIMER_8_CH3N_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
			pwm_handle->channel = TIM_CHANNEL_3;
		#endif
	}

	if (htim->Instance == TIM9) {
		__HAL_RCC_TIM8_CLK_ENABLE();

		#if defined TIMER_9_CH1_PIN
			gpio_soc_init_alternate(TIMER_9_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM9);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif

		#if defined TIMER_9_CH2_PIN
			gpio_soc_init_alternate(TIMER_9_CH2_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM9);
			pwm_handle->channel = TIM_CHANNEL_2;
		#endif
	}

	if (htim->Instance == TIM10) {
		__HAL_RCC_TIM10_CLK_ENABLE();

		#if defined TIMER_10_CH1_PIN
			gpio_soc_init_alternate(TIMER_10_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM10);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
	}

	if (htim->Instance == TIM11) {
		__HAL_RCC_TIM11_CLK_ENABLE();

		#if defined TIMER_11_CH1_PIN
			gpio_soc_init_alternate(TIMER_11_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM11);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
	}

	if (htim->Instance == TIM12) {
		__HAL_RCC_TIM12_CLK_ENABLE();

		#if defined TIMER_12_CH1_PIN
			gpio_soc_init_alternate(TIMER_12_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF9_TIM12);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
		#if defined TIMER_12_CH2_PIN
			gpio_soc_init_alternate(TIMER_12_CH2_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF9_TIM12);
			pwm_handle->channel = TIM_CHANNEL_2;
		#endif
	}

	if (htim->Instance == TIM13) {
		__HAL_RCC_TIM13_CLK_ENABLE();

		#if defined TIMER_13_CH1_PIN
			gpio_soc_init_alternate(TIMER_13_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF9_TIM13);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
	}

	if (htim->Instance == TIM14) {
		__HAL_RCC_TIM14_CLK_ENABLE();

		#if defined TIMER_14_CH1_PIN
			gpio_soc_init_alternate(TIMER_14_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF9_TIM14);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
	}
	
	return;
}