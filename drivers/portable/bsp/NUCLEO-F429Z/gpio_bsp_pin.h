/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __GPIO_BSP_PIN_H__
#define __GPIO_BSP_PIN_H__

#include <FreeRTOS.h>
#include "stm32f4xx_hal_conf.h"

#include "gpio_bsp_con.h"

#define CN8_2_PIN GPIO_PIN_8
#define CN8_2_PORT GPIOC

#define CN8_4_PIN GPIO_PIN_9
#define CN8_4_PORT GPIOC

#define CN8_6_PIN GPIO_PIN_10
#define CN8_6_PORT GPIOC

#define CN8_8_PIN GPIO_PIN_11
#define CN8_8_PORT GPIOC

#define CN8_10_PIN GPIO_PIN_12
#define CN8_10_PORT GPIOC

#define CN8_12_PIN GPIO_PIN_2
#define CN8_12_PORT GPIOD

#define CN8_14_PIN GPIO_PIN_2
#define CN8_14_PORT GPIOG

#define CN8_16_PIN GPIO_PIN_3
#define CN8_16_PORT GPIOG

#define CN9_1_PIN GPIO_PIN_3
#define CN9_1_PORT GPIOA

#define CN9_2_PIN GPIO_PIN_7
#define CN9_2_PORT GPIOD

#define CN9_3_PIN GPIO_PIN_0
#define CN9_3_PORT GPIOC

#define CN9_4_PIN GPIO_PIN_6
#define CN9_4_PORT GPIOD

#define CN9_5_PIN GPIO_PIN_3
#define CN9_5_PORT GPIOC

#define CN9_6_PIN GPIO_PIN_5
#define CN9_6_PORT GPIOD

#define CN9_7_PIN GPIO_PIN_3
#define CN9_7_PORT GPIOF

#define CN9_8_PIN GPIO_PIN_4
#define CN9_8_PORT GPIOD

#define CN9_9_PIN GPIO_PIN_5
#define CN9_9_PORT GPIOF

#define CN9_10_PIN GPIO_PIN_3
#define CN9_10_PORT GPIOD

#define CN9_11_PIN GPIO_PIN_10
#define CN9_11_PORT GPIOF

#define CN9_14_PIN GPIO_PIN_2
#define CN9_14_PORT GPIOE

#define CN9_15_PIN GPIO_PIN_7
#define CN9_15_PORT GPIOA

#define CN9_16_PIN GPIO_PIN_4
#define CN9_16_PORT GPIOE

#define CN9_17_PIN GPIO_PIN_2
#define CN9_17_PORT GPIOF

#define CN9_18_PIN GPIO_PIN_5
#define CN9_18_PORT GPIOE

#define CN9_19_PIN GPIO_PIN_1
#define CN9_19_PORT GPIOF

#define CN9_20_PIN GPIO_PIN_6
#define CN9_20_PORT GPIOE

#define CN9_21_PIN GPIO_PIN_0
#define CN9_21_PORT GPIOF

#define CN9_22_PIN GPIO_PIN_3
#define CN9_22_PORT GPIOE

#define CN9_24_PIN GPIO_PIN_8
#define CN9_24_PORT GPIOF

#define CN9_25_PIN GPIO_PIN_0
#define CN9_25_PORT GPIOD

#define CN9_26_PIN GPIO_PIN_7
#define CN9_26_PORT GPIOF

#define CN9_27_PIN GPIO_PIN_1
#define CN9_27_PORT GPIOD

#define CN9_28_PIN GPIO_PIN_9
#define CN9_28_PORT GPIOF

#define CN9_29_PIN GPIO_PIN_0
#define CN9_29_PORT GPIOG

#define CN9_30_PIN GPIO_PIN_1
#define CN9_30_PORT GPIOG

#define CN7_1_PIN GPIO_PIN_6
#define CN7_1_PORT GPIOC

#define CN7_2_PIN GPIO_PIN_8
#define CN7_2_PORT GPIOB

#define CN7_3_PIN GPIO_PIN_15
#define CN7_3_PORT GPIOB

#define CN7_4_PIN GPIO_PIN_9
#define CN7_4_PORT GPIOB

#define CN7_5_PIN GPIO_PIN_13
#define CN7_5_PORT GPIOB

#define CN7_7_PIN GPIO_PIN_12
#define CN7_7_PORT GPIOB

#define CN7_9_PIN GPIO_PIN_15
#define CN7_9_PORT GPIOA

#define CN7_10_PIN GPIO_PIN_5
#define CN7_10_PORT GPIOA

#define CN7_11_PIN GPIO_PIN_7
#define CN7_11_PORT GPIOC

#define CN7_12_PIN GPIO_PIN_6
#define CN7_12_PORT GPIOA

#define CN7_13_PIN GPIO_PIN_5
#define CN7_13_PORT GPIOB

#define CN7_14_PIN GPIO_PIN_7
#define CN7_14_PORT GPIOA

#define CN7_15_PIN GPIO_PIN_3
#define CN7_15_PORT GPIOB

#define CN7_16_PIN GPIO_PIN_14
#define CN7_16_PORT GPIOD

#define CN7_17_PIN GPIO_PIN_4
#define CN7_17_PORT GPIOA

#define CN7_18_PIN GPIO_PIN_15
#define CN7_18_PORT GPIOD

#define CN7_19_PIN GPIO_PIN_4
#define CN7_19_PORT GPIOB

#define CN7_20_PIN GPIO_PIN_12
#define CN7_20_PORT GPIOF

#define CN10_2_PIN GPIO_PIN_13
#define CN10_2_PORT GPIOF

#define CN10_4_PIN GPIO_PIN_9
#define CN10_4_PORT GPIOE

#define CN10_6_PIN GPIO_PIN_11
#define CN10_6_PORT GPIOE

#define CN10_7_PIN GPIO_PIN_1
#define CN10_7_PORT GPIOB

#define CN10_8_PIN GPIO_PIN_14
#define CN10_8_PORT GPIOF

#define CN10_9_PIN GPIO_PIN_2
#define CN10_9_PORT GPIOC

#define CN10_10_PIN GPIO_PIN_13
#define CN10_10_PORT GPIOE

#define CN10_11_PIN GPIO_PIN_4
#define CN10_11_PORT GPIOF

#define CN10_12_PIN GPIO_PIN_15
#define CN10_12_PORT GPIOF

#define CN10_13_PIN GPIO_PIN_6
#define CN10_13_PORT GPIOB

#define CN10_14_PIN GPIO_PIN_14
#define CN10_14_PORT GPIOG

#define CN10_15_PIN GPIO_PIN_2
#define CN10_15_PORT GPIOB

#define CN10_16_PIN GPIO_PIN_9
#define CN10_16_PORT GPIOG

#define CN10_18_PIN GPIO_PIN_8
#define CN10_18_PORT GPIOE

#define CN10_19_PIN GPIO_PIN_13
#define CN10_19_PORT GPIOD

#define CN10_20_PIN GPIO_PIN_7
#define CN10_20_PORT GPIOE

#define CN10_21_PIN GPIO_PIN_12
#define CN10_21_PORT GPIOD

#define CN10_23_PIN GPIO_PIN_11
#define CN10_23_PORT GPIOD

#define CN10_24_PIN GPIO_PIN_10
#define CN10_24_PORT GPIOE

#define CN10_25_PIN GPIO_PIN_2
#define CN10_25_PORT GPIOE

#define CN10_26_PIN GPIO_PIN_12
#define CN10_26_PORT GPIOE

#define CN10_28_PIN GPIO_PIN_14
#define CN10_28_PORT GPIOE

#define CN10_29_PIN GPIO_PIN_0
#define CN10_29_PORT GPIOA

#define CN10_30_PIN GPIO_PIN_15
#define CN10_30_PORT GPIOE

#define CN10_31_PIN GPIO_PIN_0
#define CN10_31_PORT GPIOB

#define CN10_32_PIN GPIO_PIN_10
#define CN10_32_PORT GPIOB

#define CN10_33_PIN GPIO_PIN_0
#define CN10_33_PORT GPIOE

#define CN10_34_PIN GPIO_PIN_11
#define CN10_34_PORT GPIOB

#define LED_1_PIN GPIO_PIN_0
#define LED_1_PORT GPIOB

#define LED_2_PIN GPIO_PIN_7
#define LED_2_PORT GPIOB

#define LED_3_PIN GPIO_PIN_14
#define LED_3_PORT GPIOB

#define USER_BUTTON_PIN GPIO_PIN_13
#define USER_BUTTON_PORT GPIOC

#define STLINK_UART_RX_PIN GPIO_PIN_8
#define STLINK_UART_RX_PORT GPIOD

#define STLINK_UART_TX_PIN GPIO_PIN_9
#define STLINK_UART_TX_PORT GPIOD

#define RMII_REF_CLK_PIN GPIO_PIN_1
#define RMII_REF_CLK_PORT GPIOA

#define RMII_MDIO_PIN GPIO_PIN_2
#define RMII_MDIO_PORT GPIOA

#define RMII_MDC_PIN GPIO_PIN_1
#define RMII_MDC_PORT GPIOC

#define RMII_RX_DATA_VALID_PIN GPIO_PIN_7
#define RMII_RX_DATA_VALID_PORT GPIOA

#define RMII_RXD0_PIN GPIO_PIN_4
#define RMII_RXD0_PORT GPIOC

#define RMII_RXD1_PIN GPIO_PIN_5
#define RMII_RXD1_PORT GPIOC

#define RMII_TX_ENABLE_PIN GPIO_PIN_11
#define RMII_TX_ENABLE_PORT GPIOG

#define RMII_TXD0_PIN GPIO_PIN_13
#define RMII_TXD0_PORT GPIOG

#define RMII_TXD1_PIN GPIO_PIN_13
#define RMII_TXD1_PORT GPIOB

typedef struct gpio_pin_s
{
    uint16_t pin;
    GPIO_TypeDef *port;
} gpio_pin_t;

const gpio_pin_t connector[] = {
    [CN8_2]                 {.pin = CN8_2_PIN, .port = CN8_2_PORT},
    [CN8_4]                 {.pin = CN8_4_PIN, .port = CN8_4_PORT},
    [CN8_6]                 {.pin = CN8_6_PIN, .port = CN8_6_PORT},
    [CN8_8]                 {.pin = CN8_8_PIN, .port = CN8_8_PORT},
    [CN8_10]                {.pin = CN8_10_PIN, .port = CN8_10_PORT},
    [CN8_12]                {.pin = CN8_12_PIN, .port = CN8_12_PORT},
    [CN8_14]                {.pin = CN8_14_PIN, .port = CN8_14_PORT},
    [CN8_16]                {.pin = CN8_16_PIN, .port = CN8_16_PORT},
    [CN9_1]                 {.pin = CN9_1_PIN, .port = CN9_1_PORT},
    [CN9_2]                 {.pin = CN9_2_PIN, .port = CN9_2_PORT},
    [CN9_3]                 {.pin = CN9_3_PIN, .port = CN9_3_PORT},
    [CN9_4]                 {.pin = CN9_4_PIN, .port = CN9_4_PORT},
    [CN9_5]                 {.pin = CN9_5_PIN, .port = CN9_5_PORT},
    [CN9_6]                 {.pin = CN9_6_PIN, .port = CN9_6_PORT},
    [CN9_7]                 {.pin = CN9_7_PIN, .port = CN9_7_PORT},
    [CN9_8]                 {.pin = CN9_8_PIN, .port = CN9_8_PORT},
    [CN9_9]                 {.pin = CN9_9_PIN, .port = CN9_9_PORT},
    [CN9_10]                {.pin = CN9_10_PIN, .port = CN9_10_PORT},
    [CN9_11]                {.pin = CN9_11_PIN, .port = CN9_11_PORT},
    [CN9_14]                {.pin = CN9_14_PIN, .port = CN9_14_PORT},
    [CN9_15]                {.pin = CN9_15_PIN, .port = CN9_15_PORT},
    [CN9_16]                {.pin = CN9_16_PIN, .port = CN9_16_PORT},
    [CN9_17]                {.pin = CN9_17_PIN, .port = CN9_17_PORT},
    [CN9_18]                {.pin = CN9_18_PIN, .port = CN9_18_PORT},
    [CN9_19]                {.pin = CN9_19_PIN, .port = CN9_19_PORT},
    [CN9_20]                {.pin = CN9_20_PIN, .port = CN9_20_PORT},
    [CN9_21]                {.pin = CN9_21_PIN, .port = CN9_21_PORT},
    [CN9_22]                {.pin = CN9_22_PIN, .port = CN9_22_PORT},
    [CN9_24]                {.pin = CN9_24_PIN, .port = CN9_24_PORT},
    [CN9_25]                {.pin = CN9_25_PIN, .port = CN9_25_PORT},
    [CN9_26]                {.pin = CN9_26_PIN, .port = CN9_26_PORT},
    [CN9_27]                {.pin = CN9_27_PIN, .port = CN9_27_PORT},
    [CN9_28]                {.pin = CN9_28_PIN, .port = CN9_28_PORT},
    [CN9_29]                {.pin = CN9_29_PIN, .port = CN9_29_PORT},
    [CN9_30]                {.pin = CN9_30_PIN, .port = CN9_30_PORT},
    [CN7_1]                 {.pin = CN7_1_PIN, .port = CN7_1_PORT},
    [CN7_2]                 {.pin = CN7_2_PIN, .port = CN7_2_PORT},
    [CN7_3]                 {.pin = CN7_3_PIN, .port = CN7_3_PORT},
    [CN7_4]                 {.pin = CN7_4_PIN, .port = CN7_4_PORT},
    [CN7_5]                 {.pin = CN7_5_PIN, .port = CN7_5_PORT},
    [CN7_7]                 {.pin = CN7_7_PIN, .port = CN7_7_PORT},
    [CN7_9]                 {.pin = CN7_9_PIN, .port = CN7_9_PORT},
    [CN7_10]                {.pin = CN7_10_PIN, .port = CN7_10_PORT},
    [CN7_11]                {.pin = CN7_11_PIN, .port = CN7_11_PORT},
    [CN7_12]                {.pin = CN7_12_PIN, .port = CN7_12_PORT},
    [CN7_13]                {.pin = CN7_13_PIN, .port = CN7_13_PORT},
    [CN7_14]                {.pin = CN7_14_PIN, .port = CN7_14_PORT},
    [CN7_15]                {.pin = CN7_15_PIN, .port = CN7_15_PORT},
    [CN7_16]                {.pin = CN7_16_PIN, .port = CN7_16_PORT},
    [CN7_17]                {.pin = CN7_17_PIN, .port = CN7_17_PORT},
    [CN7_18]                {.pin = CN7_18_PIN, .port = CN7_18_PORT},
    [CN7_19]                {.pin = CN7_19_PIN, .port = CN7_19_PORT},
    [CN7_20]                {.pin = CN7_20_PIN, .port = CN7_20_PORT},
    [CN10_2]                {.pin = CN10_2_PIN, .port = CN10_2_PORT},
    [CN10_4]                {.pin = CN10_4_PIN, .port = CN10_4_PORT},
    [CN10_6]                {.pin = CN10_6_PIN, .port = CN10_6_PORT},
    [CN10_7]                {.pin = CN10_7_PIN, .port = CN10_7_PORT},
    [CN10_8]                {.pin = CN10_8_PIN, .port = CN10_8_PORT},
    [CN10_9]                {.pin = CN10_9_PIN, .port = CN10_9_PORT},
    [CN10_10]               {.pin = CN10_10_PIN, .port = CN10_10_PORT},
    [CN10_11]               {.pin = CN10_11_PIN, .port = CN10_11_PORT},
    [CN10_12]               {.pin = CN10_12_PIN, .port = CN10_12_PORT},
    [CN10_13]               {.pin = CN10_13_PIN, .port = CN10_13_PORT},
    [CN10_14]               {.pin = CN10_14_PIN, .port = CN10_14_PORT},
    [CN10_15]               {.pin = CN10_15_PIN, .port = CN10_15_PORT},
    [CN10_16]               {.pin = CN10_16_PIN, .port = CN10_16_PORT},
    [CN10_18]               {.pin = CN10_18_PIN, .port = CN10_18_PORT},
    [CN10_19]               {.pin = CN10_19_PIN, .port = CN10_19_PORT},
    [CN10_20]               {.pin = CN10_20_PIN, .port = CN10_20_PORT},
    [CN10_21]               {.pin = CN10_21_PIN, .port = CN10_21_PORT},
    [CN10_23]               {.pin = CN10_23_PIN, .port = CN10_23_PORT},
    [CN10_24]               {.pin = CN10_24_PIN, .port = CN10_24_PORT},
    [CN10_25]               {.pin = CN10_25_PIN, .port = CN10_25_PORT},
    [CN10_26]               {.pin = CN10_26_PIN, .port = CN10_26_PORT},
    [CN10_28]               {.pin = CN10_28_PIN, .port = CN10_28_PORT},
    [CN10_29]               {.pin = CN10_29_PIN, .port = CN10_29_PORT},
    [CN10_30]               {.pin = CN10_30_PIN, .port = CN10_30_PORT},
    [CN10_31]               {.pin = CN10_31_PIN, .port = CN10_31_PORT},
    [CN10_32]               {.pin = CN10_32_PIN, .port = CN10_32_PORT},
    [CN10_33]               {.pin = CN10_33_PIN, .port = CN10_33_PORT},
    [CN10_34]               {.pin = CN10_34_PIN, .port = CN10_34_PORT},
    [LED_1]                 {.pin = LED_1_PIN, .port = LED_1_PORT},
    [LED_2]                 {.pin = LED_2_PIN, .port = LED_2_PORT},
    [LED_3]                 {.pin = LED_3_PIN, .port = LED_3_PORT},
    [USER_BUTTON]           {.pin = USER_BUTTON_PIN, .port = USER_BUTTON_PORT},
    [STLINK_UART_RX]        {.pin = STLINK_UART_RX_PIN, .port = STLINK_UART_RX_PORT},
    [STLINK_UART_TX]        {.pin = STLINK_UART_TX_PIN, .port = STLINK_UART_TX_PORT},
    [RMII_REF_CLK]          {.pin = RMII_REF_CLK_PIN, .port = RMII_REF_CLK_PORT},
    [RMII_MDIO]             {.pin = RMII_MDIO_PIN, .port = RMII_MDIO_PORT},
    [RMII_MDC]              {.pin = RMII_MDC_PIN, .port = RMII_MDC_PORT},
    [RMII_RX_DATA_VALID]    {.pin = RMII_RX_DATA_VALID_PIN, .port = RMII_RX_DATA_VALID_PORT},
    [RMII_RXD0]             {.pin = RMII_RXD0_PIN, .port = RMII_RXD0_PORT},
    [RMII_RXD1]             {.pin = RMII_RXD1_PIN, .port = RMII_RXD1_PORT},
    [RMII_TX_ENABLE]        {.pin = RMII_TX_ENABLE_PIN, .port = RMII_TX_ENABLE_PORT},
    [RMII_TXD0]             {.pin = RMII_TXD0_PIN, .port = RMII_TXD0_PORT},
    [RMII_TXD1]             {.pin = RMII_TXD1_PIN, .port = RMII_TXD1_PORT}
};

#endif // __GPIO_BSP_PIN_H__


