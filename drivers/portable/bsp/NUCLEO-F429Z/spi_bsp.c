/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "stm32f4xx_hal_conf.h"

#include "bsp_cfg.h"

#ifdef SPI1_CFG_1
	#define SPI1_PINS /*GPIO_PIN_4 | */GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7
	#define SPI1_PORT GPIOA
	#define SPI1_PULL GPIO_PULLUP
	#define SPI1_IO_CLK_EN __HAL_RCC_GPIOA_CLK_ENABLE
	#define SPI1_IRQ_PRIORITY 5
#endif

void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
	GPIO_InitTypeDef spi_gpio_cfg;

	#ifdef SPI1_CFG_1
	if( hspi->Instance == SPI1) {
		spi_gpio_cfg.Pin = SPI1_PINS;
		spi_gpio_cfg.Mode = GPIO_MODE_AF_PP;
		spi_gpio_cfg.Pull = SPI1_PULL;
		spi_gpio_cfg.Speed = GPIO_SPEED_FREQ_HIGH;
		spi_gpio_cfg.Alternate = GPIO_AF5_SPI1;

		SPI1_IO_CLK_EN();

		HAL_GPIO_Init( SPI1_PORT, &spi_gpio_cfg );

		__HAL_RCC_SPI1_CLK_ENABLE( );

		HAL_NVIC_SetPriority(SPI1_IRQn, SPI1_IRQ_PRIORITY, 0);
		HAL_NVIC_EnableIRQ(SPI1_IRQn);
    }
	#else
		/* TODO: add error logging */
		return;
	#endif
}
