/*
 * Copyright (c) 2019 Jakub Nowacki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __GPIO_CON_BSP_H__
#define __GPIO_CON_BSP_H__

#define CN8_2   			0
#define CN8_4 				(CN8_2 + 1)
#define CN8_6   			(CN8_4 + 1)
#define CN8_8   			(CN8_6 + 1)
#define CN8_10  			(CN8_8 + 1)
#define CN8_12  			(CN8_10 + 1)
#define CN8_14  			(CN8_12 + 1)
#define CN8_16  			(CN8_14 + 1)
#define CN9_1   			(CN8_16 + 1)
#define CN9_2   			(CN9_1 + 1)
#define CN9_3   			(CN9_2 + 1)
#define CN9_4   			(CN9_3 + 1)
#define CN9_5   			(CN9_4 + 1)
#define CN9_6   			(CN9_5 + 1)
#define CN9_7   			(CN9_6 + 1)
#define CN9_8   			(CN9_7 + 1)
#define CN9_9   			(CN9_8 + 1)
#define CN9_10  			(CN9_9 + 1)
#define CN9_11  			(CN9_10 + 1)
#define CN9_14  			(CN9_11 + 1)
#define CN9_15  			(CN9_14 + 1)
#define CN9_16  			(CN9_15 + 1)
#define CN9_17  			(CN9_16 + 1)
#define CN9_18  			(CN9_17 + 1)
#define CN9_19  			(CN9_18 + 1)
#define CN9_20  			(CN9_19 + 1)
#define CN9_21  			(CN9_20 + 1)
#define CN9_22  			(CN9_21 + 1)
#define CN9_24  			(CN9_22 + 1)
#define CN9_25  			(CN9_24 + 1)
#define CN9_26  			(CN9_25 + 1)
#define CN9_27  			(CN9_26 + 1)
#define CN9_28  			(CN9_27 + 1)
#define CN9_29  			(CN9_28 + 1)
#define CN9_30  			(CN9_29 + 1)
#define CN7_1   			(CN9_30 + 1)
#define CN7_2   			(CN7_1 + 1)
#define CN7_3   			(CN7_2 + 1)
#define CN7_4   			(CN7_3 + 1)
#define CN7_5   			(CN7_4 + 1)
#define CN7_7   			(CN7_5 + 1)
#define CN7_9   			(CN7_7 + 1)
#define CN7_10  			(CN7_9 + 1)
#define CN7_11  			(CN7_10 + 1)
#define CN7_12  			(CN7_11 + 1)
#define CN7_13  			(CN7_12 + 1)
#define CN7_14  			(CN7_13 + 1)
#define CN7_15  			(CN7_14 + 1)
#define CN7_16  			(CN7_15 + 1)
#define CN7_17  			(CN7_16 + 1)
#define CN7_18  			(CN7_17 + 1)
#define CN7_19  			(CN7_18 + 1)
#define CN7_20  			(CN7_19 + 1)
#define CN10_2  			(CN7_20 + 1)
#define CN10_4  			(CN10_2 + 1)
#define CN10_6  			(CN10_4 + 1)
#define CN10_7  			(CN10_6 + 1)
#define CN10_8  			(CN10_7 + 1)
#define CN10_9  			(CN10_8 + 1)
#define CN10_10 			(CN10_9 + 1)
#define CN10_11 			(CN10_10 + 1)
#define CN10_12 			(CN10_11 + 1)
#define CN10_13 			(CN10_12 + 1)
#define CN10_14 			(CN10_13 + 1)
#define CN10_15 			(CN10_14 + 1)
#define CN10_16 			(CN10_15 + 1)
#define CN10_18 			(CN10_16 + 1)
#define CN10_19 			(CN10_18 + 1)
#define CN10_20 			(CN10_19 + 1)
#define CN10_21 			(CN10_20 + 1)
#define CN10_23 			(CN10_21 + 1)
#define CN10_24 			(CN10_23 + 1)
#define CN10_25 			(CN10_24 + 1)
#define CN10_26 			(CN10_25 + 1)
#define CN10_28 			(CN10_26 + 1)
#define CN10_29 			(CN10_28 + 1)
#define CN10_30 			(CN10_29 + 1)
#define CN10_31 			(CN10_30 + 1)
#define CN10_32 			(CN10_31 + 1)
#define CN10_33 			(CN10_32 + 1)
#define CN10_34 			(CN10_33 + 1)
#define LED_1				(CN10_34 + 1)
#define LED_2 				(LED_1 + 1)
#define LED_3 				(LED_2 + 1)
#define USER_BUTTON			(LED_3 + 1)
#define STLINK_UART_RX		(USER_BUTTON + 1)
#define STLINK_UART_TX		(STLINK_UART_RX + 1)
#define RMII_REF_CLK		(STLINK_UART_TX + 1)
#define RMII_MDIO			(RMII_REF_CLK + 1)
#define RMII_MDC			(RMII_MDIO + 1)
#define RMII_RX_DATA_VALID	(RMII_MDC + 1)
#define RMII_RXD0			(RMII_RX_DATA_VALID + 1)
#define RMII_RXD1			(RMII_RXD0 + 1)
#define RMII_TX_ENABLE		(RMII_RXD1 + 1)
#define RMII_TXD0			(RMII_TX_ENABLE + 1)
#define RMII_TXD1			(RMII_TXD0 + 1)

#endif /* __GPIO_CON_BSP_H__ */
