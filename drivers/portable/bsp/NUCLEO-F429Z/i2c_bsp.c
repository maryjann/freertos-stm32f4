/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "stm32f4xx_hal_conf.h"
#include "bsp_cfg.h"

#ifdef I2C1_CFG_1
	#define I2C1_PINS GPIO_PIN_8 | GPIO_PIN_9
	#define I2C1_PORT GPIOB
	#define I2C1_PULL GPIO_NOPULL
	#define I2C1_IO_CLK_EN __HAL_RCC_GPIOB_CLK_ENABLE
	#define I2C1_IRQ_PRIORITY 5
#endif

#ifdef I2C2_CFG_1
	#define I2C2_PINS GPIO_PIN_10 | GPIO_PIN_11
	#define I2C2_PORT GPIOB
	#define I2C2_PULL GPIO_NOPULL
	#define I2C2_IO_CLK_EN __HAL_RCC_GPIOB_CLK_ENABLE
	#define I2C2_IRQ_PRIORITY 5
#endif

#ifdef I2C2_CFG_2
	#define I2C2_PINS GPIO_PIN_0 | GPIO_PIN_1
	#define I2C2_PORT GPIOF
	#define I2C2_PULL GPIO_NOPULL
	#define I2C2_IO_CLK_EN __HAL_RCC_GPIOF_CLK_ENABLE
	#define I2C2_IRQ_PRIORITY 5
#endif

void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c)
{
	GPIO_InitTypeDef i2c_gpio_cfg;

	if(hi2c->Instance == I2C1)
	{
	#if defined I2C1_PINS && defined I2C1_PORT && defined I2C1_PULL && defined I2C1_IO_CLK_EN && defined I2C1_IRQ_PRIORITY
		i2c_gpio_cfg.Pin = I2C1_PINS;
		i2c_gpio_cfg.Mode = GPIO_MODE_AF_OD;
        i2c_gpio_cfg.Pull = I2C1_PULL;
        i2c_gpio_cfg.Speed = GPIO_SPEED_FREQ_LOW;
        i2c_gpio_cfg.Alternate = GPIO_AF4_I2C1;

        I2C1_IO_CLK_EN();

        HAL_GPIO_Init( I2C1_PORT, &i2c_gpio_cfg );

        __HAL_RCC_I2C1_CLK_ENABLE( );

        HAL_NVIC_SetPriority(I2C1_ER_IRQn, I2C1_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C1_ER_IRQn);
        HAL_NVIC_SetPriority(I2C1_EV_IRQn, I2C1_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);
	#else
        /* TODO: add error logging */
		return;
	#endif
	}
	else if(hi2c->Instance == I2C2)
	{
	#if defined I2C2_PINS && defined I2C2_PORT && defined I2C2_PULL && defined I2C2_IO_CLK_EN && defined I2C2_IRQ_PRIORITY
		i2c_gpio_cfg.Pin = I2C2_PINS;
		i2c_gpio_cfg.Mode = GPIO_MODE_AF_OD;
        i2c_gpio_cfg.Pull = I2C2_PULL;
        i2c_gpio_cfg.Speed = GPIO_SPEED_FREQ_LOW;
        i2c_gpio_cfg.Alternate = GPIO_AF4_I2C2;

        I2C2_IO_CLK_EN();

        HAL_GPIO_Init( I2C2_PORT, &i2c_gpio_cfg );

        __HAL_RCC_I2C2_CLK_ENABLE( );

        HAL_NVIC_SetPriority(I2C2_ER_IRQn, I2C2_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C2_ER_IRQn);
        HAL_NVIC_SetPriority(I2C2_EV_IRQn, I2C2_IRQ_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(I2C2_EV_IRQn);
	#else
        (void) i2c_gpio_cfg;
		/* TODO: add error logging */
		return;
	#endif
	}
	else
	{
		(void) i2c_gpio_cfg;
		/* TODO: add error logging */
		return;
	}
}
