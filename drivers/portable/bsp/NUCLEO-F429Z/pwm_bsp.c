/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "stm32f4xx_hal_conf.h"

#include "pwm_soc.h"
#include "gpio_soc.h"
#include "bsp_cfg.h"

#ifdef PWM_TIMER_8_CH3_CN8_2
    #define TIMER_8_CH3_PIN CN8_2
#endif

#ifdef PWM_TIMER_10_CH1_CN7_2
	#define TIMER_10_CH1_PIN CN7_2
#endif

#ifdef PWM_TIMER_11_CH1_CN7_4
	#define TIMER_11_CH1_PIN CN7_4
#endif

#ifdef PWM_TIMER_11_CH1_CN9_26
	#define TIMER_11_CH1_PIN CN9_26
#endif

#ifdef PWM_TIMER_13_CH1_CN9_24
	#define TIMER_13_CH1_PIN CN9_24
#endif

#ifdef PWM_TIMER_13_CH1_CN7_12
	#define TIMER_13_CH1_PIN CN7_12
#endif

#ifdef PWM_TIMER_14_CH1_CN9_28
	#define TIMER_14_CH1_PIN CN9_28
#endif

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim) {
	if (htim == NULL) {
		return;
	}
	
	pwm_handle_t *pwm_handle = (pwm_handle_t *)htim;
	
	if (htim->Instance == TIM10) {
		__HAL_RCC_TIM10_CLK_ENABLE();

		#if defined TIMER_10_CH1_PIN
			gpio_soc_init_alternate(TIMER_10_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM10);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
	}
	else if (htim->Instance == TIM11) {
		__HAL_RCC_TIM11_CLK_ENABLE();

		#if defined TIMER_11_CH1_PIN
			gpio_soc_init_alternate(TIMER_11_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM11);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
	}
	else if (htim->Instance == TIM13) {
		__HAL_RCC_TIM13_CLK_ENABLE();

		#if defined TIMER_13_CH1_PIN
			gpio_soc_init_alternate(TIMER_13_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF9_TIM13);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
	}
	else if (htim->Instance == TIM14) {
		__HAL_RCC_TIM14_CLK_ENABLE();

		#if defined TIMER_14_CH1_PIN
			gpio_soc_init_alternate(TIMER_14_CH1_PIN, G_MODE_ALTERNATE_PP, G_PULL_PULL_UP, G_IRQ_NONE, NULL, NULL, GPIO_AF9_TIM14);
			pwm_handle->channel = TIM_CHANNEL_1;
		#endif
	}
    else if (htim->Instance == TIM8) {
        __HAL_RCC_TIM8_CLK_ENABLE();

        #if defined TIMER_8_CH3_PIN
            gpio_soc_init_alternate(TIMER_8_CH3_PIN, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF3_TIM8);
            pwm_handle->channel = TIM_CHANNEL_3;
            pwm_handle->irq = TIM8_CC_IRQn;
        #endif
    }

	return;
}
