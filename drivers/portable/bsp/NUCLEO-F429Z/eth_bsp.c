/*
 * Copyright (c) 2020 Jakub Nowacki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "stm32f4xx_hal_conf.h"
#include "gpio.h"

void HAL_ETH_MspInit(ETH_HandleTypeDef *heth)
{
	gpio_soc_init_alternate(RMII_REF_CLK, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF11_ETH);
	gpio_soc_init_alternate(RMII_MDIO, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF11_ETH);
	gpio_soc_init_alternate(RMII_MDC, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF11_ETH);
	gpio_soc_init_alternate(RMII_RX_DATA_VALID, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF11_ETH);
	gpio_soc_init_alternate(RMII_RXD0, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF11_ETH);
	gpio_soc_init_alternate(RMII_RXD1, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF11_ETH);
	gpio_soc_init_alternate(RMII_TX_ENABLE, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF11_ETH);
	gpio_soc_init_alternate(RMII_TXD0, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF11_ETH);
	gpio_soc_init_alternate(RMII_TXD1, G_MODE_ALTERNATE_PP, G_PULL_NONE, G_IRQ_NONE, NULL, NULL, GPIO_AF11_ETH);

	HAL_NVIC_SetPriority(ETH_IRQn, 0x7, 0);
	HAL_NVIC_EnableIRQ(ETH_IRQn);
  
	__HAL_RCC_ETH_CLK_ENABLE();
	__HAL_RCC_ETHMACTX_CLK_ENABLE();
	__HAL_RCC_ETHMACRX_CLK_ENABLE();
}
