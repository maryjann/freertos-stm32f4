/*
 * Copyright (c) 2019 Damian Eppel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>
#include "ws2812b_soc.h"
#include "ws2812b_bsp.h"

static pwm_timer_t timer;
static dma_handle_t dma;

int ws2812b_soc_dma2timer_init(dma_isr_cb_t cb, uint32_t *t0h, uint32_t *t1h) {

    struct dma2periph_config dma_cfg;
    struct ws2812b_bsp_config bsp_cfg;

    ws2812b_get_bsp_config(&bsp_cfg);

    timer = bsp_cfg.timer;
    dma = bsp_cfg.dma;

    dma_cfg.channel = bsp_cfg.dma_channel;
    dma_cfg.dma_cb = cb;
    dma_cfg.cb_priv = NULL;
    dma_cfg.dma_irq_prio = 4;
    dma_cfg.memory_align = DMA_DATA_ALIGN_WORD;
    dma_cfg.periph_align = DMA_DATA_ALIGN_WORD;
    dma_cfg.periph_inc = DMA_ADDR_NOINCREMENT;
    dma_cfg.memory_inc = DMA_ADDR_INCREMENT;
    dma_cfg.periph_addr = bsp_cfg.periph_addr;
    dma2periph_setup(dma, &dma_cfg);

    uint32_t timer_res = (pwm_soc_get_timer_clock(timer) / WS2812B_PWM_FREQ);
    pwm_soc_init(timer, WS2812B_PWM_FREQ, timer_res - 1);
    pwm_soc_set_timer_for_dma(timer);

    *t0h = 0.28 * timer_res;
    *t1h = timer_res - *t0h;

    return 0;
}

void ws2812b_soc_dma2timer_start(uint32_t mem_addr,
                                 size_t size,
                                 int ht_tc_flags,
                                 enum dma_run_mode mode) {

    dma2periph_start(dma, mem_addr, size, ht_tc_flags, mode);
    pwm_soc_timer_start(timer);
}

void ws2812b_soc_dma2timer_stop() {

    pwm_soc_timer_stop(timer);
    dma2periph_stop(dma);
}
