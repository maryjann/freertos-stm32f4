/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __USART_SOC_H__
#define __USART_SOC_H__

#include "stm32f4xx_hal_conf.h"

#include "usart_bsp.h"

typedef void (*usart_send_done_clbk)(void *data, int result);
typedef void (*usart_recv_done_clbk)(void *data, int result);

typedef enum usart_world_length_e {
	USART_WORLD_LENGTH_8 = UART_WORDLENGTH_8B,
	USART_WORLD_LENGTH_9 = UART_WORDLENGTH_9B
} usart_world_length_t;

typedef enum usart_stop_bits_e {
	USART_STOPBITS_1 = UART_STOPBITS_1,
	USART_STOPBITS_2 = UART_STOPBITS_2
} usart_stop_bits_t;

typedef enum  usart_parity_e {
	USART_PARITY_NONE = UART_PARITY_NONE,
	USART_PARITY_EVEN = UART_PARITY_EVEN,
	USART_PARITY_ODD = UART_PARITY_ODD
} usart_parity_t;

typedef enum usart_hw_control_e {
	USART_HWCONTROL_NONE = UART_HWCONTROL_NONE,
	USART_HWCONTROL_RTS = UART_HWCONTROL_RTS,
	USART_HWCONTROL_CTS = UART_HWCONTROL_CTS,
	USART_HWCONTROL_RTS_CTS = UART_HWCONTROL_RTS_CTS
} usart_hw_control_t;

int usart_soc_init(usart_t port, uint32_t baud_rate, usart_world_length_t world_length, usart_stop_bits_t stop_bits, usart_parity_t parity, usart_hw_control_t hw_control);
void usart_soc_deinit(usart_t port);

int usart_soc_send(usart_t port, uint8_t *data, size_t length, usart_send_done_clbk clbk, void *clbk_data);
int usart_soc_abort_send(usart_t port);

int usart_soc_recv(usart_t port, uint8_t *data, size_t length, usart_recv_done_clbk clbk, void *clbk_data);
int usart_soc_abort_recv(usart_t port);

#endif //__USART_SOC_H__
