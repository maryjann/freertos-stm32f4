/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>

#include "i2c_common_soc.h"

static i2c_handle_t i2c_handles[I2C_BUS_END];

int i2c_common_periph_init(i2c_bus_t bus, uint32_t addr_mode, uint32_t clock_speed, uint32_t own_address)
{
	i2c_handle_t *i2c_handle = &i2c_handles[bus];
	memset( i2c_handle, 0, sizeof(i2c_handle_t) );

	if( bus == I2C_BUS_1 )
    {
        i2c_handle->i2c_hal_handle.Instance = I2C1;
    }
    else if( bus == I2C_BUS_2 )
    {
        i2c_handle->i2c_hal_handle.Instance = I2C2;
    }

	i2c_handle->i2c_hal_handle.Init.AddressingMode = addr_mode;
    i2c_handle->i2c_hal_handle.Init.ClockSpeed = clock_speed;
    i2c_handle->i2c_hal_handle.Init.DutyCycle = I2C_DUTYCYCLE_2;
    i2c_handle->i2c_hal_handle.Init.OwnAddress1 = own_address;
    /* **************************** */
    /* Unsupported options:			*/
    /*		DualAddressMode			*/
    /*		OwnAddress2				*/
    /*		GeneralCallMode			*/
    /*		NoStretchMode           */
    /* **************************** */

    HAL_I2C_Init( &i2c_handle->i2c_hal_handle );

	return 0;
}

void i2c_common_periph_deinit(i2c_bus_t bus)
{
	return;
}

i2c_handle_t * i2c_common_get_handle(i2c_bus_t bus)
{
    return &i2c_handles[bus];
}

void HAL_I2C_AbortCpltCallback(I2C_HandleTypeDef *hi2c)
{
	assert_param(1);
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
	assert_param(1);
}

/* I2C interrupt handlers */
void I2C1_EV_IRQHandler(void)
{
    HAL_I2C_EV_IRQHandler((I2C_HandleTypeDef *)&i2c_handles[I2C_BUS_1]);
}

void I2C1_ER_IRQHandler(void)
{
    HAL_I2C_ER_IRQHandler((I2C_HandleTypeDef *)&i2c_handles[I2C_BUS_1]);
}

void I2C2_EV_IRQHandler(void)
{
    HAL_I2C_EV_IRQHandler((I2C_HandleTypeDef *)&i2c_handles[I2C_BUS_2]);
}

void I2C2_ER_IRQHandler(void)
{
    HAL_I2C_ER_IRQHandler((I2C_HandleTypeDef *)&i2c_handles[I2C_BUS_2]);
}



