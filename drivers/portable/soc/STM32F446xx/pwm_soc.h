/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __PWM_SOC_H__
#define __PWM_SOC_H__

#include "stm32f4xx_hal_conf.h"

#include "pwm_bsp.h"


#define TIMER_IRQ_PRORITY 7

typedef struct pwm_handle_s {
	TIM_HandleTypeDef pwm_hal_handle;
	TIM_OC_InitTypeDef pwm_hal_config;

	uint32_t channel;
    IRQn_Type irq;
} pwm_handle_t;

int pwm_soc_init(pwm_timer_t timer, uint32_t p_freq, uint32_t res);
void pwm_soc_deinit(pwm_timer_t timer);

uint32_t pwm_soc_get_timer_clock(pwm_timer_t timer);

int pwm_soc_set_duty_cycle(pwm_timer_t timer, uint16_t dc);
int pwm_soc_get_duty_cycle(pwm_timer_t timer, uint16_t *dc);
void pwm_soc_set_timer_for_dma(pwm_timer_t timer);
void pwm_soc_timer_start(pwm_timer_t timer);
void pwm_soc_timer_stop(pwm_timer_t timer);

#endif
