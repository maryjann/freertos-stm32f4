/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>

#include "stm32f4xx_hal_conf.h"
#include "encoder_soc.h"

#include <tools/container_of.h>

static encoder_handle_t encoder_handles[ENCODER_TIMER_END];

int encoder_soc_init_single(encoder_timer_t timer) {
	TIM_SlaveConfigTypeDef sSlaveConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	if(timer >= ENCODER_TIMER_END)
		return -1;

	encoder_handle_t *encoder_handle = &encoder_handles[timer];
	memset(encoder_handle, 0, sizeof(encoder_handle_t));

	switch(timer) {
		case ENCODER_TIMER_1:
			encoder_handle->encoder_hal_handle.Instance = TIM1;
			break;
		case ENCODER_TIMER_2:
			encoder_handle->encoder_hal_handle.Instance = TIM2;
			break;
		case ENCODER_TIMER_3:
			encoder_handle->encoder_hal_handle.Instance = TIM3;
			break;
		case ENCODER_TIMER_4:
			encoder_handle->encoder_hal_handle.Instance = TIM4;
			break;
		case ENCODER_TIMER_5:
			encoder_handle->encoder_hal_handle.Instance = TIM5;
			break;
		case ENCODER_TIMER_8:
			encoder_handle->encoder_hal_handle.Instance = TIM8;
			break;
		default:
			return -2;
	}

	encoder_handle->encoder_type = ENCODER_TYPE_SINGLE;

	encoder_handle->encoder_hal_handle.Init.Period             = 65535;
	encoder_handle->encoder_hal_handle.Init.Prescaler          = 0;
	encoder_handle->encoder_hal_handle.Init.ClockDivision      = TIM_CLOCKDIVISION_DIV1;
	encoder_handle->encoder_hal_handle.Init.CounterMode        = TIM_COUNTERMODE_UP;
	encoder_handle->encoder_hal_handle.Init.RepetitionCounter  = 0;

	if (HAL_TIM_Base_Init(&encoder_handle->encoder_hal_handle) != HAL_OK)
	{
		return -3;
	}

	sSlaveConfig.SlaveMode = TIM_SLAVEMODE_EXTERNAL1;
	sSlaveConfig.InputTrigger = TIM_TS_TI1FP1;
	sSlaveConfig.TriggerPolarity = TIM_TRIGGERPOLARITY_BOTHEDGE;
	sSlaveConfig.TriggerFilter = 15;

	if (HAL_TIM_SlaveConfigSynchronization(&encoder_handle->encoder_hal_handle,
		&sSlaveConfig) != HAL_OK)
	{
		return -4;
	}

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;

	if (HAL_TIMEx_MasterConfigSynchronization(&encoder_handle->encoder_hal_handle,
		&sMasterConfig) != HAL_OK)
	{
		return -5;
	}

	HAL_TIM_Base_Start(&encoder_handle->encoder_hal_handle);
	return 0;
}

int encoder_soc_init_dual(encoder_timer_t timer) {
	if(timer >= ENCODER_TIMER_END)
		return -1;

	uint32_t channels = 0;
	encoder_handle_t *encoder_handle = &encoder_handles[timer];
	memset(encoder_handle, 0, sizeof(encoder_handle_t));
	//ToDo - timer enters overflow interrupt after reset
	encoder_handle->overflow_cnt = -1;

	switch(timer) {
		case ENCODER_TIMER_1:
			encoder_handle->encoder_hal_handle.Instance = TIM1;
			HAL_NVIC_SetPriority(TIM1_UP_TIM10_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);
			break;
		case ENCODER_TIMER_2:
			encoder_handle->encoder_hal_handle.Instance = TIM2;
			HAL_NVIC_SetPriority(TIM2_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(TIM2_IRQn);
			break;
		case ENCODER_TIMER_3:
			encoder_handle->encoder_hal_handle.Instance = TIM3;
			HAL_NVIC_SetPriority(TIM3_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(TIM3_IRQn);
			break;
		case ENCODER_TIMER_4:
			encoder_handle->encoder_hal_handle.Instance = TIM4;
			HAL_NVIC_SetPriority(TIM4_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(TIM4_IRQn);
			break;
		case ENCODER_TIMER_5:
			encoder_handle->encoder_hal_handle.Instance = TIM5;
			HAL_NVIC_SetPriority(TIM5_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(TIM5_IRQn);
			break;
		case ENCODER_TIMER_8:
			encoder_handle->encoder_hal_handle.Instance = TIM8;
			HAL_NVIC_SetPriority(TIM8_UP_TIM13_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);
			break;
		default:
			return -2;
	}

	encoder_handle->encoder_type = ENCODER_TYPE_DUAL;

	encoder_handle->encoder_hal_handle.Init.Period             = 0x7FFF;
	encoder_handle->encoder_hal_handle.Init.Prescaler          = 0;
	encoder_handle->encoder_hal_handle.Init.ClockDivision      = TIM_CLOCKDIVISION_DIV1;
	encoder_handle->encoder_hal_handle.Init.CounterMode        = TIM_COUNTERMODE_UP;
	encoder_handle->encoder_hal_handle.Init.RepetitionCounter  = 0;

	switch(encoder_bsp_get_mode(timer)) {
		case TIM_ENCODERMODE_TI1:
			channels |= TIM_CHANNEL_1;
			break;
		case TIM_ENCODERMODE_TI2:
			channels |= TIM_CHANNEL_2;
			break;
		case TIM_ENCODERMODE_TI12:
			channels |= TIM_CHANNEL_1 | TIM_CHANNEL_2;
			break;
		default:
			return -3;
	}

	encoder_handle->encoder_hal_config.IC1Polarity        = TIM_ICPOLARITY_FALLING;
	encoder_handle->encoder_hal_config.IC1Selection       = TIM_ICSELECTION_DIRECTTI;
	encoder_handle->encoder_hal_config.IC1Prescaler       = TIM_ICPSC_DIV8;
	encoder_handle->encoder_hal_config.IC1Filter          = 0x0F;

	encoder_handle->encoder_hal_config.IC2Polarity        = TIM_ICPOLARITY_FALLING;
	encoder_handle->encoder_hal_config.IC2Selection       = TIM_ICSELECTION_DIRECTTI;
	encoder_handle->encoder_hal_config.IC2Prescaler       = TIM_ICPSC_DIV8;
	encoder_handle->encoder_hal_config.IC2Filter          = 0x0F;
	encoder_handle->encoder_hal_config.EncoderMode        = encoder_bsp_get_mode(timer);

	HAL_StatusTypeDef hal_ret = HAL_TIM_Encoder_Init((TIM_HandleTypeDef *)encoder_handle, &encoder_handle->encoder_hal_config);
	if(hal_ret != HAL_OK) {
		return -4;
	}

	__HAL_TIM_SET_COUNTER(&encoder_handle->encoder_hal_handle, 0x7FFF);
	__HAL_TIM_URS_ENABLE(&encoder_handle->encoder_hal_handle);
	__HAL_TIM_ENABLE_IT(&encoder_handle->encoder_hal_handle, TIM_IT_UPDATE);
	HAL_TIM_Encoder_Start(&encoder_handle->encoder_hal_handle, channels);

	return 0;
}

int encoder_soc_init(encoder_timer_t timer, encoder_type_t type) {
	if (type == ENCODER_TYPE_SINGLE)
		return encoder_soc_init_single(timer);

	if (type == ENCODER_TYPE_DUAL)
		return encoder_soc_init_dual(timer);

	return -1;
}

void encoder_soc_deinit(encoder_timer_t timer) {
	if(timer >= ENCODER_TIMER_END)
		return;

	encoder_handle_t *encoder_handle = &encoder_handles[timer];

	if (encoder_handle->encoder_type == ENCODER_TYPE_SINGLE)
		HAL_TIM_Base_Stop(&encoder_handle->encoder_hal_handle);

	if (encoder_handle->encoder_type == ENCODER_TYPE_DUAL)
		HAL_TIM_Encoder_Stop(&encoder_handle->encoder_hal_handle, TIM_CHANNEL_ALL);
}

encoder_dir_t encoder_soc_get_direction(encoder_timer_t timer) {
	uint32_t dir = __HAL_TIM_IS_TIM_COUNTING_DOWN(&encoder_handles[timer].encoder_hal_handle);
	if(dir)
		return ENCODER_DIR_DOWN;
	else
		return ENCODER_DIR_UP;
}

int32_t encoder_soc_get_value(encoder_timer_t timer) {
	uint16_t tmp = __HAL_TIM_GET_COUNTER(&encoder_handles[timer].encoder_hal_handle);
	return (encoder_handles[timer].overflow_cnt * 0x8000 + tmp - 0x7FFF);
}

void encoder_soc_handle_irq(encoder_timer_t timer) {
	HAL_TIM_IRQHandler(&encoder_handles[timer].encoder_hal_handle);	
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	encoder_handle_t *encoder_handle = container_of(htim, encoder_handle_t, encoder_hal_handle);

	if(__HAL_TIM_IS_TIM_COUNTING_DOWN(htim)) {
		encoder_handle->overflow_cnt--;
	} else {
		encoder_handle->overflow_cnt++;
	}

	return;
}
