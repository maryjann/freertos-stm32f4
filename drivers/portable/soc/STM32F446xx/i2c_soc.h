/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __I2C_SOC_H__
#define __I2C_SOC_H__

#include "stm32f4xx_hal_conf.h"

#include "i2c_bsp.h"

#define I2C_ERROR_NONE 				HAL_I2C_ERROR_NONE
#define I2C_ERROR_BERR				HAL_I2C_ERROR_BERR
#define I2C_ERROR_ARLO				HAL_I2C_ERROR_ARLO
#define I2C_ERROR_AF				HAL_I2C_ERROR_AF
#define I2C_ERROR_OVR				HAL_I2C_ERROR_OVR
#define I2C_ERROR_DMA				HAL_I2C_ERROR_DMA
#define I2C_ERROR_TIMEOUT			HAL_I2C_ERROR_TIMEOUT

#define I2C_ADDRESSING_MODE_7BIT	I2C_ADDRESSINGMODE_7BIT
#define I2C_ADDRESSING_MODE_10BIT	I2C_ADDRESSINGMODE_10BIT

typedef void (*i2c_op_clbk_t)(int result, void *data);

/* I2C master interface */
int i2c_master_soc_init(i2c_bus_t i2c_bus, uint32_t addr_mode, uint32_t clock_speed);
void i2c_master_soc_deinit(i2c_bus_t i2c_bus);
int i2c_master_soc_send(i2c_bus_t bus, uint8_t address, uint8_t *data, size_t length, i2c_op_clbk_t clbk, void *user);
int i2c_master_soc_recv(i2c_bus_t bus, uint8_t address, uint8_t *data, size_t length, i2c_op_clbk_t clbk, void *user);

/* I2C slave interface */
/* TODO */

#endif /* __I2C_SOC_H__ */
