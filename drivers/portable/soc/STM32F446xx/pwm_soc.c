/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>

#include "pwm_soc.h"

static pwm_handle_t pwm_handles[PWM_TIMER_END];

static inline void enable_dma_request(TIM_HandleTypeDef *instance, uint32_t channel)
{
    switch (channel) {

    case TIM_CHANNEL_1:
        __HAL_TIM_ENABLE_DMA(instance, TIM_DMA_CC1);
        break;

    case TIM_CHANNEL_2:
        __HAL_TIM_ENABLE_DMA(instance, TIM_DMA_CC2);
        break;

    case TIM_CHANNEL_3:
        __HAL_TIM_ENABLE_DMA(instance, TIM_DMA_CC3);
        break;

    case TIM_CHANNEL_4:
        __HAL_TIM_ENABLE_DMA(instance, TIM_DMA_CC4);
        break;

    default:
        break;
    }
}

int pwm_soc_init(pwm_timer_t timer, uint32_t p_freq, uint32_t res) {
	if (timer > PWM_TIMER_END) {
		return -1;
	}

	uint32_t timer_freq = pwm_soc_get_timer_clock(timer);

	if (timer_freq == 0 || p_freq * res > timer_freq) {
		return -2;
	}

	pwm_handle_t *pwm_handle = &pwm_handles[timer];
	memset(pwm_handle, 0, sizeof(pwm_handle_t));

	switch(timer) {
		case PWM_TIMER_2:
			pwm_handle->pwm_hal_handle.Instance = TIM2;
			break;
		case PWM_TIMER_3:
			pwm_handle->pwm_hal_handle.Instance = TIM3;
			break;
		case PWM_TIMER_4:
			pwm_handle->pwm_hal_handle.Instance = TIM4;
			break;
		case PWM_TIMER_5:
			pwm_handle->pwm_hal_handle.Instance = TIM5;
			break;
		case PWM_TIMER_8:
			pwm_handle->pwm_hal_handle.Instance = TIM8;
			break;
		case PWM_TIMER_9:
			pwm_handle->pwm_hal_handle.Instance = TIM9;
			break;
		case PWM_TIMER_10:
			pwm_handle->pwm_hal_handle.Instance = TIM11;
			break;
		case PWM_TIMER_11:
			pwm_handle->pwm_hal_handle.Instance = TIM11;
			break;
		case PWM_TIMER_12:
			pwm_handle->pwm_hal_handle.Instance = TIM12;
			break;
		case PWM_TIMER_13:
			pwm_handle->pwm_hal_handle.Instance = TIM13;
			break;
		case PWM_TIMER_14:
			pwm_handle->pwm_hal_handle.Instance = TIM14;
			break;
		default:
			return -3;
	}

	pwm_handle->pwm_hal_handle.Init.Prescaler         = (timer_freq / (p_freq * res)) - 1;
	pwm_handle->pwm_hal_handle.Init.Period            = res;
	pwm_handle->pwm_hal_handle.Init.ClockDivision     = 0;
	pwm_handle->pwm_hal_handle.Init.CounterMode       = TIM_COUNTERMODE_UP;
	pwm_handle->pwm_hal_handle.Init.RepetitionCounter = 0;

	pwm_handle->pwm_hal_config.OCMode       = TIM_OCMODE_PWM1;
	pwm_handle->pwm_hal_config.OCPolarity   = TIM_OCPOLARITY_HIGH;
	pwm_handle->pwm_hal_config.OCFastMode   = TIM_OCFAST_DISABLE;
	pwm_handle->pwm_hal_config.OCNPolarity  = TIM_OCNPOLARITY_HIGH;
	pwm_handle->pwm_hal_config.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	pwm_handle->pwm_hal_config.OCIdleState  = TIM_OCIDLESTATE_RESET;
	pwm_handle->pwm_hal_config.Pulse 		= 0;

	HAL_TIM_PWM_Init((TIM_HandleTypeDef *)pwm_handle);
	HAL_TIM_PWM_ConfigChannel(&pwm_handle->pwm_hal_handle, &pwm_handle->pwm_hal_config, pwm_handle->channel);
    //HAL_TIM_PWM_Start(&pwm_handle->pwm_hal_handle, pwm_handle->channel);
	
	return 0;
}

uint32_t pwm_soc_get_timer_clock(pwm_timer_t timer)
{
	if (timer > PWM_TIMER_END) {
		return 0;
	}

	RCC_ClkInitTypeDef clkConf;
	RCC_PeriphCLKInitTypeDef pclkConf;
	uint32_t pFLatency;
	uint32_t timer_freq = 0;

	HAL_RCCEx_GetPeriphCLKConfig(&pclkConf);
	HAL_RCC_GetClockConfig(&clkConf, &pFLatency);
	
	switch(timer) {
		//APB2
		case PWM_TIMER_1:
		case PWM_TIMER_8:
		case PWM_TIMER_9:
		case PWM_TIMER_10:
		case PWM_TIMER_11:
			if (pclkConf.TIMPresSelection == RCC_TIMPRES_DESACTIVATED) 
			{
				if (clkConf.APB2CLKDivider == RCC_HCLK_DIV1)
					timer_freq = HAL_RCC_GetPCLK2Freq();
				else
					timer_freq = 2 * HAL_RCC_GetPCLK2Freq();
			}
			else 
			{
				if (clkConf.APB2CLKDivider == RCC_HCLK_DIV1 || 
					clkConf.APB2CLKDivider == RCC_HCLK_DIV2 ||
					clkConf.APB2CLKDivider == RCC_HCLK_DIV4)
					timer_freq = HAL_RCC_GetHCLKFreq();
				else
					timer_freq = 4 * HAL_RCC_GetPCLK2Freq();
			}
			break;
		//APB1
		case PWM_TIMER_2:
		case PWM_TIMER_3:
		case PWM_TIMER_4:
		case PWM_TIMER_5:
		case PWM_TIMER_12:
		case PWM_TIMER_13:
		case PWM_TIMER_14:
			if (pclkConf.TIMPresSelection == RCC_TIMPRES_DESACTIVATED) 
			{
				if (clkConf.APB1CLKDivider == 1)
					timer_freq = HAL_RCC_GetPCLK1Freq();
				else
					timer_freq = 2 * HAL_RCC_GetPCLK1Freq();
			}
			else 
			{
				if (clkConf.APB1CLKDivider >= 1 && clkConf.APB1CLKDivider <= 4)
					timer_freq = HAL_RCC_GetHCLKFreq();
				else
					timer_freq = 4 * HAL_RCC_GetPCLK1Freq();
			}
			break;
		default:
			return -1;
	}

	return timer_freq;
}

void pwm_soc_set_timer_for_dma(pwm_timer_t timer)
{
    pwm_handle_t *pwm_handle = &pwm_handles[timer];

    enable_dma_request(&pwm_handle->pwm_hal_handle, pwm_handle->channel);

    switch(timer) {
        case PWM_TIMER_1:
            pwm_handle->irq = TIM1_CC_IRQn;
            break;
        case PWM_TIMER_8:
            pwm_handle->irq = TIM8_CC_IRQn;
            break;
        default:
            return;
    }

    HAL_NVIC_SetPriority(pwm_handle->irq, TIMER_IRQ_PRORITY, 0);
    HAL_NVIC_EnableIRQ(pwm_handle->irq);
}

void pwm_soc_timer_start(pwm_timer_t timer)
{
    pwm_handle_t *pwm_handle = &pwm_handles[timer];
    HAL_TIM_PWM_Start(&pwm_handle->pwm_hal_handle, pwm_handle->channel);
}

void pwm_soc_timer_stop(pwm_timer_t timer)
{
    pwm_handle_t *pwm_handle = &pwm_handles[timer];
    HAL_TIM_PWM_Stop(&pwm_handle->pwm_hal_handle, pwm_handle->channel);
}

void pwm_soc_deinit(pwm_timer_t timer) {
	return;
}

int pwm_soc_set_duty_cycle(pwm_timer_t timer, uint16_t dc) {
	if (timer > PWM_TIMER_END) {
		return -1;
	}

	//TODO: add mutex protection
	pwm_handles[timer].pwm_hal_config.Pulse = dc;
	HAL_TIM_PWM_ConfigChannel(&pwm_handles[timer].pwm_hal_handle, &pwm_handles[timer].pwm_hal_config, pwm_handles[timer].channel);
	HAL_TIM_PWM_Start(&pwm_handles[timer].pwm_hal_handle, pwm_handles[timer].channel);

	return 0;
}

int pwm_soc_get_duty_cycle(pwm_timer_t timer, uint16_t *dc) {
	if (timer > PWM_TIMER_END) {
		return -1;
	}
	if (dc == NULL) {
		return -2;
	}
	
	*dc = pwm_handles[timer].pwm_hal_config.Pulse;

	return 0;
}
