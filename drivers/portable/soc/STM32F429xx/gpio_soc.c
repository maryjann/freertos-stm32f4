/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "gpio_soc.h"
#include "gpio_bsp_pin.h"

#define EXTI_LINES_CNT	16

static uint8_t gpio_a_cnt = 0;
static uint8_t gpio_b_cnt = 0;
static uint8_t gpio_c_cnt = 0;
static uint8_t gpio_d_cnt = 0;
static uint8_t gpio_e_cnt = 0;
static uint8_t gpio_f_cnt = 0;
static uint8_t gpio_g_cnt = 0;

static void gpio_clk_enable(size_t pin);
static void gpio_clk_disable(size_t pin);
static void enable_irq(size_t pin);
static void disable_irq(size_t pin);

typedef struct gpio_irq_callback_s
{
	gpio_irq_callback_f callback;
	void *data;
} gpio_irq_callback_t;

static gpio_irq_callback_t gpio_irq_callbacks[EXTI_LINES_CNT];

int gpio_soc_init(size_t pin, gpio_mode_t mode, gpio_pull_t pull, gpio_irq_t irq, gpio_irq_callback_f irq_callback, void *data)
{
	return gpio_soc_init_alternate(pin, mode, pull, irq, irq_callback, data, 0);
}

int gpio_soc_init_alternate(size_t pin, gpio_mode_t mode, gpio_pull_t pull, gpio_irq_t irq, gpio_irq_callback_f irq_callback, void *data, uint32_t alternate)
{
	if(pin >= sizeof(connector) / sizeof(connector[0]))
	{
		assert_param(1);
	}

	GPIO_InitTypeDef gpio_cfg;
	gpio_cfg.Pin = connector[pin].pin;
	gpio_cfg.Mode = mode | irq;
    gpio_cfg.Pull = pull;
    gpio_cfg.Alternate = alternate;
    gpio_cfg.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

    gpio_clk_enable(pin);

    if(irq != G_IRQ_NONE)
    {
    	uint16_t pin_num;
    	for(pin_num = 0; pin_num < EXTI_LINES_CNT; ++pin_num)
    	{
    		if(connector[pin].pin >> pin_num & (uint16_t)1)
    			break;
    	}
    	gpio_irq_callbacks[pin_num].callback = irq_callback;
    	gpio_irq_callbacks[pin_num].data = data;

    	enable_irq(connector[pin].pin);
    }

    HAL_GPIO_Init(connector[pin].port, &gpio_cfg);

	return 0;
}

inline void gpio_soc_deinit(size_t pin)
{
	if(pin >= sizeof(connector) / sizeof(connector[0]))
	{
		assert_param(1);
	}

	disable_irq(connector[pin].pin);
	HAL_GPIO_DeInit(connector[pin].port, connector[pin].pin);

	gpio_clk_disable(pin);
}

inline uint32_t gpio_soc_read_pin(size_t pin)
{
	if(pin >= sizeof(connector) / sizeof(connector[0]))
	{
		assert_param(1);
	}

	return HAL_GPIO_ReadPin(connector[pin].port, connector[pin].pin);
}

inline void gpio_soc_write_pin(size_t pin, uint32_t state)
{
	if(pin >= sizeof(connector) / sizeof(connector[0]))
	{
		assert_param(1);
	}

	HAL_GPIO_WritePin(connector[pin].port, connector[pin].pin, state);
}

inline void gpio_soc_toggle_pin(size_t pin)
{
	if(pin >= sizeof(connector) / sizeof(connector[0]))
	{
		assert_param(1);
	}

	HAL_GPIO_TogglePin(connector[pin].port, connector[pin].pin);
}

static void gpio_clk_enable(size_t pin)
{
	if(connector[pin].port == GPIOA)
	{
		if(gpio_a_cnt == 0)
			__HAL_RCC_GPIOA_CLK_ENABLE();
		else if(gpio_a_cnt == 255)
			assert_param(1);
		++gpio_a_cnt;
	}
	else if(connector[pin].port == GPIOB)
	{
		if(gpio_b_cnt == 0)
			__HAL_RCC_GPIOB_CLK_ENABLE();
		else if(gpio_b_cnt == 255)
			assert_param(1);
		++gpio_b_cnt;
	}
	else if(connector[pin].port == GPIOC)
	{
		if(gpio_c_cnt == 0)
			__HAL_RCC_GPIOC_CLK_ENABLE();
		else if(gpio_c_cnt == 255)
			assert_param(1);
		++gpio_c_cnt;
	}
	else if(connector[pin].port == GPIOD)
	{
		if(gpio_d_cnt == 0)
			__HAL_RCC_GPIOD_CLK_ENABLE();
		else if(gpio_d_cnt == 255)
			assert_param(1);
		++gpio_d_cnt;
	}
	else if(connector[pin].port == GPIOE)
	{
		if(gpio_e_cnt == 0)
			__HAL_RCC_GPIOE_CLK_ENABLE();
		else if(gpio_e_cnt == 255)
			assert_param(1);
		++gpio_e_cnt;
	}
	else if(connector[pin].port == GPIOF)
	{
		if(gpio_f_cnt == 0)
			__HAL_RCC_GPIOF_CLK_ENABLE();
		else if(gpio_f_cnt == 255)
			assert_param(1);
		++gpio_f_cnt;
	}
	else if(connector[pin].port == GPIOG)
	{
		if(gpio_g_cnt == 0)
			__HAL_RCC_GPIOG_CLK_ENABLE();
		else if(gpio_g_cnt == 255)
			assert_param(1);
		++gpio_g_cnt;
	}
}

static void gpio_clk_disable(size_t pin)
{
	if(connector[pin].port == GPIOA)
	{
		if(gpio_a_cnt == 0)
			return;
		if(--gpio_a_cnt == 0)
			__HAL_RCC_GPIOA_CLK_DISABLE();
	}
	else if(connector[pin].port == GPIOB)
	{
		if(gpio_b_cnt == 0)
			return;
		if(--gpio_b_cnt == 0)
			__HAL_RCC_GPIOB_CLK_DISABLE();
	}
	else if(connector[pin].port == GPIOC)
	{
		if(gpio_c_cnt == 0)
			return;
		if(--gpio_c_cnt == 0)
			__HAL_RCC_GPIOC_CLK_DISABLE();
	}
	else if(connector[pin].port == GPIOD)
	{
		if(gpio_d_cnt == 0)
			return;
		if(--gpio_d_cnt == 0)
			__HAL_RCC_GPIOD_CLK_DISABLE();
	}
	else if(connector[pin].port == GPIOE)
	{
		if(gpio_e_cnt == 0)
			return;
		if(--gpio_e_cnt == 0)
			__HAL_RCC_GPIOE_CLK_DISABLE();
	}
	else if(connector[pin].port == GPIOD)
	{
		if(gpio_f_cnt == 0)
			return;
		if(--gpio_f_cnt == 0)
			__HAL_RCC_GPIOF_CLK_DISABLE();
	}
	else if(connector[pin].port == GPIOG)
	{
		if(gpio_g_cnt == 0)
			return;
		if(--gpio_g_cnt == 0)
			__HAL_RCC_GPIOG_CLK_DISABLE();
	}
}

static void enable_irq(size_t pin)
{
	if(pin == GPIO_PIN_0)
	{
		HAL_NVIC_SetPriority(EXTI0_IRQn, 7, 0);
    	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	}
	else if(pin == GPIO_PIN_1)
	{
		HAL_NVIC_SetPriority(EXTI1_IRQn, 7, 0);
    	HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	}
	else if(pin == GPIO_PIN_2)
	{
		HAL_NVIC_SetPriority(EXTI2_IRQn, 7, 0);
    	HAL_NVIC_EnableIRQ(EXTI2_IRQn);
	}
	else if(pin == GPIO_PIN_3)
	{
		HAL_NVIC_SetPriority(EXTI3_IRQn, 7, 0);
    	HAL_NVIC_EnableIRQ(EXTI3_IRQn);
	}
	else if(pin == GPIO_PIN_4)
	{
		HAL_NVIC_SetPriority(EXTI4_IRQn, 7, 0);
    	HAL_NVIC_EnableIRQ(EXTI4_IRQn);
	}
	else if(pin >= GPIO_PIN_5 && pin <= GPIO_PIN_9)
	{
		HAL_NVIC_SetPriority(EXTI9_5_IRQn, 7, 0);
    	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
	}
	else if(pin >= GPIO_PIN_10 && pin <= GPIO_PIN_15)
	{
		HAL_NVIC_SetPriority(EXTI15_10_IRQn, 7, 0);
    	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
	}
}

static void disable_irq(size_t pin)
{
	if(pin == GPIO_PIN_0)
	{
		HAL_NVIC_DisableIRQ(EXTI0_IRQn);
	}
	else if(pin == GPIO_PIN_1)
	{
		HAL_NVIC_DisableIRQ(EXTI1_IRQn);
	}
	else if(pin == GPIO_PIN_2)
	{
		HAL_NVIC_DisableIRQ(EXTI2_IRQn);
	}
	else if(pin == GPIO_PIN_3)
	{
		HAL_NVIC_DisableIRQ(EXTI3_IRQn);
	}
	else if(pin == GPIO_PIN_4)
	{
		HAL_NVIC_DisableIRQ(EXTI4_IRQn);
	}
	else if(pin >= GPIO_PIN_5 && pin <= GPIO_PIN_9)
	{
		HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
	}
	else if(pin >= GPIO_PIN_5 && pin <= GPIO_PIN_9)
	{
		HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{

	    uint16_t pin_num;
    	for(pin_num = 0; pin_num < EXTI_LINES_CNT; ++pin_num)
    	{
    		if(GPIO_Pin >> pin_num & (uint16_t)1)
    			break;
    	}

    	if(gpio_irq_callbacks[pin_num].callback)
    	{
    		gpio_irq_callbacks[pin_num].callback(gpio_irq_callbacks[pin_num].data);
    	}
}

void EXTI0_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

void EXTI1_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

void EXTI2_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

void EXTI3_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

void EXTI4_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
}

void EXTI9_5_IRQHandler(void)
{
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_5))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
	}
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_6))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_6);
	}
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_7))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_7);
	}
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_8))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8);
	}
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_9))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_9);
	}
}

void EXTI15_10_IRQHandler(void)
{
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_10))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_10);
	}
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_11))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_11);
	}
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_12))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_12);
	}
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_13))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
	}
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_14))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_14);
	}
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_15))
	{
		HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_15);
	}
}
