/*
 * Copyright (c) 2020 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>

#include "flash_soc.h"

 /* Base address of the Flash sectors Bank 1 */
 #define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of sector 0, 16 Kbytes */
 #define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of sector 1, 16 Kbytes */
 #define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of sector 2, 16 Kbytes */
 #define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of sector 3, 16 Kbytes */
 #define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of sector 4, 64 Kbytes */
 #define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of sector 5, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of sector 6, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of sector 7, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of sector 8, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of sector 9, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of sector 10, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of sector 11, 128 Kbytes */
 
 /* Base address of the Flash sectors Bank 2 */
 #define ADDR_FLASH_SECTOR_12     ((uint32_t)0x08100000) /* Base @ of sector 0, 16 Kbytes */
 #define ADDR_FLASH_SECTOR_13     ((uint32_t)0x08104000) /* Base @ of sector 1, 16 Kbytes */
 #define ADDR_FLASH_SECTOR_14     ((uint32_t)0x08108000) /* Base @ of sector 2, 16 Kbytes */
 #define ADDR_FLASH_SECTOR_15     ((uint32_t)0x0810C000) /* Base @ of sector 3, 16 Kbytes */
 #define ADDR_FLASH_SECTOR_16     ((uint32_t)0x08110000) /* Base @ of sector 4, 64 Kbytes */
 #define ADDR_FLASH_SECTOR_17     ((uint32_t)0x08120000) /* Base @ of sector 5, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_18     ((uint32_t)0x08140000) /* Base @ of sector 6, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_19     ((uint32_t)0x08160000) /* Base @ of sector 7, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_20     ((uint32_t)0x08180000) /* Base @ of sector 8, 128 Kbytes  */
 #define ADDR_FLASH_SECTOR_21     ((uint32_t)0x081A0000) /* Base @ of sector 9, 128 Kbytes  */
 #define ADDR_FLASH_SECTOR_22     ((uint32_t)0x081C0000) /* Base @ of sector 10, 128 Kbytes */
 #define ADDR_FLASH_SECTOR_23     ((uint32_t)0x081E0000) /* Base @ of sector 11, 128 Kbytes */

static uint32_t get_address(size_t sector);
static size_t get_sector(uint32_t address);
static size_t get_sector_size(size_t sector);
static int parital_erase(size_t address, size_t len);

extern uint32_t __FLASH_BEGIN__;
extern uint32_t __FLASH_END__;

static size_t flash_begin = (size_t)&__FLASH_BEGIN__;
static size_t flash_end = (size_t)&__FLASH_END__;

int flash_soc_init(void)
{
	HAL_FLASH_Unlock();
	return 0;
}

void flash_soc_deinit(void)
{
	HAL_FLASH_Lock();
}

size_t flash_soc_get_begin(void)
{
	return flash_begin;
}

size_t flash_soc_get_end(void)
{
	return (flash_end + 1) - get_sector_size(get_sector(flash_end));
}

int flash_soc_erase(size_t address, size_t len)
{
	size_t start_sector = get_sector(address);
	size_t end_sector = get_sector(address + len - 1);
	size_t bytes_to_erase = len;
	uint32_t sector_error;
	
	if (address < flash_begin || address > flash_end)
		return -1;

	FLASH_EraseInitTypeDef erase_init;
	erase_init.Sector = start_sector;
	erase_init.TypeErase = FLASH_TYPEERASE_SECTORS;
	erase_init.VoltageRange = FLASH_VOLTAGE_RANGE_3;
	erase_init.NbSectors = 1;
	
	if (get_address(start_sector) != address || len < get_sector_size(start_sector))
	{
		bytes_to_erase -= parital_erase(address, get_sector_size(start_sector) - address + get_address(start_sector) < len ? get_sector_size(start_sector) - address + get_address(start_sector) : len);
		erase_init.Sector++;
	}

	while(erase_init.Sector <= end_sector && bytes_to_erase >= get_sector_size(end_sector))
	{
		HAL_FLASHEx_Erase(&erase_init, &sector_error);
		bytes_to_erase -= get_sector_size(erase_init.Sector);
		erase_init.Sector++;
	}

	if (bytes_to_erase && bytes_to_erase < get_sector_size(end_sector))
	{
		parital_erase(get_address(end_sector), bytes_to_erase);
	}
	
	return 0;
}

int flash_soc_write(size_t address, void *data, size_t len)
{
	size_t words = len / 4;
	size_t half_words = (len - words * 4) / 2;
	size_t bytes = len - words * 4 - half_words * 2;

	for(size_t idx = 0; idx < words; idx++)
	{
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address + 4 * idx, ((uint32_t *)data)[idx]);
	}

	for(size_t idx = 0; idx < half_words; idx++)
	{
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, address + 4 * words + 2 * idx, ((uint16_t *)data)[2 * words + idx]);
	}

	for(size_t idx = 0; idx < bytes; idx++)
	{
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, address + 4 * words + 2 * half_words + idx, ((uint16_t *)data)[4 * words + 2 * half_words + idx]);
	}


	return len;
}

int flash_soc_read(size_t address, void *data, size_t len)
{
	memcpy((uint8_t *)data, (uint32_t *)address, len);
	return 0;
}

static uint32_t get_address(size_t sector)
{
	uint32_t address = ADDR_FLASH_SECTOR_0;

	for (size_t idx = FLASH_SECTOR_0; idx < sector; idx++)
		address += get_sector_size(idx);

	return address;
}

static size_t get_sector(uint32_t address)
{
	if((address < ADDR_FLASH_SECTOR_1) && (address >= ADDR_FLASH_SECTOR_0))
		return FLASH_SECTOR_0;
	if((address < ADDR_FLASH_SECTOR_2) && (address >= ADDR_FLASH_SECTOR_1))
		return FLASH_SECTOR_1;
	if((address < ADDR_FLASH_SECTOR_3) && (address >= ADDR_FLASH_SECTOR_2))
		return FLASH_SECTOR_2;
	if((address < ADDR_FLASH_SECTOR_4) && (address >= ADDR_FLASH_SECTOR_3))
		return FLASH_SECTOR_3;
	if((address < ADDR_FLASH_SECTOR_5) && (address >= ADDR_FLASH_SECTOR_4))
		return FLASH_SECTOR_4;
	if((address < ADDR_FLASH_SECTOR_6) && (address >= ADDR_FLASH_SECTOR_5))
		return FLASH_SECTOR_5;
	if((address < ADDR_FLASH_SECTOR_7) && (address >= ADDR_FLASH_SECTOR_6))
		return FLASH_SECTOR_6;
	if((address < ADDR_FLASH_SECTOR_8) && (address >= ADDR_FLASH_SECTOR_7))
		return FLASH_SECTOR_7;
	if((address < ADDR_FLASH_SECTOR_9) && (address >= ADDR_FLASH_SECTOR_8))
		return FLASH_SECTOR_8;
	if((address < ADDR_FLASH_SECTOR_10) && (address >= ADDR_FLASH_SECTOR_9))
		return FLASH_SECTOR_9;
	if((address < ADDR_FLASH_SECTOR_11) && (address >= ADDR_FLASH_SECTOR_10))
		return FLASH_SECTOR_10;
	if((address < ADDR_FLASH_SECTOR_12) && (address >= ADDR_FLASH_SECTOR_11))
		return FLASH_SECTOR_11;
	if((address < ADDR_FLASH_SECTOR_13) && (address >= ADDR_FLASH_SECTOR_12))
		return FLASH_SECTOR_12;
	if((address < ADDR_FLASH_SECTOR_14) && (address >= ADDR_FLASH_SECTOR_13))
		return FLASH_SECTOR_13;
	if((address < ADDR_FLASH_SECTOR_15) && (address >= ADDR_FLASH_SECTOR_14))
		return FLASH_SECTOR_14;
	if((address < ADDR_FLASH_SECTOR_16) && (address >= ADDR_FLASH_SECTOR_15))
		return FLASH_SECTOR_15;
	if((address < ADDR_FLASH_SECTOR_17) && (address >= ADDR_FLASH_SECTOR_16))
		return FLASH_SECTOR_16;
	if((address < ADDR_FLASH_SECTOR_18) && (address >= ADDR_FLASH_SECTOR_17))
		return FLASH_SECTOR_17;
	if((address < ADDR_FLASH_SECTOR_19) && (address >= ADDR_FLASH_SECTOR_18))
		return FLASH_SECTOR_18;
	if((address < ADDR_FLASH_SECTOR_20) && (address >= ADDR_FLASH_SECTOR_19))
		return FLASH_SECTOR_19;
	if((address < ADDR_FLASH_SECTOR_21) && (address >= ADDR_FLASH_SECTOR_20))
		return FLASH_SECTOR_20;
	if((address < ADDR_FLASH_SECTOR_22) && (address >= ADDR_FLASH_SECTOR_21))
		return FLASH_SECTOR_21;
	if((address < ADDR_FLASH_SECTOR_23) && (address >= ADDR_FLASH_SECTOR_22))
		return FLASH_SECTOR_22;
	/* (address < FLASH_END_ADDR) && (address >= ADDR_FLASH_SECTOR_23) */
	return FLASH_SECTOR_23;
}

static size_t get_sector_size(size_t sector)
{
	if((sector == FLASH_SECTOR_0) || (sector == FLASH_SECTOR_1) || (sector == FLASH_SECTOR_2) ||\
	   (sector == FLASH_SECTOR_3) || (sector == FLASH_SECTOR_12) || (sector == FLASH_SECTOR_13) ||\
	   (sector == FLASH_SECTOR_14) || (sector == FLASH_SECTOR_15))
	{
    	return 16 * 1024;
    }

	if((sector == FLASH_SECTOR_4) || (sector == FLASH_SECTOR_16))
	{
		return 64 * 1024;
	}
	
	return 128 * 1024;
}

static int parital_erase(size_t address, size_t len)
{
	size_t sector = get_sector(address);
	uint32_t sector_error;
	FLASH_EraseInitTypeDef erase_init;
	erase_init.TypeErase = FLASH_TYPEERASE_SECTORS;
	erase_init.VoltageRange = FLASH_VOLTAGE_RANGE_3;
	erase_init.NbSectors = 1;

	erase_init.Sector = get_sector(flash_end);
	HAL_FLASHEx_Erase(&erase_init, &sector_error);	

	size_t tmp = flash_soc_write (get_address(get_sector(flash_end)), (uint32_t *)(get_address(sector)), address - get_address(sector));
	flash_soc_write (get_address(get_sector(flash_end)) + tmp + len, (uint32_t *)(get_address(sector) + tmp + len), get_sector_size(get_sector(flash_end)) - len - tmp);

	erase_init.Sector = sector;
	HAL_FLASHEx_Erase(&erase_init, &sector_error);

	flash_soc_write (get_address(sector), (uint32_t *)get_address(get_sector(flash_end)), get_sector_size(get_sector(flash_end)));

	return len;
}