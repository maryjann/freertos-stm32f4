/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>

#include "stm32f4xx_hal_conf.h"

#include "spi_soc.h"
#include "spi_bsp.h"

typedef struct spi_handle_s
{
	SPI_HandleTypeDef spi_hal_handle;
	spi_op_clbk_t clbk;
	void *user;
} spi_handle_t;

static spi_handle_t spi_handles[SPI_BUS_END];

int spi_soc_init(spi_bus_t bus, uint32_t clock_speed)
{
	spi_handle_t *spi_handle = &spi_handles[bus];
	memset( spi_handle, 0, sizeof(spi_handle_t) );

	if( bus == SPI_BUS_1 )
	{
		spi_handle->spi_hal_handle.Instance = SPI1;
	}
	else
	{
		return -1;
	}

	spi_handle->spi_hal_handle.Init.Mode = SPI_MODE_MASTER;
	spi_handle->spi_hal_handle.Init.Direction = SPI_DIRECTION_2LINES;
	spi_handle->spi_hal_handle.Init.DataSize = SPI_DATASIZE_8BIT;
	spi_handle->spi_hal_handle.Init.CLKPolarity = SPI_POLARITY_HIGH;
	spi_handle->spi_hal_handle.Init.CLKPhase = SPI_PHASE_2EDGE;
	spi_handle->spi_hal_handle.Init.NSS = SPI_NSS_SOFT;
	spi_handle->spi_hal_handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
	spi_handle->spi_hal_handle.Init.FirstBit = SPI_FIRSTBIT_MSB;
	spi_handle->spi_hal_handle.Init.TIMode = SPI_TIMODE_DISABLE;
	spi_handle->spi_hal_handle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	spi_handle->spi_hal_handle.Init.CRCPolynomial = 7;

	HAL_SPI_Init(&spi_handle->spi_hal_handle);

	return 0;
}

void spi_soc_deinit(spi_bus_t bus)
{
}

int spi_soc_send(spi_bus_t bus, uint8_t *data, size_t length, spi_op_clbk_t clbk, void *user)
{
	HAL_StatusTypeDef ret = HAL_ERROR;

	if (bus >= SPI_BUS_END)
		return -1;

	spi_handle_t *spi_handle = &spi_handles[bus];

	if (clbk == NULL)
	{
		spi_handle->clbk = NULL;
		spi_handle->user = NULL;
	}
	else
	{
		spi_handle->clbk = clbk;
		spi_handle->user = user;

	}
	//HAL_SPI_Transmit(&spi_handle->spi_hal_handle, data, length, 0xDEADBEEF);
	ret = HAL_SPI_Transmit_IT(&spi_handle->spi_hal_handle, data, length);
	if (ret != HAL_OK)
		return -ret;

	return 0;
}

int spi_soc_recv(spi_bus_t bus, uint8_t *data, size_t length, spi_op_clbk_t clbk, void *user)
{
	HAL_StatusTypeDef ret = HAL_ERROR;

	if (bus >= SPI_BUS_END)
		return -1;

	spi_handle_t *spi_handle = &spi_handles[bus];

	if (clbk == NULL)
	{
		spi_handle->clbk = NULL;
		spi_handle->user = NULL;
	}
	else
	{
		spi_handle->clbk = clbk;
		spi_handle->user = user;
	}

	ret = HAL_SPI_Receive_IT(&spi_handle->spi_hal_handle, data, length);
	if (ret != HAL_OK)
		return -ret;

	return 0;
}

int spi_soc_send_recv(spi_bus_t bus, uint8_t *send, uint8_t *recv, spi_op_clbk_t clbk, size_t length, void *user)
{
	HAL_StatusTypeDef ret = HAL_ERROR;

	if (bus >= SPI_BUS_END)
		return -1;

	spi_handle_t *spi_handle = &spi_handles[bus];

	if (clbk == NULL)
	{
		spi_handle->clbk = NULL;
		spi_handle->user = NULL;
	}
	else
	{
		spi_handle->clbk = clbk;
		spi_handle->user = user;
	}

	ret = HAL_SPI_TransmitReceive_IT(&spi_handle->spi_hal_handle, send, recv, length);
	if (ret != HAL_OK)
		return -ret;

	return 0;
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(((spi_handle_t *)hspi)->clbk != NULL)
	{
		((spi_handle_t *)hspi)->clbk(hspi->ErrorCode, ((spi_handle_t *)hspi)->user);
	}
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(((spi_handle_t *)hspi)->clbk != NULL)
	{
		((spi_handle_t *)hspi)->clbk(hspi->ErrorCode, ((spi_handle_t *)hspi)->user);
	}
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(((spi_handle_t *)hspi)->clbk != NULL)
	{
		((spi_handle_t *)hspi)->clbk(hspi->ErrorCode, ((spi_handle_t *)hspi)->user);
	}
}

void SPI1_IRQHandler(void)
{
	HAL_SPI_IRQHandler((SPI_HandleTypeDef *)&spi_handles[SPI_BUS_1]);
}
