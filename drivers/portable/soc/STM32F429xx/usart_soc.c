/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>

#include <FreeRTOS.h>
#include "task.h"
#include "queue.h"

#include "stm32f4xx_hal_conf.h"

#include "usart_soc.h"
#include <tools/container_of.h>

#define USART_SOC_RETRIES     5

typedef struct usart_handle_s {
	UART_HandleTypeDef handle;
	usart_send_done_clbk tx_clbk;
	void *tx_data;
	usart_recv_done_clbk rx_clbk;
	void *rx_data;
} usart_handle_t;

static inline usart_t USART_TypeDef_2_usart_t(USART_TypeDef *Instance);

static usart_handle_t usart_handles[USART_PORT_END];

int usart_soc_init(usart_t port, uint32_t baud_rate, usart_world_length_t world_length, usart_stop_bits_t stop_bits, usart_parity_t parity, usart_hw_control_t hw_control) {
	if (port > USART_PORT_END)
		return -1;

	memset(&usart_handles[port], 0, sizeof(usart_handle_t));

	switch(port) {
		case USART_PORT_1:
			usart_handles[port].handle.Instance = USART1;
			break;
		case USART_PORT_2:
			usart_handles[port].handle.Instance = USART2;
			break;
		case USART_PORT_3:
			usart_handles[port].handle.Instance = USART3;
			break;
		case USART_PORT_6:
			usart_handles[port].handle.Instance = USART6;
			break;
		default:
			return -2;
	}

    usart_handles[port].handle.Init.BaudRate = baud_rate;
    usart_handles[port].handle.Init.WordLength = world_length;
    usart_handles[port].handle.Init.StopBits = stop_bits;
    usart_handles[port].handle.Init.Parity = parity;
    usart_handles[port].handle.Init.Mode = UART_MODE_TX_RX;
    usart_handles[port].handle.Init.HwFlowCtl = hw_control;
    usart_handles[port].handle.Init.OverSampling = UART_OVERSAMPLING_16;

    HAL_UART_Init(&usart_handles[port].handle);

	return 0;
}

void usart_soc_deinit(usart_t port) {
	return;
}

int usart_soc_send(usart_t port, uint8_t *data, size_t length, usart_send_done_clbk clbk, void *clbk_data) {
	if (port > USART_PORT_END)
		return -1;
	size_t retries = USART_SOC_RETRIES;
	usart_handles[port].tx_clbk = clbk;
	usart_handles[port].tx_data = clbk_data;
	while(retries) {
		int ret = HAL_UART_Transmit_IT(&usart_handles[port].handle, data, length);
		if (ret == HAL_OK) {
			break;
		}
		retries--;
	}
	if (retries == 0) {
		return -2;
	}

	return 0;
}

int usart_soc_abort_send(usart_t port) {
	HAL_UART_AbortTransmit_IT(&usart_handles[port].handle);

	if (usart_handles[port].tx_clbk != NULL) {
		usart_handles[port].tx_clbk(usart_handles[port].tx_data, -1);
	}

	return 0;
}

int usart_soc_recv(usart_t port, uint8_t *data, size_t length, usart_recv_done_clbk clbk, void *clbk_data) {
	if (port > USART_PORT_END)
		return -1;
	size_t retries = USART_SOC_RETRIES;

	usart_handles[port].rx_clbk = clbk;
	usart_handles[port].rx_data = clbk_data;
	while(retries) {
		if (HAL_UART_Receive_IT(&usart_handles[port].handle, data, length) == HAL_OK) {
			break;
		}
		retries--;		
	}
	if (retries == 0) {
		return -2;
	}

	return 0;
}

int usart_soc_abort_recv(usart_t port) {
	HAL_UART_AbortReceive_IT(&usart_handles[port].handle);

	if (usart_handles[port].rx_clbk != NULL) {
		usart_handles[port].rx_clbk(usart_handles[port].rx_data, -1);
	}

	return 0;
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
	usart_handle_t *usart_handle = container_of(huart, usart_handle_t, handle);

	if (usart_handle == NULL)
		return;

	usart_handle->tx_clbk(usart_handle->tx_data, 0);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	usart_handle_t *usart_handle = (usart_handle_t *)huart;

	if (usart_handle == NULL || usart_handle->rx_clbk == NULL)
		return;

	usart_handle->rx_clbk(usart_handle->rx_data, huart->RxXferSize);
}

static inline usart_t USART_TypeDef_2_usart_t(USART_TypeDef *Instance) {
	if(Instance == USART1) return USART_PORT_1;
	if(Instance == USART2) return USART_PORT_2;
	if(Instance == USART3) return USART_PORT_3;
	if(Instance == USART6) return USART_PORT_6;

	return USART_PORT_END;
}

void USART1_IRQHandler(void)
{
	HAL_UART_IRQHandler(&usart_handles[USART_PORT_1].handle);
}

void USART2_IRQHandler(void)
{
	HAL_UART_IRQHandler(&usart_handles[USART_PORT_2].handle);
}

void USART3_IRQHandler(void)
{
	HAL_UART_IRQHandler(&usart_handles[USART_PORT_3].handle);
}

void USART6_IRQHandler(void)
{
	HAL_UART_IRQHandler(&usart_handles[USART_PORT_6].handle);
}
