/*
 * Copyright (c) 2019 Damian Eppel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __DMA_SOC_H__
#define __DMA_SOC_H__

#include <inttypes.h>
#include <unistd.h>

typedef void (*dma_isr_cb_t)(int flag, void *);

#define DMA_HT_IRQ_ENABLED (1 << 0)
#define DMA_TC_IRQ_ENABLED (1 << 1)
#define DMA_CONTROLLER(_ctrl_nr) (_ctrl_nr << 4)
#define DMA_STREAM(_stream_nr) (_stream_nr << 0)
#define DMA_HANDLE_GET_CTRL_NR(_handle) ((_handle >> 4) & 0x3)
#define DMA_HANLDE_GET_STREAM_NR(_handle) ((_handle >> 0) & 0x7)

enum dma_controller {
    DMA_CONTROLLER_1 = 1,
    DMA_CONTROLLER_2,

    DMA_CONTROLLER_LAST = DMA_CONTROLLER_2
};

enum dma_stream {
    DMA_STREAM_0,
    DMA_STREAM_1,
    DMA_STREAM_2,
    DMA_STREAM_3,
    DMA_STREAM_4,
    DMA_STREAM_5,
    DMA_STREAM_6,
    DMA_STREAM_7,

    DMA_STREAM_LAST = DMA_STREAM_7,
    DMA_STREAM_COUNT
};

enum dma_channel {
    DMA_CHNL_0,
    DMA_CHNL_1,
    DMA_CHNL_2,
    DMA_CHNL_3,
    DMA_CHNL_4,
    DMA_CHNL_5,
    DMA_CHNL_6,
    DMA_CHNL_7,

    DMA_CHNL_LAST = DMA_CHNL_7
};

enum dma_data_align {
    DMA_DATA_ALIGN_BYTE,
    DMA_DATA_ALIGN_HALFWORD,
    DMA_DATA_ALIGN_WORD
};

enum dma_increment_mode {
    DMA_ADDR_NOINCREMENT,
    DMA_ADDR_INCREMENT
};

enum dma_run_mode {
    DMA_MODE_NORMAL,
    DMA_MODE_CIRCULAR
};

enum dma_irq_type {
    DMA_IRQ_HT,
    DMA_IRQ_TC
};

enum dma_handle {
    DMA1_STREAM0 = DMA_CONTROLLER(1) | DMA_STREAM(0),
    DMA1_STREAM1 = DMA_CONTROLLER(1) | DMA_STREAM(1),
    DMA1_STREAM2 = DMA_CONTROLLER(1) | DMA_STREAM(2),
    DMA1_STREAM3 = DMA_CONTROLLER(1) | DMA_STREAM(3),
    DMA1_STREAM4 = DMA_CONTROLLER(1) | DMA_STREAM(4),
    DMA1_STREAM5 = DMA_CONTROLLER(1) | DMA_STREAM(5),
    DMA1_STREAM6 = DMA_CONTROLLER(1) | DMA_STREAM(6),
    DMA1_STREAM7 = DMA_CONTROLLER(1) | DMA_STREAM(7),

    DMA2_STREAM0 = DMA_CONTROLLER(2) | DMA_STREAM(0),
    DMA2_STREAM1 = DMA_CONTROLLER(2) | DMA_STREAM(1),
    DMA2_STREAM2 = DMA_CONTROLLER(2) | DMA_STREAM(2),
    DMA2_STREAM3 = DMA_CONTROLLER(2) | DMA_STREAM(3),
    DMA2_STREAM4 = DMA_CONTROLLER(2) | DMA_STREAM(4),
    DMA2_STREAM5 = DMA_CONTROLLER(2) | DMA_STREAM(5),
    DMA2_STREAM6 = DMA_CONTROLLER(2) | DMA_STREAM(6),
    DMA2_STREAM7 = DMA_CONTROLLER(2) | DMA_STREAM(7),
};

struct dma2periph_config {
    enum dma_channel channel;
    enum dma_data_align periph_align;
    enum dma_data_align memory_align;
    enum dma_increment_mode periph_inc;
    enum dma_increment_mode memory_inc;
    uint32_t periph_addr;
    int dma_irq_prio;
    dma_isr_cb_t dma_cb;
    void *cb_priv;
};

typedef enum dma_handle dma_handle_t;

int dma2periph_setup(dma_handle_t dma_handle,
                     struct dma2periph_config *user_setup);

void dma2periph_start(dma_handle_t dma_handle,
                      uint32_t mem_addr,
                      size_t size,
                      int ht_tc_flags,
                      enum dma_run_mode mode);

void dma2periph_stop(dma_handle_t dma_handle);


#endif //__DMA_SOC_H__
