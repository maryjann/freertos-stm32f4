/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __ENCODER_SOC_H__
#define __ENCODER_SOC_H__

#include "stm32f4xx_hal_conf.h"

#include "encoder_bsp.h"

typedef enum ecooder_config_e {
	ENCODER_CHANNEL_CFG_1,
	ENCODER_CHANNEL_CFG_2,
	ENCODER_CHANNEL_CFG_12,
	ENCODER_CHANNEL_CFG_INVALID
} encoder_config_t;

typedef enum encoder_dir_e {
	ENCODER_DIR_INVALID = 0,
	ENCODER_DIR_UP,
	ENCODER_DIR_DOWN
} encoder_dir_t;

typedef enum encoder_type_e {
	ENCODER_TYPE_INVALID = 0,
	ENCODER_TYPE_SINGLE,
	ENCODER_TYPE_DUAL,
} encoder_type_t;

typedef struct encoder_handle_s {
	TIM_HandleTypeDef encoder_hal_handle;
	TIM_Encoder_InitTypeDef encoder_hal_config;
	encoder_type_t encoder_type;
	int32_t overflow_cnt;
} encoder_handle_t;

int encoder_soc_init(encoder_timer_t timer, encoder_type_t type);
void encoder_soc_deinit(encoder_timer_t timer);

encoder_dir_t encoder_soc_get_direction(encoder_timer_t timer);
int32_t encoder_soc_get_value(encoder_timer_t timer);

void encoder_soc_handle_irq(encoder_timer_t timer);

#endif
