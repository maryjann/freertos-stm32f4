/*
 * Copyright (c) 2019 Damian Eppel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __WS2812B_SOC_H__
#define __WS2812B_SOC_H__

#include <inttypes.h>
#include <unistd.h>

#include "dma_soc.h"

#define WS2812B_PWM_FREQ			800000

#define LED_CFG_BYTES_PER_LED		3
#define LED_CFG_RAW_BYTES_PER_LED	(LED_CFG_BYTES_PER_LED * 8)



int ws2812b_soc_dma2timer_init(dma_isr_cb_t cb, uint32_t *t0h, uint32_t *t1h);

void ws2812b_soc_dma2timer_start(uint32_t mem_addr,
                                 size_t size,
                                 int ht_tc_flags,
                                 enum dma_run_mode mode);

void ws2812b_soc_dma2timer_stop();


#endif //__WS2812B_SOC_H__
