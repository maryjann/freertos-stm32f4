/*
 * Copyright (c) 2019 Damian Eppel
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_ll_bus.h"
#include <string.h>

#include "dma_soc.h"

#define DMA_IRQ_HANDLER(_ctrl, _stream) \
void DMA##_ctrl##_Stream##_stream##_IRQHandler(void) {\
    \
    struct dma_setup *setup = &dma##_ctrl[DMA_STREAM_##_stream];\
    \
    if (dma_is_active_flag_ht(DMA##_ctrl, DMA_STREAM_##_stream)) {\
        dma_clear_flag_ht(DMA##_ctrl, DMA_STREAM_##_stream);\
        \
        if (setup->dma_cb)\
            setup->dma_cb(DMA_IRQ_HT, setup->cb_priv);\
    } else if (dma_is_active_flag_tc(DMA##_ctrl, DMA_STREAM_##_stream)) {\
        dma_clear_flag_tc(DMA##_ctrl, DMA_STREAM_##_stream);\
        \
        if (setup->dma_cb)\
            setup->dma_cb(DMA_IRQ_TC, setup->cb_priv);\
    }\
}

enum dma_op_mode {
    UNUSED,
    DMA2PERIPH
};

enum dma_status {
    UNINITIALIZED,
    READY,
    BUSY
};

struct dma_setup {

    enum dma_status status;
    enum dma_op_mode mode;
    dma_isr_cb_t dma_cb;
    void *cb_priv;
};

static struct dma_setup dma1[DMA_STREAM_COUNT];
static struct dma_setup dma2[DMA_STREAM_COUNT];

static inline DMA_TypeDef * handle_get_controller(dma_handle_t dma)
{
    switch(DMA_HANDLE_GET_CTRL_NR(dma)) {

    case DMA_CONTROLLER_1:
        return DMA1;

    case DMA_CONTROLLER_2:
        return DMA2;

    default:
        return NULL;
    }
}

static inline uint32_t handle_get_stream(dma_handle_t dma) {

    switch(DMA_HANLDE_GET_STREAM_NR(dma)) {

    case DMA_STREAM_0:
        return LL_DMA_STREAM_0;

    case DMA_STREAM_1:
        return LL_DMA_STREAM_1;

    case DMA_STREAM_2:
        return LL_DMA_STREAM_2;

    case DMA_STREAM_3:
        return LL_DMA_STREAM_3;

    case DMA_STREAM_4:
        return LL_DMA_STREAM_4;

    case DMA_STREAM_5:
        return LL_DMA_STREAM_5;

    case DMA_STREAM_6:
        return LL_DMA_STREAM_6;

    case DMA_STREAM_7:
        return LL_DMA_STREAM_7;

    default:
        return 0;
    }
}

static inline uint32_t get_channel(enum dma_channel channel) {

    switch(channel) {

    case DMA_CHNL_0:
        return LL_DMA_CHANNEL_0;

    case DMA_CHNL_1:
        return LL_DMA_CHANNEL_1;

    case DMA_CHNL_2:
        return LL_DMA_CHANNEL_2;

    case DMA_CHNL_3:
        return LL_DMA_CHANNEL_3;

    case DMA_CHNL_4:
        return LL_DMA_CHANNEL_4;

    case DMA_CHNL_5:
        return LL_DMA_CHANNEL_5;

    case DMA_CHNL_6:
        return LL_DMA_CHANNEL_6;

    case DMA_CHNL_7:
        return LL_DMA_CHANNEL_7;

    default:
        return 0;
    }
}

static inline uint32_t get_pdata_align(enum dma_data_align align) {

    switch(align) {

    case DMA_DATA_ALIGN_BYTE:
        return LL_DMA_PDATAALIGN_BYTE;

    case DMA_DATA_ALIGN_HALFWORD:
        return LL_DMA_PDATAALIGN_HALFWORD;

    case DMA_DATA_ALIGN_WORD:
        return LL_DMA_PDATAALIGN_WORD;

    default:
        return 0;
    }
}

static inline uint32_t get_mdata_align(enum dma_data_align align) {

    switch(align) {

    case DMA_DATA_ALIGN_BYTE:
        return LL_DMA_MDATAALIGN_BYTE;

    case DMA_DATA_ALIGN_HALFWORD:
        return LL_DMA_MDATAALIGN_HALFWORD;

    case DMA_DATA_ALIGN_WORD:
        return LL_DMA_MDATAALIGN_WORD;

    default:
        return 0;
    }
}

static inline int get_irq_number(enum dma_controller dma_ctrl, enum dma_stream stream)
{
    if (dma_ctrl == DMA_CONTROLLER_1) {
        switch (stream) {

        case DMA_STREAM_0:
            return DMA1_Stream0_IRQn;

        case DMA_STREAM_1:
            return DMA1_Stream1_IRQn;

        case DMA_STREAM_2:
            return DMA1_Stream2_IRQn;

        case DMA_STREAM_3:
            return DMA1_Stream3_IRQn;

        case DMA_STREAM_4:
            return DMA1_Stream4_IRQn;

        case DMA_STREAM_5:
            return DMA1_Stream5_IRQn;

        case DMA_STREAM_6:
            return DMA1_Stream6_IRQn;

        case DMA_STREAM_7:
            return DMA1_Stream7_IRQn;

        default:
            return 0;
        }
    } else if (dma_ctrl == DMA_CONTROLLER_2) {

        switch (stream) {

        case DMA_STREAM_0:
            return DMA2_Stream0_IRQn;

        case DMA_STREAM_1:
            return DMA2_Stream1_IRQn;

        case DMA_STREAM_2:
            return DMA2_Stream2_IRQn;

        case DMA_STREAM_3:
            return DMA2_Stream3_IRQn;

        case DMA_STREAM_4:
            return DMA2_Stream4_IRQn;

        case DMA_STREAM_5:
            return DMA2_Stream5_IRQn;

        case DMA_STREAM_6:
            return DMA2_Stream6_IRQn;

        case DMA_STREAM_7:
            return DMA2_Stream7_IRQn;

        default:
            return 0;
        }
    }

    return 0;
}

static inline uint32_t get_mdata_increment(enum dma_increment_mode inc) {

    switch(inc) {

    case DMA_ADDR_INCREMENT:
        return LL_DMA_MEMORY_INCREMENT;

    case DMA_ADDR_NOINCREMENT:
        return LL_DMA_MEMORY_NOINCREMENT;

    default:
        return 0;
    }
}

static inline uint32_t get_pdata_increment(enum dma_increment_mode inc) {

    switch(inc) {

    case DMA_ADDR_INCREMENT:
        return LL_DMA_PERIPH_INCREMENT;

    case DMA_ADDR_NOINCREMENT:
        return LL_DMA_PERIPH_NOINCREMENT;

    default:
        return 0;
    }
}

static inline void dma_clear_flag_tc(DMA_TypeDef *dma_ctrl, enum dma_stream stream)
{
    switch (stream) {

    case DMA_STREAM_0:
        LL_DMA_ClearFlag_TC0(dma_ctrl);
        break;

    case DMA_STREAM_1:
        LL_DMA_ClearFlag_TC1(dma_ctrl);
        break;

    case DMA_STREAM_2:
        LL_DMA_ClearFlag_TC2(dma_ctrl);
        break;

    case DMA_STREAM_3:
        LL_DMA_ClearFlag_TC3(dma_ctrl);
        break;

    case DMA_STREAM_4:
        LL_DMA_ClearFlag_TC4(dma_ctrl);
        break;

    case DMA_STREAM_5:
        LL_DMA_ClearFlag_TC5(dma_ctrl);
        break;

    case DMA_STREAM_6:
        LL_DMA_ClearFlag_TC6(dma_ctrl);
        break;

    case DMA_STREAM_7:
        LL_DMA_ClearFlag_TC7(dma_ctrl);
        break;

    default:
        break;
    }
}

static inline void dma_clear_flag_ht(DMA_TypeDef *dma_ctrl, enum dma_stream stream)
{
    switch (stream) {

    case DMA_STREAM_0:
        LL_DMA_ClearFlag_HT0(dma_ctrl);
        break;

    case DMA_STREAM_1:
        LL_DMA_ClearFlag_HT1(dma_ctrl);
        break;

    case DMA_STREAM_2:
        LL_DMA_ClearFlag_HT2(dma_ctrl);
        break;

    case DMA_STREAM_3:
        LL_DMA_ClearFlag_HT3(dma_ctrl);
        break;

    case DMA_STREAM_4:
        LL_DMA_ClearFlag_HT4(dma_ctrl);
        break;

    case DMA_STREAM_5:
        LL_DMA_ClearFlag_HT5(dma_ctrl);
        break;

    case DMA_STREAM_6:
        LL_DMA_ClearFlag_HT6(dma_ctrl);
        break;

    case DMA_STREAM_7:
        LL_DMA_ClearFlag_HT7(dma_ctrl);
        break;

    default:
        break;
    }
}


static inline int dma_is_active_flag_tc(DMA_TypeDef *dma_ctrl, enum dma_stream stream)
{
    switch (stream) {

    case DMA_STREAM_0:
        return LL_DMA_IsActiveFlag_TC0(dma_ctrl);

    case DMA_STREAM_1:
        return LL_DMA_IsActiveFlag_TC1(dma_ctrl);

    case DMA_STREAM_2:
        return LL_DMA_IsActiveFlag_TC2(dma_ctrl);

    case DMA_STREAM_3:
        return LL_DMA_IsActiveFlag_TC3(dma_ctrl);

    case DMA_STREAM_4:
        return LL_DMA_IsActiveFlag_TC4(dma_ctrl);

    case DMA_STREAM_5:
        return LL_DMA_IsActiveFlag_TC5(dma_ctrl);

    case DMA_STREAM_6:
        return LL_DMA_IsActiveFlag_TC6(dma_ctrl);

    case DMA_STREAM_7:
        return LL_DMA_IsActiveFlag_TC7(dma_ctrl);

    default:
        break;
    }

    return 0;
}

static inline int dma_is_active_flag_ht(DMA_TypeDef *dma_ctrl, enum dma_stream stream)
{
    switch (stream) {

    case DMA_STREAM_0:
        return LL_DMA_IsActiveFlag_HT0(dma_ctrl);

    case DMA_STREAM_1:
        return LL_DMA_IsActiveFlag_HT1(dma_ctrl);

    case DMA_STREAM_2:
        return LL_DMA_IsActiveFlag_HT2(dma_ctrl);

    case DMA_STREAM_3:
        return LL_DMA_IsActiveFlag_HT3(dma_ctrl);

    case DMA_STREAM_4:
        return LL_DMA_IsActiveFlag_HT4(dma_ctrl);

    case DMA_STREAM_5:
        return LL_DMA_IsActiveFlag_HT5(dma_ctrl);

    case DMA_STREAM_6:
        return LL_DMA_IsActiveFlag_HT6(dma_ctrl);

    case DMA_STREAM_7:
        return LL_DMA_IsActiveFlag_HT7(dma_ctrl);

    default:
        break;
    }

    return 0;
}

static inline struct dma_setup * get_dma_setup(dma_handle_t dma_handle)
{
    enum dma_controller dma_ctrl = DMA_HANDLE_GET_CTRL_NR(dma_handle);
    enum dma_stream stream = DMA_HANLDE_GET_STREAM_NR(dma_handle);

    if (dma_ctrl > DMA_CONTROLLER_LAST || stream > DMA_STREAM_LAST)
        return NULL;

    switch (dma_ctrl) {

    case DMA_CONTROLLER_1:
        return &dma1[stream];

    case DMA_CONTROLLER_2:
        return &dma2[stream];

    default:
        break;
    }

    return NULL;
}


int dma2periph_setup(dma_handle_t dma_handle, struct dma2periph_config *user_setup) {

    DMA_TypeDef *dma_ctrl = handle_get_controller(dma_handle);
    uint32_t dma_stream = handle_get_stream(dma_handle);
    IRQn_Type irq_nr = get_irq_number(
                DMA_HANDLE_GET_CTRL_NR(dma_handle),
                DMA_HANLDE_GET_STREAM_NR(dma_handle));

    struct dma_setup *internal_setup = get_dma_setup(dma_handle);


    (DMA_HANDLE_GET_CTRL_NR(dma_handle) == DMA_CONTROLLER_1) ?
                LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1) :
                LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA2);

    /* TODO: try to rewrite below low-level driver API to HAL API */
    LL_DMA_SetChannelSelection(dma_ctrl, dma_stream, get_channel(user_setup->channel));
    LL_DMA_SetDataTransferDirection(dma_ctrl, dma_stream, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
    LL_DMA_SetStreamPriorityLevel(dma_ctrl, dma_stream, LL_DMA_PRIORITY_LOW);
    LL_DMA_SetPeriphIncMode(dma_ctrl, dma_stream, get_pdata_increment(user_setup->periph_inc));
    LL_DMA_SetMemoryIncMode(dma_ctrl, dma_stream, get_mdata_increment(user_setup->memory_inc));
    LL_DMA_SetPeriphSize(dma_ctrl, dma_stream, get_pdata_align(user_setup->periph_align));
    LL_DMA_SetMemorySize(dma_ctrl, dma_stream, get_mdata_align(user_setup->memory_align));
    LL_DMA_DisableFifoMode(dma_ctrl, dma_stream);

    LL_DMA_SetPeriphAddress(dma_ctrl, dma_stream, user_setup->periph_addr);

    internal_setup->cb_priv = user_setup->cb_priv;
    internal_setup->dma_cb = user_setup->dma_cb;

    NVIC_SetPriority(irq_nr, user_setup->dma_irq_prio);
    NVIC_EnableIRQ(irq_nr);

    internal_setup->mode = DMA2PERIPH;
    internal_setup->status = READY;

    return 0;
}

void dma2periph_start(dma_handle_t dma_handle,
                      uint32_t mem_addr,
                      size_t size,
                      int ht_tc_flags,
                      enum dma_run_mode mode) {

    DMA_TypeDef *dma_ctrl = handle_get_controller(dma_handle);
    uint32_t dma_stream = handle_get_stream(dma_handle);

    LL_DMA_SetMode(dma_ctrl, dma_stream, (mode == DMA_MODE_NORMAL) ?
                       LL_DMA_MODE_NORMAL :
                       LL_DMA_MODE_CIRCULAR);
    LL_DMA_SetMemoryAddress(dma_ctrl, dma_stream, mem_addr);
    LL_DMA_SetDataLength(dma_ctrl, dma_stream, size);

    /* Clear DMA flags */
    dma_clear_flag_tc(dma_ctrl, dma_stream);
    dma_clear_flag_ht(dma_ctrl, dma_stream);

    (ht_tc_flags & DMA_HT_IRQ_ENABLED) ?
                LL_DMA_EnableIT_HT(dma_ctrl, dma_stream) :
                LL_DMA_DisableIT_HT(dma_ctrl, dma_stream);


    (ht_tc_flags & DMA_TC_IRQ_ENABLED) ?
                LL_DMA_EnableIT_TC(dma_ctrl, dma_stream) :
                LL_DMA_DisableIT_TC(dma_ctrl, dma_stream);


    LL_DMA_EnableStream(dma_ctrl, dma_stream);
}

void dma2periph_stop(dma_handle_t dma_handle) {

    DMA_TypeDef *dma_ctrl = handle_get_controller(dma_handle);
    uint32_t dma_stream = handle_get_stream(dma_handle);

    /* Disable timer output and disable DMA stream */
    LL_DMA_DisableStream(dma_ctrl, dma_stream);
}

/* irq handlers for DMA1, one per stream nr */
DMA_IRQ_HANDLER(1, 0)
DMA_IRQ_HANDLER(1, 1)
DMA_IRQ_HANDLER(1, 2)
DMA_IRQ_HANDLER(1, 3)
DMA_IRQ_HANDLER(1, 4)
DMA_IRQ_HANDLER(1, 5)
DMA_IRQ_HANDLER(1, 6)
DMA_IRQ_HANDLER(1, 7)

/* irq handlers for DMA2, one per stream nr */
DMA_IRQ_HANDLER(2, 0)
DMA_IRQ_HANDLER(2, 1)
DMA_IRQ_HANDLER(2, 2)
DMA_IRQ_HANDLER(2, 3)
DMA_IRQ_HANDLER(2, 4)
DMA_IRQ_HANDLER(2, 5)
DMA_IRQ_HANDLER(2, 6)
DMA_IRQ_HANDLER(2, 7)


