/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __I2C_COMMON_SOC_H__
#define __I2C_COMMON_SOC_H__

#include "stm32f4xx_hal_conf.h"

#include "i2c_soc.h"
#include "i2c_bsp.h"

typedef struct i2c_handle_s
{
	I2C_HandleTypeDef i2c_hal_handle;
	i2c_op_clbk_t clbk;
	void *user;
} i2c_handle_t;

int i2c_common_periph_init(i2c_bus_t bus, uint32_t addr_mode, uint32_t clock_speed, uint32_t own_address);
void i2c_common_periph_deinit(i2c_bus_t bus);
i2c_handle_t * i2c_common_get_handle(i2c_bus_t bus);

#endif /* __I2C_COMMON_SOC_H__ */