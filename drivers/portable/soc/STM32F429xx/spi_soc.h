/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __SPI_SOC_H__
#define __SPI_SOC_H__

#include "stm32f4xx_hal_conf.h"

#include "spi_bsp.h"

#define SPI_ERROR_NONE	HAL_SPI_ERROR_NONE
#define SPI_ERROR_MODF	HAL_SPI_ERROR_MODF
#define SPI_ERROR_CRC	HAL_SPI_ERROR_CRC
#define SPI_ERROR_OVR	HAL_SPI_ERROR_OVR
#define SPI_ERROR_FRE	HAL_SPI_ERROR_FRE
#define SPI_ERROR_DMA	HAL_SPI_ERROR_DMA
#define SPI_ERROR_FLAG	HAL_SPI_ERROR_FLAG

typedef void (*spi_op_clbk_t)(int result, void *data);

int spi_soc_init(spi_bus_t bus, uint32_t clock_speed);
void spi_soc_deinit(spi_bus_t bus);
int spi_soc_send(spi_bus_t bus, uint8_t *data, size_t length, spi_op_clbk_t clbk, void *user);
int spi_soc_recv(spi_bus_t bus, uint8_t *data, size_t length, spi_op_clbk_t clbk, void *user);
int spi_soc_send_recv(spi_bus_t bus, uint8_t *send, uint8_t *recv, spi_op_clbk_t clbk, size_t length, void *user);

#endif /* __SPI_SOC_H__ */