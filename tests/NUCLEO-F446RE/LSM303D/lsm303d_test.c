/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "acc_mag_LSM303D.h"
#include "usart.h"
#include "i2c.h"

static portTASK_FUNCTION_PROTO( vLsm303dTestTask, pvParameters );
static TaskHandle_t xLsm303dTestTask;

static usart_handle_t usart_handle;

BaseType_t lsm303d_test_init( void )
{
    BaseType_t ret = 0;

    usart_handle = usart_init(USART_PORT_2, 115200, USART_WORLD_LENGTH_8, UART_STOPBITS_1, UART_PARITY_NONE, UART_HWCONTROL_NONE);
    configASSERT(usart_handle != NULL);

    i2c_master_init(I2C_BUS_1, I2C_ADDRESSING_MODE_7BIT, 400000);

    ret = xTaskCreate( vLsm303dTestTask, "LSM303D test", configMINIMAL_STACK_SIZE + 512, NULL, tskIDLE_PRIORITY + 1, &xLsm303dTestTask );
    if( ret != pdPASS )
    {
        return pdFAIL;
    }

    return pdPASS;
}

static portTASK_FUNCTION( vLsm303dTestTask, pvParameters )
{
    acc_mag_config_t config = {I2C_BUS_1, 1, LA_FS_2G, ACC_DATA_RATE_25Hz, MAG_DATA_RATE_25Hz, MAG_RES_HIGH, MAG_FS_2G};
    float XYZ[3];
    char buf[100];
    int delay = 100;
    int ret, size;

    ret = lsm303d_acc_mag_init(&config);
    if (ret) {
        vTaskDelete(NULL);
    }

    lsm303d_reset_mag();

    for(;;)
    {
        lsm303d_read_acc_val(XYZ);
        size = sprintf(buf, "ACC: %.04f; %.04f; %.04f\n\r", XYZ[0], XYZ[1], XYZ[2]);
        usart_send(usart_handle, (uint8_t *)buf, size, -1);

        lsm303d_read_mag_val(XYZ);
        size = sprintf(buf, "MAG: %.04f; %.04f; %.04f\n\r", XYZ[0], XYZ[1], XYZ[2]);
        usart_send(usart_handle, (uint8_t *)buf, size, -1);

        vTaskDelay(delay / portTICK_PERIOD_MS);
    }
}
