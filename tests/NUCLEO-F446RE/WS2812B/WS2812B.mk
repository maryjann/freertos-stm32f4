WS2812B_TEST_DIR = $(PWD)
WS2812B_TEST_OBJ = $(WS2812B_TEST_DIR)/main.o \
				   $(WS2812B_TEST_DIR)/ws2812b_test.o

CFLAGS += -I$(WS2812B_TEST_DIR)

$(WS2812B_TEST_DIR)/ws2812b_test.elf: $(TARGET_OBJ) $(WS2812B_TEST_OBJ)
	$(CC) -mthumb -mcpu=$(CPU) -mfloat-abi=hard -mfpu=fpv4-sp-d16 -T$(ROOT_DIR)/$(LNK_SCRIPT) -Wl,--gc-sections -o $@ $^
	$(OBJCOPY) -Oihex $(WS2812B_TEST_DIR)/ws2812b_test.elf $(WS2812B_TEST_DIR)/ws2812b_test.hex

test/WS2812B: $(WS2812B_TEST_DIR)/ws2812b_test.elf

test/WS2812B_clean:
	rm -rf $(WS2812B_TEST_OBJ)
	rm -rf $(WS2812B_TEST_DIR)/ws2812b_test.elf $(WS2812B_TEST_DIR)/ws2812b_test.hex
	rm -rf soc
	rm -rf bsp
	rm -rf hal