/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "ws2812b.h"
#include "ws2812b_test.h"

static portTASK_FUNCTION_PROTO( vLedTask, pvParameters );
static TaskHandle_t xLedTask;

int ws2812b_test_init( void ) {
	BaseType_t ret = 0;

	ret = pwm_leds_init();
	configASSERT(ret == 0);

	ret = xTaskCreate( vLedTask, "WS2812B", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xLedTask );
	configASSERT(ret == pdPASS);

    return pdPASS;
}

static portTASK_FUNCTION( vLedTask, pvParameters )
{
    for( ;; )
    {
        pwm_leds_set_color_all(0xFF000000);
		pwm_leds_update(1);
        vTaskDelay( 500 / portTICK_PERIOD_MS );
        pwm_leds_set_color_all(0x00FF0000);
		pwm_leds_update(1);
        vTaskDelay( 500 / portTICK_PERIOD_MS );
        pwm_leds_set_color_all(0x0000FF00);
        pwm_leds_update(1);
		vTaskDelay( 500 / portTICK_PERIOD_MS );
    }
}
