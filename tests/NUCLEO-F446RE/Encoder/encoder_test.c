/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"

#include "leds.h"
#include "encoder.h"

static portTASK_FUNCTION_PROTO( vEncoderTask, pvParameters );

static TaskHandle_t xEncoderTask;

int encoder_test_init( void )
{
    BaseType_t ret = 0;

    leds_init( LEDS_GREEN );
    ret = encoder_init(ENCODER_TIMER_2, ENCODER_TYPE_DUAL);
    configASSERT(ret == 0);

    ret = encoder_init(ENCODER_TIMER_3, ENCODER_TYPE_DUAL);
    configASSERT(ret == 0);

    ret = xTaskCreate( vEncoderTask, "Encoder test ENC", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xEncoderTask );
    if( ret != pdPASS )
    {
        /*TODO - add error logging*/
        leds_deinit( LEDS_GREEN );
        return pdFAIL;
    }

    return pdPASS;
}

static portTASK_FUNCTION( vEncoderTask, pvParameters )
{
    int32_t val_1, val_2;
    for( ;; )
    {
        val_1 = encoder_get_value(ENCODER_TIMER_2);
        val_2 = encoder_get_value(ENCODER_TIMER_3);
        
        if(abs(val_1 - val_2) < 100)
            leds_set(LEDS_GREEN, 1);
        else
            leds_set(LEDS_GREEN, 0);

        vTaskDelay( 100 / portTICK_PERIOD_MS );
    }
}
