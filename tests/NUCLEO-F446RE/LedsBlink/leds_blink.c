/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "leds.h"

static portTASK_FUNCTION_PROTO( vLedRedTestTask, pvParameters );
static portTASK_FUNCTION_PROTO( vLedBlueTestTask, pvParameters );
static portTASK_FUNCTION_PROTO( vLedGreenTestTask, pvParameters );

static TaskHandle_t xLedRedTask;
static TaskHandle_t xLedBlueTask;
static TaskHandle_t xLedGreenTask;

BaseType_t xLedsBlinkStart( void )
{
    BaseType_t ret = 0;

    leds_init( LEDS_GREEN );
    ret = xTaskCreate( vLedRedTestTask, "LED_RED_Test", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xLedRedTask );
    if( ret != pdPASS )
    {
        /*TODO - add error logging*/
        leds_deinit( LEDS_GREEN );
        return pdFAIL;
    }

    return pdPASS;
}

void vLedsBlinkStop( void )
{
    vTaskDelete( xLedRedTask );
    
    leds_deinit( LEDS_GREEN );
}

static portTASK_FUNCTION( vLedRedTestTask, pvParameters )
{
    for( ;; )
    {
        leds_toggle( LEDS_GREEN );
        vTaskDelay( 500 / portTICK_PERIOD_MS );
    }
}
