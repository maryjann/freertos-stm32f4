/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "i2c.h"

static portTASK_FUNCTION_PROTO( vLedI2c1Task, pvParameters );
static TaskHandle_t xLedI2c1Task;

BaseType_t xLedsBlinkStart( void )
{
    BaseType_t ret = 0;

    i2c_master_init(I2C_BUS_1, I2C_ADDRESSING_MODE_7BIT, 400000);

    ret = xTaskCreate( vLedI2c1Task, "LED I2C1 Test", configMINIMAL_STACK_SIZE + 512, NULL, tskIDLE_PRIORITY + 1, &xLedI2c1Task );
    if( ret != pdPASS )
    {
        return pdFAIL;
    }

    return pdPASS;
}

void vLedsBlinkStop( void )
{
    vTaskDelete( xLedI2c1Task );
}

static portTASK_FUNCTION( vLedI2c1Task, pvParameters )
{
    uint8_t data_1 = 0xF0;
   
    i2c_master_send(I2C_BUS_1, 0x76, &data_1, sizeof(uint8_t));

    for( ;; )
    {
        i2c_master_recv(I2C_BUS_1, 0x76, &data_1, sizeof(uint8_t));
        data_1 = (data_1 + 1) & 0x0F;
        i2c_master_send(I2C_BUS_1, 0x76, &data_1, sizeof(uint8_t));

        vTaskDelay( 125 / portTICK_PERIOD_MS );
    }
}
