/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "usart.h"

#include "stlink_uart_test.h"

static portTASK_FUNCTION_PROTO( vSTlinkUartTestTask, pvParameters );
static TaskHandle_t xSTlinkUartTestTask;

static usart_handle_t usart_handle;

int stlink_uart_test(void) {
	int ret = 0;
	
	usart_handle = usart_init(USART_PORT_2, 115200, USART_WORLD_LENGTH_8, UART_STOPBITS_1, UART_PARITY_NONE, UART_HWCONTROL_NONE);
	configASSERT(usart_handle != NULL);

	ret = xTaskCreate(vSTlinkUartTestTask, "Encoder measure task", 3 * configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xSTlinkUartTestTask);
	configASSERT(ret == pdPASS);

	return 0;
}

static portTASK_FUNCTION( vSTlinkUartTestTask, pvParameters ) {
	int ret = 0;
	uint8_t in[1];
	uint8_t out[1];

	while(1) {
		ret = usart_recv(usart_handle, in, sizeof(in), 500);
		if (ret == 0) {
			memcpy(out, in, sizeof(in));
			usart_send(usart_handle, out, sizeof(out), -1);
		}
	}
}
