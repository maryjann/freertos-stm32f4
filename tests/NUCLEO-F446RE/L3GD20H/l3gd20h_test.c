/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "l3gd20h.h"
#include "usart.h"
#include "i2c.h"

static portTASK_FUNCTION_PROTO( vL3gd20hTestTask, pvParameters );
static TaskHandle_t xL3gd20hTestTask;

static usart_handle_t usart_handle;

BaseType_t l3gd20h_test_init( void )
{
    BaseType_t ret = 0;

    usart_handle = usart_init(USART_PORT_2, 115200, USART_WORLD_LENGTH_8, UART_STOPBITS_1, UART_PARITY_NONE, UART_HWCONTROL_NONE);
    configASSERT(usart_handle != NULL);

    i2c_master_init(I2C_BUS_1, I2C_ADDRESSING_MODE_7BIT, 400000);

    ret = xTaskCreate( vL3gd20hTestTask, "L3GD20H test", configMINIMAL_STACK_SIZE + 512, NULL, tskIDLE_PRIORITY + 1, &xL3gd20hTestTask );
    if( ret != pdPASS )
    {
        return pdFAIL;
    }

    return pdPASS;
}

static portTASK_FUNCTION( vL3gd20hTestTask, pvParameters )
{
    l3gd20h_float_data_t data;
    char buf[100];
    int delay = 100, size;
    bool ret;

    l3gd20h_sensor_t *l3gd20h = l3gd20h_init_sensor(I2C_BUS_1, L3GD20H_I2C_ADDRESS_2);
    configASSERT(l3gd20h != NULL);

    ret = l3gd20h_select_output_filter (l3gd20h, l3gd20h_hpf_only);
    configASSERT(ret == true);

    ret = l3gd20h_config_hpf (l3gd20h, l3gd20h_hpf_normal, 0);
    configASSERT(ret == true);
    
    ret = l3gd20h_set_mode (l3gd20h, l3gd20h_normal_odr_12_5, 3, true, true, true);
    configASSERT(ret == true);

    for(;;)
    {
        vTaskDelay(delay / portTICK_PERIOD_MS);
        ret = l3gd20h_new_data(l3gd20h);
        if (ret == true)
        {
            l3gd20h_get_float_data(l3gd20h, &data);
            size = sprintf(buf, "GYRO: %.04f\t%.04f\t%.04f\n\r", data.x, data.y, data.z);
            usart_send(usart_handle, (uint8_t *)buf, size, -1);
            continue;
        }
    }
}
