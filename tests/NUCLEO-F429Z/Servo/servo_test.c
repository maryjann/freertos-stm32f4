/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "leds.h"
#include "encoder.h"
#include "servo.h"

#define SERVO_MIN 0
#define SERVO_MAX 100

static portTASK_FUNCTION_PROTO( vEncoderTask, pvParameters );

static TaskHandle_t xEncoderTask;

int servo_test_init( void )
{
    BaseType_t ret = 0;

    leds_init( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
    ret = encoder_init(ENCODER_TIMER_1, ENCODER_TYPE_DUAL);
    configASSERT(ret == 0);

    ret = servo_init(PWM_TIMER_10);
    configASSERT(ret == 0);

    ret = xTaskCreate( vEncoderTask, "Servo test ENC", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xEncoderTask );
    if( ret != pdPASS )
    {
        leds_deinit( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
        return pdFAIL;
    }

    return pdPASS;
}

static portTASK_FUNCTION( vEncoderTask, pvParameters )
{
    encoder_dir_t dir;
    static uint32_t enc_val_old = 0;

    for( ;; )
    {
        dir = encoder_get_direction(ENCODER_TIMER_1);
        if (dir == ENCODER_DIR_UP) {
            leds_set(LEDS_GREEN, 0);
            leds_set(LEDS_BLUE, 1);
        } else if (dir == ENCODER_DIR_DOWN) {
            leds_set(LEDS_GREEN, 1);
            leds_set(LEDS_BLUE, 0);
        }
        
        uint32_t enc_val = encoder_get_value(ENCODER_TIMER_1);
        if(enc_val_old != enc_val) {
            leds_toggle(LEDS_RED);
            enc_val_old = enc_val;

            if (enc_val < SERVO_MIN)
                enc_val = SERVO_MIN;
            else if (enc_val > SERVO_MAX)
                enc_val = SERVO_MAX;

            servo_set_position(PWM_TIMER_10, enc_val);
        }

        vTaskDelay( 50 / portTICK_PERIOD_MS );
    }
}
