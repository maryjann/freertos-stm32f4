SERVO_TEST_DIR = $(PWD)
SERVO_TEST_OBJ = $(SERVO_TEST_DIR)/main.o \
				   $(SERVO_TEST_DIR)/servo_test.o

CFLAGS += -I$(SERVO_TEST_DIR)

$(SERVO_TEST_DIR)/servo_test.elf: $(TARGET_OBJ) $(SERVO_TEST_OBJ)
	$(CC) -mthumb -mcpu=$(CPU) -mfloat-abi=hard -mfpu=fpv4-sp-d16 -T$(ROOT_DIR)/$(LNK_SCRIPT) -Wl,--gc-sections -o $@ $^
	$(OBJCOPY) -Oihex $(SERVO_TEST_DIR)/servo_test.elf $(SERVO_TEST_DIR)/servo_test.hex

test/Servo: $(SERVO_TEST_DIR)/servo_test.elf

test/Servo_clean:
	rm -rf $(SERVO_TEST_OBJ)
	rm -rf $(SERVO_TEST_DIR)/servo_test.elf $(SERVO_TEST_DIR)/servo_test.hex
	rm -rf soc
	rm -rf bsp
	rm -rf hal