/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __BSP_CFG_H__
#define __BSP_CFG_H__

//#define ENCODER_TIMER_2_CH1_CN10_29
//#define ENCODER_TIMER_2_CH2_CN7_15

//#define ENCODER_TIMER_3_CH1_CN7_12
//#define ENCODER_TIMER_3_CH1_CN7_1
//#define ENCODER_TIMER_3_CH1_CN7_19

//#define ENCODER_TIMER_3_CH2_CN9_15
//#define ENCODER_TIMER_3_CH2_CN7_11
//#define ENCODER_TIMER_3_CH2_CN7_13

//#define ENCODER_TIMER_4_CH1_CN10_21
//#define ENCODER_TIMER_4_CH2_CN10_2

//#define ENCODER_TIMER_5_CH1_CN10_29

#define ENCODER_TIMER_1_CH1_CN10_4
#define ENCODER_TIMER_1_CH2_CN10_6

#endif /* __BSP_CFG_H__ */