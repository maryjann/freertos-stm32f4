/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"

#include "gpio.h"
#include "leds.h"
#include "encoder.h"
#include "mc33926.h"

#define MC33926_MIN 0
#define MC33926_MAX 100

static portTASK_FUNCTION_PROTO( vEncoderTask, pvParameters );

static TaskHandle_t xEncoderTask;

static const mc33926_init_t mc33926_conf = {
    .en_pin = CN7_1,
    .dir_pin = CN7_3,
    .d2_pin = CN7_5,
    .pwm = PWM_TIMER_10
};

static mc33926_handle_t mc33926_handle; 

int mc33926_test_init( void )
{
    BaseType_t ret = 0;

    leds_init( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
    ret = encoder_init(ENCODER_TIMER_1, ENCODER_TYPE_DUAL);
    configASSERT(ret == 0);

    ret = xTaskCreate( vEncoderTask, "MC33926 test ENC", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xEncoderTask );
    if( ret != pdPASS )
    {
        leds_deinit( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
        return pdFAIL;
    }

    mc33926_handle = mc33926_init(&mc33926_conf);
    configASSERT(mc33926_handle);

    mc33926_set_power(mc33926_handle, ENABLED);

    return pdPASS;
}

static portTASK_FUNCTION( vEncoderTask, pvParameters )
{
    mc33926_set_output(mc33926_handle, ENABLED);

    for( ;; )
    {
        encoder_dir_t dir = encoder_get_direction(ENCODER_TIMER_1);
        if (dir == ENCODER_DIR_UP) {
            leds_set(LEDS_GREEN, 0);
            leds_set(LEDS_BLUE, 1);
        } else if (dir == ENCODER_DIR_DOWN) {
            leds_set(LEDS_GREEN, 1);
            leds_set(LEDS_BLUE, 0);
        }
        
        int16_t enc_val = (int16_t)encoder_get_value(ENCODER_TIMER_1);
        if(enc_val < 0) {
            mc33926_set_direction(mc33926_handle, INVERTED);
        } else {
            mc33926_set_direction(mc33926_handle, NON_INVERTED);
        }

        if (abs(enc_val) > MC33926_MAX)
            enc_val = MC33926_MAX;

        mc33926_set_speed(mc33926_handle, abs(enc_val));

        vTaskDelay( 50 / portTICK_PERIOD_MS );
    }
}
