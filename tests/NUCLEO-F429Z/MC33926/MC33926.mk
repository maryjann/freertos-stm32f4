MC33926_TEST_DIR = $(PWD)
MC33926_TEST_OBJ = $(MC33926_TEST_DIR)/main.o \
				   $(MC33926_TEST_DIR)/mc33926_test.o

CFLAGS += -I$(MC33926_TEST_DIR)

$(MC33926_TEST_DIR)/mc33926_test.elf: $(TARGET_OBJ) $(MC33926_TEST_OBJ)
	$(CC) -mthumb -mcpu=$(CPU) -mfloat-abi=hard -mfpu=fpv4-sp-d16 -T$(ROOT_DIR)/$(LNK_SCRIPT) -Wl,--gc-sections -o $@ $^
	$(OBJCOPY) -Oihex $(MC33926_TEST_DIR)/mc33926_test.elf $(MC33926_TEST_DIR)/mc33926_test.hex

test/MC33926: $(MC33926_TEST_DIR)/mc33926_test.elf

test/MC33926_clean:
	rm -rf $(MC33926_TEST_OBJ)
	rm -rf $(MC33926_TEST_DIR)/mc33926_test.elf $(MC33926_TEST_DIR)/mc33926_test.hex
	rm -rf soc
	rm -rf bsp
	rm -rf hal