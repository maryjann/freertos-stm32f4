/*
 * Copyright (c) 2019 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "leds.h"
#include "spi.h"

static portTASK_FUNCTION_PROTO( vSpiTestTask, pvParameters );

static TaskHandle_t xSpiTestTask;

BaseType_t spi_test_init( void )
{
    BaseType_t ret = 0;

    leds_init( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
    ret = xTaskCreate( vSpiTestTask, "SPI Test", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xSpiTestTask );
    if( ret != pdPASS )
    {
        /*TODO - add error logging*/
        leds_deinit( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
        return pdFAIL;
    }

    ret = spi_init (SPI_BUS_1, 100000);
    if (ret != 0)
    {
        leds_deinit( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
        vTaskDelete( xSpiTestTask );

        return pdFAIL;
    } 

    return pdPASS;
}

void spi_test_deinit( void )
{
    vTaskDelete( xSpiTestTask );
    leds_deinit( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
}

static portTASK_FUNCTION( vSpiTestTask, pvParameters )
{
    uint8_t tx = 0;
    uint8_t rx;

    for( ;; )
    {
        spi_send_recv(SPI_BUS_1, &tx, &rx, sizeof(uint8_t));
        if(tx == rx) {
            leds_set(LEDS_GREEN, 1);
            leds_set(LEDS_RED, 0);
        }
        else {
            leds_set(LEDS_GREEN, 0);
            leds_set(LEDS_RED, 1);
        }
        tx++;
        leds_toggle( LEDS_BLUE );
        vTaskDelay( 250 / portTICK_PERIOD_MS );
    }
}
