SPI_TEST_DIR = $(PWD)
SPI_TEST_OBJ = $(SPI_TEST_DIR)/main.o \
			   $(SPI_TEST_DIR)/spi_test.o

CFLAGS += -I$(SPI_TEST_DIR)

$(SPI_TEST_DIR)/spi_test.elf: $(TARGET_OBJ) $(SPI_TEST_OBJ)
	$(CC) -mthumb -mcpu=$(CPU) -mfloat-abi=hard -mfpu=fpv4-sp-d16 -T$(ROOT_DIR)/$(LNK_SCRIPT) -Wl,--gc-sections -o $@ $^
	$(OBJCOPY) -Oihex $(SPI_TEST_DIR)/spi_test.elf $(SPI_TEST_DIR)/spi_test.hex

test/SPI: $(SPI_TEST_DIR)/spi_test.elf

test/SPI_clean:
	rm -rf $(SPI_TEST_OBJ)
	rm -rf $(SPI_TEST_DIR)/spi_test.elf $(SPI_TEST_DIR)/spi_test.hex
	rm -rf soc
	rm -rf bsp
	rm -rf hal