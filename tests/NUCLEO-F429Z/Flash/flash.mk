FLASH_TEST_DIR = $(PWD)
FLASH_TEST_OBJ = $(FLASH_TEST_DIR)/main.o \
				 $(FLASH_TEST_DIR)/flash_test.o

CFLAGS += -I$(FLASH_TEST_DIR)

$(FLASH_TEST_DIR)/flash_test.elf: $(TARGET_OBJ) $(FLASH_TEST_OBJ)
	$(CC) -mthumb -mcpu=$(CPU) -mfloat-abi=hard -mfpu=fpv4-sp-d16 -T$(LNK_SCRIPT) -Wl,--gc-sections -o $@ $^
	$(OBJCOPY) -Oihex $(FLASH_TEST_DIR)/flash_test.elf $(FLASH_TEST_DIR)/flash_test.hex

test/Flash: $(FLASH_TEST_DIR)/flash_test.elf

test/Flash_clean:
	rm -rf $(FLASH_TEST_OBJ)
	rm -rf $(FLASH_TEST_DIR)/flash_test.elf $(FLASH_TEST_DIR)/flash_test.hex
	rm -rf soc
	rm -rf bsp
	rm -rf hal