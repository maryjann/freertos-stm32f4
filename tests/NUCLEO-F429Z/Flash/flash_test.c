/*
 * Copyright (c) 2020 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "leds.h"
#include "flash.h"

#include "flash_test.h"

static portTASK_FUNCTION_PROTO( vFlashTestTask, pvParameters );
static TaskHandle_t xFlashTestTask;

BaseType_t xFlashTestStart(void)
{
    BaseType_t ret = 0;

    leds_init( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
    flash_init();
    
    ret = xTaskCreate( vFlashTestTask, "Flash Test", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xFlashTestTask );
    if( ret != pdPASS )
    {
        /*TODO - add error logging*/
        leds_deinit( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
        return pdFAIL;
    }
    
    return pdPASS;
}

static portTASK_FUNCTION( vFlashTestTask, pvParameters )
{

    uint32_t data = 0;
    size_t flash_begin = flash_get_begin();
    size_t flash_end = flash_get_end();

    //Erase available flash memory
    leds_toggle( LEDS_RED );
    flash_erase(flash_begin, flash_end - flash_begin);
    leds_toggle( LEDS_BLUE );
    for (size_t idx = 0; idx < (flash_end - flash_begin)/4; idx++)
    {
        flash_read(flash_begin + idx, &data, sizeof(data));
        configASSERT(data == 0xffffffff);
    }  

    data = 0xaaaaaaaa;
    flash_write(flash_begin, &data, sizeof(data));
    data = 0xbbbbbbbb;
    flash_write(flash_begin + 4, &data, sizeof(data));
    data = 0xcccccccc;
    flash_write(flash_begin + 16*1024, &data, sizeof(data));
    data = 0xdddddddd;
    flash_write(flash_begin + 32*1024, &data, sizeof(data));
    data = 0xeeeeeeee;
    flash_write(flash_begin + 32*1024 + 4, &data, sizeof(data));
    data = 0x55555555;
    flash_write(flash_end - 4, &data, sizeof(data));
    data = 0x5a5a5a5a;
    flash_write(flash_end - 8, &data, sizeof(data));

    flash_read(flash_begin, &data, sizeof(data));
    configASSERT(data == 0xaaaaaaaa);
    
    flash_read(flash_begin + 4, &data, sizeof(data));
    configASSERT(data == 0xbbbbbbbb);

    flash_read(flash_begin + 16 * 1024, &data, sizeof(data));
    configASSERT(data == 0xcccccccc);

    flash_read(flash_begin + 32 * 1024, &data, sizeof(data));
    configASSERT(data == 0xdddddddd);

    flash_read(flash_begin + 32 * 1024 + 4, &data, sizeof(data));
    configASSERT(data == 0xeeeeeeee);

    flash_read(flash_end - 4, &data, sizeof(data));
    configASSERT(data == 0x55555555);

    flash_read(flash_end - 8, &data, sizeof(data));
    configASSERT(data == 0x5a5a5a5a);

    flash_erase(flash_begin + 4, 32 * 1024);
    flash_erase(flash_end - 8, sizeof(data));
    leds_toggle( LEDS_GREEN );

    flash_read(flash_begin, &data, sizeof(data));
    configASSERT(data == 0xaaaaaaaa);
    
    flash_read(flash_begin + 4, &data, sizeof(data));
    configASSERT(data == 0xffffffff);

    flash_read(flash_begin + 16 * 1024, &data, sizeof(data));
    configASSERT(data == 0xffffffff);

    flash_read(flash_begin + 32 * 1024, &data, sizeof(data));
    configASSERT(data == 0xffffffff);

    flash_read(flash_begin + 32 * 1024 + 4, &data, sizeof(data));
    configASSERT(data == 0xeeeeeeee);

    flash_read(flash_end - 8, &data, sizeof(data));
    configASSERT(data == 0xffffffff);

    flash_read(flash_end - 4, &data, sizeof(data));
    configASSERT(data == 0x55555555);

    for( ;; )
    {
        leds_toggle( LEDS_RED );
        leds_toggle( LEDS_BLUE );
        leds_toggle( LEDS_GREEN );
        vTaskDelay( 250 / portTICK_PERIOD_MS );
    }
}
