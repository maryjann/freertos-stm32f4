/*
 * Copyright (c) 2020 Jakub Nowacki
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "FreeRTOS_IP.h"
#include "stm32f4xx_hal_conf.h"

#include "FreeRTOS_Sockets.h"
#include "NetworkInterface.h"

#include "leds.h"

static portTASK_FUNCTION_PROTO( vLedRedTestTask, pvParameters );
static portTASK_FUNCTION_PROTO( vLedBlueTestTask, pvParameters );
static portTASK_FUNCTION_PROTO( vLedGreenTestTask, pvParameters );

static TaskHandle_t xLedRedTask;
static TaskHandle_t xLedBlueTask;
static TaskHandle_t xLedGreenTask;

static const uint8_t ucIPAddress[ 4 ] = { 10, 0, 0, 42 };
static const uint8_t ucNetMask[ 4 ] = { 255, 255, 255, 0 };
static const uint8_t ucGatewayAddress[ 4 ] = { 10, 0, 0, 1 };
static const uint8_t ucDNSServerAddress[ 4 ] = { 10, 0, 0, 1 };
const uint8_t ucMACAddress[ 6 ] = { MAC_ADDR0, MAC_ADDR1, MAC_ADDR2, MAC_ADDR3, MAC_ADDR4, MAC_ADDR5 };

BaseType_t eth_test_init( void )
{
    BaseType_t ret = 0;

    leds_init( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
    ret = xTaskCreate( vLedRedTestTask, "LED_RED_Test", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xLedRedTask );
    if( ret != pdPASS )
    {
        /*TODO - add error logging*/
        leds_deinit( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
        return pdFAIL;
    }

    ret = xTaskCreate( vLedBlueTestTask, "LED_BLUE_Test", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xLedBlueTask );
    if( ret != pdPASS )
    {
        /*TODO - add error logging*/
        vTaskDelete( xLedRedTask );
        leds_deinit( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
        return pdFAIL;
    }

    ret = xTaskCreate( vLedGreenTestTask, "LED_GREEN_Test", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, &xLedGreenTask );
    if( ret != pdPASS )
    {
        /*TODO - add error logging*/
        vTaskDelete( xLedRedTask );
        vTaskDelete( xLedBlueTask );
        leds_deinit( LEDS_RED | LEDS_BLUE | LEDS_GREEN );
        return pdFAIL;
    }

    FreeRTOS_IPInit( ucIPAddress, ucNetMask, ucGatewayAddress, ucDNSServerAddress, ucMACAddress );

    return pdPASS;
}

static portTASK_FUNCTION( vLedRedTestTask, pvParameters )
{
    for( ;; )
    {
        leds_toggle( LEDS_RED );
        vTaskDelay( 250 / portTICK_PERIOD_MS );
    }
}

static portTASK_FUNCTION( vLedBlueTestTask, pvParameters )
{
    struct freertos_sockaddr xClient, xBindAddress;
    Socket_t xListeningSocket, xConnectedSocket;
    socklen_t xSize = sizeof( xClient );
    static const TickType_t xReceiveTimeOut = portMAX_DELAY;
    const BaseType_t xBacklog = 20;

    /* Attempt to open the socket. */
    xListeningSocket = FreeRTOS_socket( FREERTOS_AF_INET,
                                        FREERTOS_SOCK_STREAM,  /* SOCK_STREAM for TCP. */
                                        FREERTOS_IPPROTO_TCP );

    /* Check the socket was created. */
    configASSERT( xListeningSocket != FREERTOS_INVALID_SOCKET );

    /* If FREERTOS_SO_RCVBUF or FREERTOS_SO_SNDBUF are to be used with
    FreeRTOS_setsockopt() to change the buffer sizes from their default then do
    it here!.  (see the FreeRTOS_setsockopt() documentation. */

    /* If ipconfigUSE_TCP_WIN is set to 1 and FREERTOS_SO_WIN_PROPERTIES is to
    be used with FreeRTOS_setsockopt() to change the sliding window size from
    its default then do it here! (see the FreeRTOS_setsockopt()
    documentation. */

    /* Set a time out so accept() will just wait for a connection. */
    FreeRTOS_setsockopt( xListeningSocket,
                         0,
                         FREERTOS_SO_RCVTIMEO,
                         &xReceiveTimeOut,
                         sizeof( xReceiveTimeOut ) );

    /* Set the listening port to 10000. */
    xBindAddress.sin_port = ( uint16_t ) 10000;
    xBindAddress.sin_port = FreeRTOS_htons( xBindAddress.sin_port );

    /* Bind the socket to the port that the client RTOS task will send to. */
    FreeRTOS_bind( xListeningSocket, &xBindAddress, sizeof( xBindAddress ) );

    /* Set the socket into a listening state so it can accept connections.
    The maximum number of simultaneous connections is limited to 20. */
    FreeRTOS_listen( xListeningSocket, xBacklog );

    for( ;; )
    {
        /* Wait for incoming connections. */
        xConnectedSocket = FreeRTOS_accept( xListeningSocket, &xClient, &xSize );
        configASSERT( xConnectedSocket != FREERTOS_INVALID_SOCKET );
        leds_toggle( LEDS_BLUE );
        // /* Spawn a RTOS task to handle the connection. */
        // xTaskCreate( prvServerConnectionInstance,
        //              “EchoServer”,
        //              usUsedStackSize,
        //              ( void * ) xConnectedSocket,
        //              tskIDLE_PRIORITY,
        //              NULL );
    }
}

static portTASK_FUNCTION( vLedGreenTestTask, pvParameters )
{
    for( ;; )
    {
        vTaskDelay( 500 / portTICK_PERIOD_MS );
        if (xGetPhyLinkStatus() == pdFAIL)
        {
            leds_off(LEDS_GREEN);
        }
        else
        {
            leds_on(LEDS_GREEN);
        }
    }
}

static UBaseType_t ulNextRand = 42;

UBaseType_t uxRand( void )
{
    const uint32_t ulMultiplier = 0x015a4e35UL, ulIncrement = 1UL;

    ulNextRand = ( ulMultiplier * ulNextRand ) + ulIncrement;
    return( ( int ) ( ulNextRand >> 16UL ) & 0x7fffUL );
}


BaseType_t xApplicationGetRandomNumber(uint32_t* pulNumber)
{
    *(pulNumber) = uxRand();
    return pdTRUE;
}

uint32_t ulApplicationGetNextSequenceNumber( uint32_t ulSourceAddress,
                                                    uint16_t usSourcePort,
                                                    uint32_t ulDestinationAddress,
                                                    uint16_t usDestinationPort )
{
    ( void ) ulSourceAddress;
    ( void ) usSourcePort;
    ( void ) ulDestinationAddress;
    ( void ) usDestinationPort;

    return uxRand();
}

void vApplicationIPNetworkEventHook( eIPCallbackEvent_t eNetworkEvent )
{
}

