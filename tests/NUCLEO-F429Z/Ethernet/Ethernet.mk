ETH_TEST_DIR = $(PWD)
ETH_TEST_OBJ = $(ETH_TEST_DIR)/main.o \
			   $(ETH_TEST_DIR)/eth_test.o

CFLAGS += -I$(ETH_TEST_DIR)

$(ETH_TEST_DIR)/eth_test.elf: $(TARGET_OBJ) $(ETH_TEST_OBJ)
	$(CC) -mthumb -mcpu=$(CPU) -mfloat-abi=hard -mfpu=fpv4-sp-d16 -T$(ROOT_DIR)/$(LNK_SCRIPT) -Wl,--gc-sections -o $@ $^
	$(OBJCOPY) -Oihex $(ETH_TEST_DIR)/eth_test.elf $(ETH_TEST_DIR)/eth_test.hex

test/Ethernet: $(ETH_TEST_DIR)/eth_test.elf

test/Ethernet_clean:
	rm -rf $(ETH_TEST_OBJ)
	rm -rf $(ETH_TEST_DIR)/eth_test.elf $(ETH_TEST_DIR)/eth_test.hex
	rm -rf soc
	rm -rf bsp
	rm -rf hal