CFLAGS += -I$(ROOT_DIR)/tcp/include
CFLAGS += -I$(ROOT_DIR)/tcp/portable/Compiler/GCC
CFLAGS += -I$(ROOT_DIR)/tcp/portable/NetworkInterface/include

CONFIG_HAL_ETHERNET = 'y'
CONFIG_BSP_ETHERNET = 'y'

TCP_DIR = $(ROOT_DIR)/tcp
TCP_OBJ = 	 $(TCP_DIR)/FreeRTOS_ARP.o \
			 $(TCP_DIR)/FreeRTOS_DHCP.o \
			 $(TCP_DIR)/FreeRTOS_DNS.o \
			 $(TCP_DIR)/FreeRTOS_IP.o \
			 $(TCP_DIR)/FreeRTOS_Sockets.o \
			 $(TCP_DIR)/FreeRTOS_Stream_Buffer.o \
			 $(TCP_DIR)/FreeRTOS_TCP_IP.o \
			 $(TCP_DIR)/FreeRTOS_TCP_WIN.o \
			 $(TCP_DIR)/FreeRTOS_UDP_IP.o \
			 $(TCP_DIR)/FreeRTOS_UDP_IP.o \
			 $(TCP_DIR)/portable/BufferManagement/BufferAllocation_2.o \
			 $(TCP_DIR)/portable/NetworkInterface/Common/phyHandling.o \
			 $(TCP_DIR)/portable/NetworkInterface/STM32Fxx/NetworkInterface.o


TARGET_OBJ += $(TCP_OBJ)
