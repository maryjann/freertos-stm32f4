/*
 * Copyright (c) 2019 Lukasz Stempien
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "stm32f4xx_hal_conf.h"

#ifndef __ENDIAN_H__
#define __ENDIAN_H__

#if __BYTE_ORDER == __LITTLE_ENDIAN
	#define HTONL(l)	__REV(l)
	#define HTONS(s)	(uint16_t)__REV16(s)
	#define NTOHL(l)	__REV(l)
	#define NTOHS(s)	(uint16_t)__REV16(s)
#elif __BYTE_ORDER == __BIG_ENDIAN
	#define HTONL(l)	(l)
	#define HTONS(s)	(s)
	#define NTOHL(l)	(l)
	#define NTOHS(s)	(s)
#else
	#error Wrong endian type
#endif

#endif //__ENDIAN_H__